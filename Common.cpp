#include "Common.h"

Common& Common::Instance(){
	static Common s;
	return s;
}

Common::Common(){}

Common::~Common(){
}

glm::vec3 Common::getBezierPoint2(float t, glm::vec3 p1, glm::vec3 p2, glm::vec3 cp){
	const float dt = 1.f - t;
	glm::vec3 p;
	p = dt * dt * p1 + 2.f * dt * t * cp + t * t * p2;
	return p;
}

//I dont know what is it
glm::vec3 Common::getBezierPointN(float t, std::vector<glm::vec3> points){
	glm::vec3 res;
	std::vector<glm::vec3> tmp = points;
	int i,k; 
	i = points.size() - 1;

	while(i>0){
		for(k = 0; k < i; ++k){
			tmp[k] = tmp[k] + t * ( tmp[k+1] - tmp[k] );
		}
		--i;
	}
	res = tmp[0];

	return res;
}

glm::vec3 Common::getBSpline( float t, int degree, std::vector<glm::vec3> points ){
	glm::vec3 res;
	int i,j,s,l, high, low;
	const int n = points.size();
	const int d = 3; 
	const int kl = n+degree+1;
	int *weights, *knots, *domain;
	float alpha, **v;

	assert(n > 2);

	weights = new int[n];
	for(i=0; i<n; ++i) weights[i] = 1;
	
	knots = new int[kl];
	for(i=0; i<kl; ++i) knots[i] = i;

	domain = new int[2]{degree, kl - 1 - degree};

	low = knots[domain[0]];
	high = knots[domain[1]];

	t = t * (float)(high - low) + (float)low;

	for(s=domain[0]; s<domain[1]; s++) {
	  if(t >= knots[s] && t <= knots[s+1]) {
			break;
		}
	}

	v = new float*[n];
	for(i=0; i<n; i++) {
		v[i] = new float[d+1];
		for(j=0; j<d; j++) {
					v[i][j] = points[i][j] * (float)weights[i];
		}
		v[i][d] = weights[i];
	}

	for(l=1; l<=degree+1; l++) {
    for(i=s; i>s-degree-1+l; i--) {

			if( (float)(knots[i+degree+1-l] - knots[i]) != 0)
				alpha = (t - knots[i]) / (float)(knots[i+degree+1-l] - knots[i]);
			else
				alpha = 0;

      for(j=0; j<d+1; j++) {
        v[i][j] = (1 - alpha) * v[i-1][j] + alpha * v[i][j];
      }
    }
  }	

	for(i=0; i<d; i++) {
		res[i] = v[s][i] / v[s][d];
	}

	delete weights;
	delete knots;

	return res;

}

glm::vec3 Common::getRoundPoint(float rad, float rot){
	float rp = 2.f * M_PI * rot;
	return glm::vec3(
		rad * cosf(rp),
		0,
		rad * sinf(rp)	
	);
}

glm::vec3 Common::getBSplinePoint(float t, std::vector<glm::vec3> points){
	glm::vec3 res;
	
	return res;
}

float Common::getTVal( float t, float start, float end ){
	return start + t * (end-start);
}
glm::vec3 Common::getTVal( float t, glm::vec3 start, glm::vec3 end ){
	return start + glm::vec3(t) * (end-start);
}
