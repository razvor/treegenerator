#ifndef B_SPLINE_CURVE
#define B_SPLINE_CURVE

#include <vector>
#include <iostream>
#include "glm/glm.hpp"

class BSplineCurve{
private:
	
	int degree;
	int dimension;
	int baseFuncRangeInt;
	std::vector<glm::vec3> points;

public:
	BSplineCurve(std::vector<glm::vec3>  points, int degree);
	~BSplineCurve();
	
	float basisDec2(float x);

	float seqAt(int dim, int n);

	float getInterpol(int dim, float t);

	glm::vec3 calcAt(float t);

};

#endif
