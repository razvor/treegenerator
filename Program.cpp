#include <iostream>
#include <fstream>
#include <OpenGL/gl.h>
#include <GLUT/glut.h>
#include <streambuf>

using namespace std;

class Program{
	
	string vShaderExt = "vsh",
				 fShaderExt = "fsh",
				 shaderFileRoot = "shaders/";

	string vShaderFile, 
				 fShaderFile, 
				 vShaderSource, 
				 fShaderSource;

	GLuint vShader, fShader, program;

	int k1 = 0, k2 = 0;

public:

	bool loadShader( int type ){
		//cout << "Load Shader" << endl;
		string fl, src;
		
		if( type == 1 ){
			fl = this -> vShaderFile + "." + this -> vShaderExt;
		}else{
			fl = this -> fShaderFile + "." + this -> fShaderExt;
		}

		fl = this -> shaderFileRoot + fl;

		ifstream shaderFile(fl);
		if( shaderFile.fail() ) return false;
		string shaderContent = string(
			istreambuf_iterator<char>(shaderFile),
			istreambuf_iterator<char>()
		);
		shaderFile.close();

		if( type == 1 ) 
			vShaderSource = shaderContent;
		else
			fShaderSource = shaderContent;

		//cout << "Loaded Shader" << endl;

		return true;
	}

	bool compileShader( int type ){
		//cout << "Compile Shader" << endl;

		GLuint shader;
		GLint compiled;
		string *src;
		bool res = true;

		if( type == 1 ){
			shader = glCreateShader(GL_VERTEX_SHADER);
			src = &vShaderSource; 
		}else{
			shader = glCreateShader(GL_FRAGMENT_SHADER);
			src = &fShaderSource; 
		}

		const char *sr = src -> c_str();
		
		glShaderSource( shader, 1, &sr, NULL );
		glCompileShader( shader );

		glGetShaderiv( shader, GL_COMPILE_STATUS, &compiled );

		if(!compiled){

			GLint blen = 0;
			GLsizei slen = 0;
				
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH , &blen);     

			GLchar* compilerLog = (GLchar*)malloc(blen);
			glGetShaderInfoLog(shader, 1000, &blen, compilerLog);
			cout << "Compilation error: " << compilerLog << endl;

			res = false;	
		}

		if( type == 1 ) vShader = shader; else fShader = shader;
		
		//cout << "Compiled shader" << endl;

		return res;

	}
	
	bool createProgram(){
		bool res = true;
		GLuint program = glCreateProgram();
		glAttachShader( program, vShader );	
		glAttachShader( program, fShader );	

		glLinkProgram( program );

		GLint linked;
		glGetProgramiv(program, GL_LINK_STATUS, &linked);
		if(!linked){
			GLint len = 0;
			//glGetProgramiv( program,	GL_INFO_LOG_LENGTH, &len );
			GLchar *linkLog = (GLchar*)malloc(len);
			//glGetInfoLog(program, len, linkLog);
			//cout << vShaderFile << endl;
			cout << "Link error in " << vShaderFile << ": " << linkLog << endl;
			res = false;
		}

		//glBindFragDataLocation( program, 0, "fragColor" );

		this -> program = program;

		return res;

	}

	bool init(){
		return 
			loadShader(1) &&
			loadShader(2) &&
			compileShader(1) &&
			compileShader(2) &&
			createProgram();
	}

	GLint getInstance(){
		return program;
	}

	void use(){
		glUseProgram( this -> getInstance() );
	}

	void unuse(){
		glUseProgram( 0 );
	}

	int attribute( const char *name ){
		int r = glGetAttribLocation( this -> getInstance(), name );
		if( r == -1 && k1 == 0 ){ cerr << "No attr found for " << name << endl; k1++;}
		return r;
	}

	int uniform( const char *name ){
		int r = glGetUniformLocation( this -> getInstance(), name );
		if( r == -1 && k2 == 0 ){ cerr << "No unif found for " << name << endl; k2++;}
		return r;
	}

	Program( string vertex, string fragment ){
		vShaderFile = vertex;
		fShaderFile = fragment;
	};

	~Program(){
		glUseProgram(0);
		glDetachShader( getInstance(), vShader );
		glDetachShader( getInstance(), fShader );
		glDeleteShader( vShader );
		glDeleteShader( fShader );
	}
};
