#ifndef COMMON
#define COMMON
#include <iostream>
#include <vector>
#include <array>
#include "glm/glm.hpp"
#include "glm/gtx/string_cast.hpp"

class Common{
public:

	static Common& Instance();
	glm::vec3 getBezierPoint2(float t, glm::vec3 sp, glm::vec3 ep, glm::vec3 cp);
	glm::vec3 getBezierPointN(float t, std::vector<glm::vec3> points);
	glm::vec3 getRoundPoint(float rad, float rot);

	glm::vec3 getBSplinePoint(float t, std::vector<glm::vec3> points);
	
	glm::vec3 getBSpline(float t, int degree, std::vector<glm::vec3> points);

	float getTVal( float t, float start, float end );
	glm::vec3 getTVal( float t, glm::vec3 start, glm::vec3 end );
	

private:

	Common(Common const&) = delete;
	Common& operator= (Common const&) = delete;

	Common();
	~Common();

};

#endif
