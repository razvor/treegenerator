#ifndef TREE_MESH_MAKER_H
#define TREE_MESH_MAKER_H

#include <vector>
#include <queue>
#include <list>
#include <iostream>
#include "SegmentMesh.h"
#include "TreeModel.h"

class TreeMeshMaker{
private:
	SegmentData *rootSegment;	

	//for multithreading
	int currentProcessingQueue;
	std::vector<std::queue<SegmentData *>> processingQueue;
	
	std::queue<SegmentMesh *> segmentsQueue;
	
	//For find parent and connect
	std::list<SegmentMesh *> inheritSegmentsList;

	TreeModel *model;

	//making segment separatly and adding to queue
	void makeSegment( SegmentData *d );

	//processing from queue, moving index, returns new index offset
	size_t processSegment( size_t offset );

	void processSegments();
	SegmentMesh *findParent(SegmentMesh *s);

	void pushSegment( SegmentData *d );
	void makerRunner(int ct);

public:
	TreeMeshMaker( SegmentData *r );
	~TreeMeshMaker();

	void runProcessing();

	void makeTestSegment();

	TreeModel *getModel();
};
#endif
