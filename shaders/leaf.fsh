#version 330

in vec3 vPosition;
in vec3 vNormal;
in mat4 vMat; 

uniform mat4 model1;
uniform mat4 model2;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 vinv;

layout(location=0) out vec4 FragColor;

struct light{
	vec4 position;
	vec4 diffuse;
	vec4 specular;

	float attenuation;
	float spotCutoff;
};

struct material{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

light lamp = light(
	vec4(0.0,  10.0,  20.0, 0.0),
	vec4( vec3( 0.7 ), 1.0),
	vec4(vec3(0.5), 1.0),
	2.0,
	180.0
);

material testMaterial = material(
	vec4( 0.8, 0.8, 0.8, 1.0 ),
	vec4( vec3( 1.0 ), 1.0 ),
	vec4( 0.5 ),
	1.0
);

vec4 mainAmbient = vec4( vec3( 0.1 ), 1.0 );

/*vec4 smoothTex( vec4 baseColor ){
	float distAlphaMask = baseColor.a;

	return baseColor;

}*/

void main(){

	mat4 model = model1 * model2;

	vec3 norm = normalize(vec3( vMat * vec4( vNormal, 0.0 ) ));
	vec3 pos = vec3( vMat * vec4( vPosition, 0.0 ) );
	vec3 ldir = normalize(vec3( vec3( view * lamp.position ) - pos ));
	vec3 vdir = normalize(vec3( vinv * vec4(1.0) - model * vec4(vPosition,1.0) ));
	float dist = length( ldir );
	ldir = normalize( ldir );

	float attenuation = 1.0 / ( dist * lamp.attenuation );

	vec4 materialColor = vec4( 0.15, 0.35, 0.15, 1.0 ); //vec4(texture( uTexture, vTexCord.xy ).rgb,1.0);

	vec3 ambientLight = vec3(materialColor); //+ vec3( mainAmbient );

	vec3 diffuse = attenuation 
		* vec3(lamp.diffuse)
		* vec3(testMaterial.diffuse)
		* max( 0.0, dot( norm, -ldir ) );

	vec3 specular = attenuation 
		* vec3(lamp.specular) 
		* vec3(testMaterial.specular) 
		* pow( max( 0.0, dot(reflect(-ldir, norm), vdir) ), testMaterial.shininess );

	FragColor = vec4( ambientLight + diffuse + specular, 1.0 );

}
