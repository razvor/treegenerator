attribute aPosition;
attribute aNormal;

uniform mvp;

varying vNormal;

void main(void){

	vNormal = aNormal;

	gl_Position = mvp * aPosition;
}
