#version 330

layout(location=0) in vec3 aPosition;
layout(location=1) in vec3 aNormal;
layout(location=2) in vec3 aTexCord;

uniform mat4 projection;
uniform mat4 model;
uniform mat4 view;

out vec3 vNormal;
out vec3 vPosition;
out mat4 vMat;

void main(){

	vNormal = aNormal;
	vPosition = aPosition;
	vMat = projection * view * model;

	gl_Position = projection * view * model * vec4( aPosition, 1.0 );
}

