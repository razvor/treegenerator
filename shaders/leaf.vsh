#version 330

layout(location=0) in vec3 aPosition;
layout(location=1) in vec3 aNormal;
//layout(location=2) in vec3 aTexCord;

//uniform mat4 projection;
//uniform mat4 model;
//uniform mat4 view;

uniform mat4 model1;
uniform mat4 model2;
uniform mat4 view;
uniform mat4 projection;

out vec3 vNormal;
out vec3 vPosition;
out mat4 vMat;

void main(){

	mat4 mvp =  projection * view * model1 * model2;

	vNormal = aNormal;
	vPosition = aPosition;
	vMat = mvp;

	gl_Position = mvp * vec4( aPosition, 1.0 );
}
