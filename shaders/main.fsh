#version 330

/*varying vec3 vPosition;
varying mat4 vMat; 
varying vec2 vTexCord;
varying vec3 vNormal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 vinv;
uniform sampler2D uTexture;*/

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCord;
in mat4 vMat; 

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 vinv;

uniform sampler2D uTexture;
uniform sampler2D uNormalMap;

layout(location=0) out vec4 FragColor;

struct light{
	vec4 position;
	vec4 diffuse;
	vec4 specular;

	float attenuation;
	float spotCutoff;
};

struct material{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

light lamp = light(
	vec4(10.0,  4.0,  10.0, 0.0),
	vec4( vec3( 0.9 ), 1.0),
	vec4(1.0, 1.0, 1.0, 1.0),
	3.0,
	180.0
);

material testMaterial = material(
	vec4( vec3( 0.9 ), 1.0 ),
	vec4( vec3( 1.0 ), 1.0 ),
	vec4( 0.5 ),
	1.0
);

vec4 mainAmbient = vec4( vec3( 0.1 ), 1.0 );

void main(){

	/*vec3 norm = normalize(vec3( vMat * vec4( vNormal, 0.0 ) ));
	vec3 pos = vec3( vMat * vec4( vPosition, 0.0 ) );
	vec3 ldir = normalize(vec3( vec3( view * lamp.position ) - pos ));
	vec3 vdir = normalize(vec3( vinv * vec4(1.0) - model * vec4(vPosition,1.0) ));
	float dist = length( ldir );
	ldir = normalize( ldir );

	float attenuation = 1.0 / ( dist * lamp.attenuation );

	vec4 materialColor = //vec4(texture( uTexture, vTexCord.xy ).rgb,1.0);
	
	vec3 ld = vec3( vec3(lamp.position) - pos );
	float ndl = max( dot( (norm), ld ), 0.0 );

	vec3 ambientLight = vec3(0);
	
	vec3 diffuse = vec3(0);
	if(ndl > 0.0){
		diffuse = vec3( attenuation  * 1.0 );
		ambientLight = vec3(mainAmbient) + vec3(materialColor); //vec3(0.4, 0.6, 0.3);
	}

	FragColor = vec4( ambientLight + diffuse, 1.0 );*/

	vec3 norm = normalize(vec3( vMat * vec4( vNormal, 0.0 ) ));
	vec3 pos = vec3( vMat * vec4( vPosition, 0.0 ) );
	vec3 ldir = normalize(vec3( vec3( view * lamp.position ) - pos ));
	vec3 vdir = normalize(vec3( vinv * vec4(1.0) - model * vec4(vPosition,1.0) ));
	float dist = length( ldir );
	ldir = normalize( ldir );

	float attenuation = 1.0 / ( dist * lamp.attenuation );

	vec4 materialColor = vec4(texture( uTexture, vTexCord.xy ).rgb,1.0);

	vec3 normalTex = normalize( texture( uNormalMap, vTexCord ).rgb * 2.0 - 1.0 );

	vec3 nt = normalTex - vec3( 0, 0, 0 );

	vec3 ambientLight = vec3(materialColor) + vec3( mainAmbient );

	vec3 diffuse = attenuation 
		* vec3(lamp.diffuse)
		* vec3(testMaterial.diffuse)
		* max( 0.0, dot( norm, -ldir ) );

	vec3 specular = attenuation 
		* vec3(lamp.specular) 
		* vec3(testMaterial.specular) 
		* pow( max( 0.0, dot(reflect(-ldir, norm), vdir) ), testMaterial.shininess );

	vec3 diffuse2 = attenuation * 2.0
		* vec3( 0.25 )
		* dot( normalTex, vec3( ldir - norm ) );

	FragColor = vec4( ambientLight + diffuse - diffuse2 * 0.0, 1.0 );	

}
