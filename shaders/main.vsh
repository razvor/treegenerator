#version 330

layout(location=0) in vec3 aPosition;
layout(location=1) in vec3 aNormal;
layout(location=2) in vec2 texCord;

uniform mat4 projection;
uniform mat4 model;
uniform mat4 view;
uniform mat4 vinv;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexCord;
out mat4 vMat;

void main(){

	vNormal = aNormal;
	vPosition = aPosition;
	vTexCord = texCord;
	vMat = projection * view * model;

	gl_Position = projection * view * model * vec4( aPosition, 1.0 );
}

