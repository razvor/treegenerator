#version 330

in vec3 vPosition;
in mat4 vMat; 
in vec3 vNormal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 vinv;
uniform mat4 smvp;
uniform sampler2D shadow;

layout(location=0) out vec4 FragColor;

struct light{
	vec4 position;
	vec4 diffuse;
	vec4 specular;

	float attenuation;
	float spotCutoff;
};

struct material{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

light lamp = light(
	vec4(0.0,  20.0,  7.0, 0.0),
	vec4(0.9, 0.9, 0.9, 1.0),
	vec4(1.0, 1.0, 1.0, 1.0),
	4.0,
	180.0
);

material testMaterial = material(
	vec4( 0.8, 0.8, 0.8, 1.0 ),
	vec4( 1.0, 0.8, 0.8, 1.0 ),
	vec4( 0.5 ),
	1.0
);

vec4 mainAmbient = vec4( vec3( 0.3 ), 1.0 );

void main(){

	vec3 norm = normalize(vec3( vMat * vec4( vNormal, 0.0 ) ));
	vec3 pos = vec3( vMat * vec4( vPosition, 0.0 ) );
	vec3 ldir = normalize(vec3( vec3( projection * view * lamp.position ) - pos ));
	vec3 vdir = normalize(vec3(vinv * vec4(0.0, 0.0, 0.0, 1.0) - model * vec4(vPosition,1.0) ));
	float dist = length( ldir );
	ldir = normalize( ldir );

	float attenuation = 1.0 / ( dist * lamp.attenuation );

	vec4 shadowPos = smvp * vec4(vPosition,1);
	vec3 shadowCords = vec3( shadowPos ) / shadowPos.w;

	float closestDepth = texture( shadow, shadowCords.xy ).r;
	float currentDepth = shadowCords.z; 

	float shadow = currentDepth > closestDepth ? 1.0 : 0.0;
	float fShadow = 1.0 - shadow;

	vec4 materialColor = vec4( 0.6 );
	//vec4( vec3( 1.0 - shadow ), 1.0 );
	//texture( shadow, vec3(shadowPos.xy, shadowPos.z / shadowPos.w ) ) * vec4(1.0); 
	//mainAmbient; //vec4(texture( uTexture, vTexCord ).rgb,1.0);
	//materialColor *= 0.3;
	
	vec3 ambientLight = vec3(materialColor) ;

	vec3 diffuse = attenuation 
		* vec3(lamp.diffuse)
		* vec3(testMaterial.diffuse)
		* max( 0.0, dot( norm, -ldir ) );

	vec3 specular = attenuation 
		* vec3(lamp.specular) 
		* vec3(testMaterial.specular) 
		* pow( max( 0.0, dot(reflect(-ldir, norm), vdir) ), testMaterial.shininess );

	FragColor = vec4( ambientLight + fShadow * (diffuse + specular), 1.0 );

}
