cmake_minimum_required(VERSION 3.2)
project(plant_generator)

find_package(OpenGL REQUIRED)

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -w -std=c++11 -O2 -lglfw3 -lglew -gdwarf-3 -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo")

set( CMAKE_EXPORT_COMPILE_COMMANDS 1 )

include_directories(glm)

set(SOURCE_FILES
	main.cpp 
	SegmentData.cpp
	SegmentMesh.cpp
	SegmentBinderMesh.cpp
	TreeMeshMaker.cpp 
	Common.cpp
	TreeModel.cpp
	TreeGenerator.cpp 
	CrustMaker.cpp 
	LeafMaker.cpp
	Logger.cpp 
	BSplineCurve.cpp
	#VertexBuffer.cpp 
	#Texture.cpp
	)
	#TreeMeshMaker.h
	#TreeGenerator.h
	#CrustMaker.h)

set(SOURCE_HEADERS
	TreeMeshMaker.h
	TreeGenerator.h
	CrustMaker.h)

add_executable(tree ${SOURCE_FILES})
#target_link_libraries(main foo)	
