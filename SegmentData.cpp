#include "SegmentData.h"
#include "glm/gtc/matrix_transform.hpp"
#include <iostream>
#include "Common.h"

SegmentData::SegmentData(){
		parent = NULL;
		children.clear();
}

SegmentData::~SegmentData(){
	for(auto c : children){
		if(c) delete c;
	}
	for(auto c : outgrouths){
		if(c) delete c;
	}
}

void SegmentData::setInheritIndices(int child, int outgrouth){
	assert( !(child != -1 && outgrouth != -1) );
	childIndex = child;
	outgrouthIndex = outgrouth;
}

int SegmentData::getInheritType(){
	if(childIndex > -1){
		return 1;
	}else if(outgrouthIndex > -1){
		return 2;
	}else{
		return 0;
	}
}

int SegmentData::getInheritValue(){
	return (childIndex > -1) ? childIndex : outgrouthIndex;
}

glm::vec3 SegmentData::getPosition(){
	return startPosition;
}
void SegmentData::setPosition(glm::vec3 p){
	startPosition = p;
}

void SegmentData::setOutgrouthPosition(glm::vec2 p){
	if(
			p.x >= 0 && p.y >= 0 &&
			p.x <= 1 && p.y <= 1
	){
		outgrouthPosition = p;
	}
}
glm::vec3 SegmentData::getOutgrouthPosition(int ind){
	assert(ind < outgrouthsPositions.size());
	return outgrouthsPositions[ind];
}
glm::vec2 SegmentData::getCurrentOutgrouthPosition(){
	return outgrouthPosition;
}

std::vector<glm::vec3> SegmentData::getCurvePoints(){
	return curvePoints;
}
void SegmentData::addCurvePoint(glm::vec3 p){
	curvePoints.push_back(p);
}

glm::vec3 SegmentData::getAngle(){
	return angle;
}
void SegmentData::setAngle(glm::vec3 a){
	angle = a;
}

float SegmentData::getLength(){
	return length;
}
void SegmentData::setLength(float l){
	length = l;
}

std::pair<float, float> SegmentData::getRadiuses(){
	return std::make_pair(startRadius, endRadius);
}
void SegmentData::setRadiuses(float sr, float er){
	startRadius = sr;
	endRadius = er;
}

std::pair<int, int> SegmentData::getDensity(){
	return std::make_pair( widthDensity, heightDensity );
}
void SegmentData::setDensity(int w, int h){
	widthDensity = w;
	heightDensity = h;
}

float SegmentData::getChildrenRotation(){
	return childRotation;
}
void SegmentData::setChildrenRotation(float cr){
	isChildrenRotation = true;
	childRotation = cr;
}

SegmentData* SegmentData::getParent(){
	return parent;
}
void SegmentData::setParent(SegmentData *p){
	parent = p;
}

int SegmentData::getChildrenCount(){
	return children.size();
}
SegmentData *SegmentData::getChild( int ind ){
	if( children.size() <= ind ) return NULL;
	return children[ ind ];
}
void SegmentData::addChild(SegmentData *p){
	children.push_back(p);	
}

void SegmentData::addOutgrouth(SegmentData *out){
	outgrouths.push_back(out);	
}
int SegmentData::getOutgrouthsCount(){
	return outgrouths.size();
}
SegmentData *SegmentData::getOutgrouth(int ind){
	assert(ind < getOutgrouthsCount());
	return outgrouths[ind];
}

glm::mat4 SegmentData::getPostureMatrix(){
	return postureMatrix;
}
glm::mat4 SegmentData::getBinderMatrix(){
	return binderMatrix;
}

glm::mat4 SegmentData::getParentPostureMatrix(){
	if( parent ){
		return parent->getPostureMatrix();	
	}else{
		return glm::mat4(1);
	}
}

glm::vec3 SegmentData::getEndPosition(){
	return endPosition; //+ glm::mat3(getBinderMatrix()) * glm::vec3( 0, getRadiuses().second, 0 );
}

glm::vec3 SegmentData::getChildPosition(int ind){
	return childrenPositions[ind];
}

float SegmentData::getSideHeight(int ind){
	assert(ind < sidesHeights.size());
	//if(ind >= sidesHeights.size() ) return 0;
	return sidesHeights[ind];
}

glm::mat4 SegmentData::getOutgrouthMatrix(int ind){
	float angle = outgrouths[ind]->getCurrentOutgrouthPosition().x;
	glm::mat4 res, own;
	glm::vec3 ownAngle;

	ownAngle = outgrouths[ind]->getAngle();
	
	own = glm::rotate( glm::mat4(1), ownAngle.x, glm::vec3(1,0,0) );
	own = glm::rotate( own, ownAngle.y, glm::vec3(0,1,0) );
	own = glm::rotate( own, ownAngle.z, glm::vec3(0,0,1) );
	
	res = getPostureMatrix();
	res = glm::rotate( res, (float)(M_PI / 2.f - M_PI * 2.f * angle), glm::vec3(0,1,0) );
	res = glm::rotate( res, (float)M_PI / 2.f, glm::vec3(1,0,0) );
	
	res = res * own;	

	return res;
}

void SegmentData::setPostureMatrix(glm::mat4 m){
	 postureMatrix = m;
}

void SegmentData::computeStartAngle(){

}
void SegmentData::comuteEndAngle(){

}
void SegmentData::computePostureMatrix(){
	glm::mat4 rotMatrs[3];
	glm::mat4 rotMatr;
	if(parent != NULL && getInheritValue() != 0){
		rotMatrs[0] = glm::rotate( glm::mat4(1.0), getAngle()[0], glm::vec3( 1, 0, 0 ) );
		rotMatrs[1] = glm::rotate( glm::mat4(1.0), parent->getChildrenRotation(), glm::vec3( 0, 1, 0 ) );
		//rotMatrs[2] = glm::rotate( glm::mat4(1.0), getAngle()[2], glm::vec3( 0, 0, 1 ) );
		//rotMatr = rotMatrs[1] * rotMatrs[0];
		rotMatr = getParentPostureMatrix() * rotMatrs[1] * rotMatrs[0]; //glm::rotate( parent->getBinderMatrix(), getAngle()[0], glm::vec3(1,0,0) );
	}else{
		rotMatrs[0] = glm::rotate( glm::mat4(1.0), getAngle()[0], glm::vec3( 1, 0, 0 ) );
		//rotMatrs[1] = glm::rotate( glm::mat4(1.0), getAngle()[1], glm::vec3( 0, 1, 0 ) );
		rotMatrs[2] = glm::rotate( glm::mat4(1.0), getAngle()[2], glm::vec3( 0, 0, 1 ) );
		rotMatr = glm::mat4(1.0); //rotMatrs[0] * rotMatrs[2];
		rotMatr = getParentPostureMatrix() * rotMatr;
	}
	postureMatrix = rotMatr; //getParentPostureMatrix() * rotMatr;
}

void SegmentData::computeBinerMatrix(){
	binderMatrix = 
		glm::rotate( getPostureMatrix(), getChildrenRotation(), glm::vec3( 0, 1, 0 ) );
}

void SegmentData::computeStartPosition(){

	glm::mat3 pmat, cmat;
	float side, r, cr, sideHeight;
	glm::vec3 cPoint;

	if(getParent()){
		
		startPosition = getParent()->getEndPosition();
		r = getParent()->getRadiuses().second;

		if(getParent()->getChildrenCount() == 1){
			startPosition += glm::mat3(getParent()->getPostureMatrix()) * glm::vec3(0, r, 0);
		}else{
			pmat = glm::mat3(getParent()->getBinderMatrix());
			side = getInheritValue() == 2 ? -1.f : 1.f;
			cr = getRadiuses().first;
			//TODO let it work work normally 
			sideHeight = getParent()->getRadiuses().second * 2.5f; //+ getChild(i)->getRadiuses().first
			cmat = glm::mat3(getPostureMatrix());
			cPoint = getEndPosition();
			cPoint += pmat * glm::vec3(0, 0, r*side);
			cPoint += cmat * glm::vec3(0, sideHeight, -r * side);
			startPosition = cPoint;
		}
		//TODO add this result to parent
	}else{
		startPosition = glm::vec3(0);
	}
}

void SegmentData::computeEndPosition(){
	endPosition = startPosition + glm::mat3(postureMatrix) * glm::vec3( 0, getLength(), 0 );
}

void SegmentData::computeChildrenPositions(){
	assert(getChildrenCount()!=0);
	float sideHeight;
	float r, cr, side, curveRadius, curveAngle, ang;
	glm::vec3 cPoint;
	glm::mat3 pmat, cmat;

	r = getRadiuses().second;

	if(getChildrenCount() == 2){
		pmat = glm::mat3(getBinderMatrix());
		for(int i=0; i<2; ++i){
			side = i ? -1.f : 1.f;
			cr = getChild(i)->getRadiuses().first;
			sideHeight = getSideHeight(i);
			cmat = glm::mat3(getChild(i)->getPostureMatrix());
			cPoint = getEndPosition();
			cPoint += pmat * glm::vec3(0, 0, r*side);
			cPoint += cmat * glm::vec3(0, 0, -r * side);
			cPoint += cmat * glm::vec3(0, sideHeight, 0);
			childrenPositions.push_back(cPoint);
		}
		
		//TODO: smooth binder
		/*for(int i=0; i<2; ++i){
			side = i ? -1.f : 1.f;
			sideHeight = getSideHeight(i);
			cmat = glm::mat3(getChild(i)->getPostureMatrix());
			cPoint = getEndPosition();
			pmat = glm::mat3(getBinderMatrix());
			//cPoint += cmat * glm::vec3(0, sideHeight, 0);
			
			ang = M_PI/2 - getChild(i)->getAngle().x;
			curveRadius = getSideHeight(i) / cosf(ang);
			curveAngle = M_PI/2 - ang;
			cPoint += pmat * glm::vec3( 0, 0, curveRadius );
			cmat = glm::mat3(glm::rotate(glm::mat4(pmat), curveAngle + (float)M_PI, glm::vec3(1,0,0)));
			cPoint += cmat * glm::vec3(0,0,curveRadius);
			
			childrenPositions.push_back(cPoint);
		}*/

	}else if(getChildrenCount() == 1){
		//TODO child segemnt to edge with smooth binder
		pmat = glm::mat3(getPostureMatrix());
		cmat = glm::mat3(getChild(0)->getPostureMatrix());
		cr = getChild(0)->getRadiuses().first;
		cPoint = getEndPosition();
		cPoint += pmat * glm::vec3( 0, cr, 0 );
		//cPoint += pmat * glm::vec3( 0, 0, r );
		//cPoint += cmat * glm::vec3( 0, 0, -cr );
		childrenPositions = {cPoint};
	}
}

void SegmentData::computeSidesHeights(){
	if(getChildrenCount() == 2){
		for(int i=0;i<2;++i){
			sidesHeights.push_back(
				getRadiuses().second * 2.5f //+ getChild(i)->getRadiuses().first
			);
		}
	}
}

void SegmentData::computeOutgrouthsPositions(){
	glm::vec2 localPos; 
	glm::vec3 pos;
	float rStep, cRadius;
	int ind = 0;
	std::vector<glm::vec3> curvePoints, controlPoints;

	outgrouthsPositions.resize(outgrouths.size());
	
	rStep = getRadiuses().second - getRadiuses().first;

	curvePoints = getCurvePoints();
	controlPoints.push_back(glm::vec3(0,0,0));
	for(int i=0; i<curvePoints.size(); ++i) controlPoints.push_back(curvePoints[i]);
	controlPoints.push_back(glm::vec3(0,getLength(),0));

	for(auto oi : outgrouths){
		
		localPos = oi->getCurrentOutgrouthPosition();

		cRadius = getRadiuses().first + rStep * localPos.y - oi->getRadiuses().first;

		pos = glm::vec3(0);
		
		//pos += Common::Instance().getBSpline( localPos.y, 2, controlPoints );
			
		pos += Common::Instance().getBezierPoint2( 
				localPos.y,
				glm::vec3(0), 
				glm::vec3(0,getLength(),0),
				getCurvePoints()[0]
		);
		
		pos += Common::Instance().getRoundPoint( cRadius, localPos.x );
		pos = glm::mat3(getPostureMatrix()) * pos;
		pos += getPosition();

		outgrouthsPositions[ind++] = pos;
	}
}
