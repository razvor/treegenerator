#ifndef TREE_MESH_MAKER_H
#define TREE_MESH_MAKER_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <array>
#include <tgmath.h>
#include <thread>
#include <functional>
#include <future>
#include <mutex>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/noise.hpp"

#include "Logger.h"

using namespace std;

class TreeMeshSegment{
public:

	//Start params
	
	glm::vec3 startPoint, endPoint, curvePoint; 
	glm::vec3 angle;

	int quality;

	float height;
	
	float startRadius, 
				endRadius;

	float childRotation;

	int parentConnect;

	float tcStart, tcEnd;

	TreeMeshSegment *parent;

	//Post counted

	int level;

	int rowCount;

	int **rowIndices;
	int *bindIndices;
	int **endIndices;
	int **endSides;

	//Outgrouth only
	float positionX, positionY;
	vector< vector< int > > outgrouthBindIndices;
	vector< vector< int > > outgrouthIndices;

	glm::mat4 parentMat;
	glm::mat4 translateMat;
	glm::mat4 childTranslateMat;

	void setStartPoint( glm::vec3 sp );

	void setEndPoint( glm::vec3 sp );

	void setRadius( float s, float e );

	void setQuality( int q );

	void setHeight( float h );

	void setAngle( glm::vec3 a );

	void setParentConnect( int pc );

	void setCurvePoint( glm::vec3 p );

	void setChildRotation( float rt );

	void setParentMat( glm::mat4 pm );

	void setTexCords( float s, float e );

	void setOutgrouthPositions( float x, float y );

	void setParent( TreeMeshSegment *p, bool duality );

	glm::mat4 getChildTranslateMat();

	TreeMeshSegment();

	~TreeMeshSegment();
	
};

class TreeMeshMaker{

public:

	int startQuality;
	int startRadius;

	long long cc;

	vector< glm::vec3 > vertices;
	vector< glm::vec3 > normals;
	vector< glm::vec2 > texCords;
	vector< int > noiseVals;
	vector< int > indices;

	mutex verticesMutex;
	mutex normalsMutex;
	mutex indicesMutex;
	mutex texCordsMutex;
	
	public:
	
	TreeMeshMaker();	
	~TreeMeshMaker();

	//Common
	
	int addVert( float x, float y, float z );
	int addVert( glm::vec3 p );

	void addInd( int i );

	static glm::vec3 getBezierPoint( float t, glm::vec3 p1, glm::vec3 p2, glm::vec3 cp );

	int getRound( int *arr, const int size, int ind );

	int *copyRound( int *arr, const int size, const int fi, const int li );

	//Segment
	
	void makeCilinderVertices( TreeMeshSegment *seg );
	
	void makeCilinderIndices( TreeMeshSegment *seg );

	void makeCilinderNormals( TreeMeshSegment *seg );

	void makeCilinderTexCords( TreeMeshSegment *seg );	

	void makeCilinderNoiseValues( TreeMeshSegment *seg );
	
	//Per segment

	void bindChildInd( int size, int *ind1, int *ind2 );	
	
	void makeEndVertices( TreeMeshSegment *seg, TreeMeshSegment *child1, TreeMeshSegment *child2	);	

	void makeEndIndices( TreeMeshSegment *seg, TreeMeshSegment *child1, TreeMeshSegment *child2 );

	void makeEndNormals( TreeMeshSegment *seg );

	void makeEndTexCords( TreeMeshSegment *seg );

	void makeFinish( TreeMeshSegment *seg );

	glm::vec3 getCilinderPoint( TreeMeshSegment *seg, float positionX, float positionY );
	
	glm::vec3 getChildPos( TreeMeshSegment *par, float angle, float radius, int n );
	static glm::mat4 getCurveMat( TreeMeshSegment *par, bool isStart );

	const glm::vec3 getOutgrouthStartPos( TreeMeshSegment *seg, TreeMeshSegment *out );
	const glm::vec3 getOutgrouthStartPos( TreeMeshSegment *seg, int a, int b );
	glm::mat4 getStartOutgrouthMat( TreeMeshSegment *seg, int hpos, int vpos );

	void bindChildren( TreeMeshSegment *par, TreeMeshSegment *c1, TreeMeshSegment *c2 );
	void bindChild( TreeMeshSegment *par, TreeMeshSegment *child );
	void makeOuthgrouthBind( TreeMeshSegment *par, TreeMeshSegment *out );
	
	void makeSegment( TreeMeshSegment *cur );

	void makeTestSegment();

	void makeTestTree();
	
	void genTestSegment();

	TreeMeshSegment *genTestSegmentC( TreeMeshSegment *par, int childType );

	TreeMeshSegment *kk( TreeMeshSegment *b, int e );

	void genChildren( TreeMeshSegment *seg );
	
	void genTestSegment( TreeMeshSegment *par, int w, int h );

	void genBB( TreeMeshSegment *par );
	void genS( TreeMeshSegment *par );
	
	
};

#endif

