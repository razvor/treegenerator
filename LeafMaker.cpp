#ifndef LEAF_MAKER
#define LEAF_MAKER

#include "LeafMaker.h"

float LeafMaker::getOneRandom(){
	return (rand() % 1000) / 1000.f;
}

void LeafMaker::addNewLeaf(){
	leafs.push_back( Leaf() );
}

void LeafMaker::makeVertices(){
	//Test only	
	
	/*Leaf *lastLeaf = &leafs.back();

	vector< glm::vec3 > verts;

	int branchCount = params.branchCount;
	int outgrouthCount = params.outCount;

	float radius = params.size; 
	float radius2 = radius / 2;
	float oAngle = M_PI / 5;

	float bRotateStep = M_PI / (branchCount + 1);

	glm::vec2 startPos, endPos, step, currentStartPos, currentEndPos;

	float currentRot, oCurrentRot;
	int i, j;

	for( int k = 0; k < 2; ++k ){
		for( i = 0; i < branchCount; i+=1 ){
			
			currentRot = (i+1) * bRotateStep;

			startPos = glm::vec2( 0, 0 );
			endPos = glm::vec2( radius * cosf( currentRot ), radius * sinf( currentRot ) );

			step = (endPos - startPos) / static_cast<float>( outgrouthCount - 1 );

			for( j = 0; j < outgrouthCount; ++j ){

				currentStartPos = startPos + static_cast<float>(j) * step;
			
				oCurrentRot = !k ? currentRot + oAngle : currentRot - oAngle;	
				currentEndPos = currentStartPos - glm::vec2( 
						radius2 * cosf( oCurrentRot ), 
						radius2 * sinf( oCurrentRot ) 
				);
				verts.push_back( glm::vec3(
					currentStartPos,
					0
				) );	
				verts.push_back( glm::vec3(
					currentEndPos,
					0
				) );	
			}

			verts.push_back( glm::vec3( static_cast<float>( j ) * step - glm::vec2(0.0), 0 ) );
			
		}
	}

	lastLeaf -> vertices = verts; */
	/*{
		glm::vec3( 0, -0.5, 0 ),
		glm::vec3( -0.5, 0.5, 0 ),
		glm::vec3( 0.5, 0.5, 0 ),
	};*/

}

void LeafMaker::makeIndices(){
	Leaf *lastLeaf = &leafs.back();
	//auto *lastIndices = &lastLeaf -> indices;
	
	vector< int > inds;

	int ci, ni, ti, nti;

	/*for( int i = 0; i < lastLeaf -> vertices.size(); i+=1 ){
		ci = i;
		ti = i + 1;	
		ni = i + 2;
		nti = i + 4;

		inds.push_back( ci );
		inds.push_back( ti );
		inds.push_back( ni );
		//inds.push_back( ni );
		//inds.push_back( nti );
		//inds.push_back( ti );

	}*/

	const int 
		sidesCount    = 2,
		branchCount = params.branchCount,
		outCount = params.outCount,
		sideStep = outCount * 2 + 1,
		branchStep = outCount * 4;

	for( int k = 0; k < sidesCount; ++k ){
		for( int j = 0; j < branchCount; ++j ){
			for( int i = 0; i < outCount; ++i ){
				ci = i * 2 + (k ? sideStep : 0) + j * branchStep;
				ti = ci + 1;	
				ni = ci + 2;
				inds.push_back( ci );
				inds.push_back( ti );
				inds.push_back( ni );
			}
		}
	}

	/*inds = {
		0, 1, 2,
		2, 3, 4,
		4, 5, 6
	};*/
	
	lastLeaf -> indices = inds; /*){
		0, 1, 2
	};*/
		
}

void LeafMaker::makeNormals(){
	Leaf *lastLeaf = &leafs.back();
	auto *lastNormals = &lastLeaf -> normals;

	for( int i = 0; i < lastLeaf -> vertices.size(); ++i )
		lastNormals->push_back( glm::vec3( 0, 1, 0 ) );
	
	//lastNormals->push_back( glm::vec3( 0, 1, 0 ) );
	//lastNormals->push_back( glm::vec3( 0, 1, 0 ) );
}

void LeafMaker::makeLeaf( glm::vec3 position, glm::vec3 rotation ){
	addNewLeaf();

	leafs.back().position = position;
	leafs.back().rotation = rotation;
	
}

void LeafMaker::makeLeafs( SegmentData *seg ){

	/*const int leafCount = params.leafCount;
	const float heightStep = 1.f / leafCount;

	glm::vec3 
		startPos = glm::vec3( 0 ),
		endPos   = glm::vec3( 0, seg->height, 0 ),
		curvePos = seg -> curvePoint;

	glm::vec3 rot;

	float rx, ry, rz;

	for( int i = 0; i < leafCount; ++i ){
		glm::vec3 p = TreeMeshMaker::getBezierPoint( i * heightStep, startPos, endPos, curvePos );
		p = glm::mat3( seg -> translateMat ) * p;
		p += seg->startPoint;
		rx = getOneRandom() * M_PI;
		ry = getOneRandom() * M_PI;
		rz = getOneRandom() * M_PI;
		rot = glm::vec3( rx, 0, rz );
		//rot = glm::vec3( M_PI, 0, 0 );

		makeLeaf( glm::vec3( p ), rot );
	}*/

	makeVertices();
	makeIndices();
	makeNormals();

}

void LeafMaker::setParams( LeafParams p ){
	params = p;
}

vector<Leaf> LeafMaker::getLeafs(){
	return leafs;
}

LeafMaker::LeafMaker(){

}

LeafMaker::~LeafMaker(){

}

#endif
