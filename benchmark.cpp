#include <iostream>

#include "TreeGenerator.h"

vector<glm::vec3> vertices;
vector<glm::vec3> normals;
vector<glm::vec2> tc;
vector<int> inds;

long long e;

void gg(){
	TreeGenerator g;

	vertices = g.maker.vertices;
	normals = g.maker.normals;
	tc = g.maker.texCords;
	inds = g.maker.indices;

	g.clearSegmentsData();

}

int main(){
	
	gg();
	
	int a;
	cin >> a;

}
