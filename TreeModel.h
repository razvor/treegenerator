#ifndef TREE_MODEL_H
#define TREE_MODEL_H

#include <vector>
#include <tuple>
#include "glm/glm.hpp"

struct TreeModel{
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texCords;
	std::vector<size_t> indices;
	//float[3] **texture;

	TreeModel();
	~TreeModel();
};

#endif
