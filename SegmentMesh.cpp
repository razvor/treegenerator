#include "SegmentMesh.h"
#include <iostream>
#include "glm/gtx/vector_angle.hpp"
#include "glm/gtx/spline.hpp"
#include "BSplineCurve.h"
#include "Common.h"

SegmentMesh::SegmentMesh(SegmentData *data){

	this->data = data;
	int dw = data->getDensity().first,
			dh = data->getDensity().second;

	rowIndices = new size_t*[dh];
	for (int i = 0; i < dh; i++) {
		rowIndices[i] = new size_t[dw];	
	}

	vertices = new glm::vec3[dw * dh];
	normals = new glm::vec3[dw * dh];
	texCords = new glm::vec2[dw * dh];

	binder = NULL;
}

SegmentMesh::~SegmentMesh(){
	if(rowIndices) delete[] rowIndices;	
	delete[] vertices;
	delete[] normals;
	delete[] texCords;

	if(binder)
		delete binder;
}

glm::vec3 SegmentMesh::getBezierPoint( float t, glm::vec3 p1, glm::vec3 p2, glm::vec3 cp ){
	const float dt = 1.f - t;
	glm::vec3 p;
	p = dt * dt * p1 + 2 * dt * t * cp + t * t * p2;
	return p;
}

void SegmentMesh::makeVertices(){
	const float 
		vStep = 2.f * M_PI / (float)data->getDensity().first,
		hStep = data->getLength() / (float)data->getDensity().second,
		rStep = (data->getRadiuses().second - data->getRadiuses().first) / data->getDensity().second,
		tStep = 0.96f / (data->getDensity().second-1);

	int i, j, ind;
	float cVertical, cHorizontal, cRadius;
	glm::vec3 cCurve, cPoint;
	glm::mat3 cmat;
	std::vector<glm::vec3> controlPoints, curvePoints;

	ind = 0;
	cmat = glm::mat3(data->getPostureMatrix());
	curvePoints = data->getCurvePoints();
	controlPoints.push_back(glm::vec3(0,0,0));
	for(int i=0; i<curvePoints.size(); ++i) controlPoints.push_back(curvePoints[i]);
	controlPoints.push_back(glm::vec3(0,data->getLength(),0));

	for (j = 0; j < data->getDensity().second; j++) {
		for (i = 0; i < data->getDensity().first; i++) {

			cVertical = vStep * i;
			cHorizontal = hStep * j;
			cRadius = (data->getRadiuses().first + rStep * j);
			
			cCurve = getBezierPoint( 
					tStep * j + 0.02, 
					glm::vec3(0), 
					glm::vec3( 0, data->getLength(), 0 ),
					data->getCurvePoints()[0]
			);

			//cCurve = Common::Instance().getBSpline( tStep * j, 2, controlPoints);
			//if(j == 0) cCurve = glm::vec3(0);

			/*cCurve = glm::hermite(
					glm::vec3(0), 
					data->getCurvePoints()[0],
					glm::vec3( 0, data->getLength(), 0 ),
					data->getCurvePoints()[1],
					tStep * j
			);*/

			cPoint = cCurve + glm::vec3(
				cRadius * cosf( cVertical ),
				0,
				cRadius * sinf( cVertical )
			);

			cPoint = cmat * cPoint;
			cPoint += data->getPosition();

			vertices[ind] = cPoint;
			rowIndices[j][i] = ind;
			ind++;

		}
	}
	
}

void SegmentMesh::makeNormals(){

	const float vStep = 2.f * M_PI / data->getDensity().first;

	int i, j, ind;	

	glm::vec3 cVec;

	ind = 0;
	for (j = 0; j < data->getDensity().second; j++) {
		for (i = 0; i < data->getDensity().first; i++) {
			cVec = glm::vec3(
				cosf( vStep * i ),
				0,
				sinf( vStep * i )
			);
			cVec = glm::mat3(data->getPostureMatrix()) * cVec;
			normals[ind] = cVec;
			ind++;
		}
	}

}
void SegmentMesh::makeTexCords(){
	const float 
		vStep = 1.f / (data->getDensity().first-1),
		hStep = 1.f / (data->getDensity().second-1);
	glm::vec2 cCord;
	int i, j, ind;	
	ind = 0;
	for (j = 0; j < data->getDensity().second; j++) {
		for (i = 0; i < data->getDensity().first; i++) {
			cCord = glm::vec2(
				hStep * i,
				vStep * j
			);
			texCords[ind] = cCord;
			ind++;
		}
	}
}
void SegmentMesh::makeIndices(){
	int i, j, ci, ni, ti, nti;
	for (j = 0; j < data->getDensity().second-1; j++) {
		for (i = 0; i < data->getDensity().first; i++) {
			ci = rowIndices[j][i];
			ti = rowIndices[j+1][i];

			if( i == data->getDensity().first - 1 ){ 
				ni = rowIndices[j][0]; 
				nti = rowIndices[j+1][0];
			}else{
				ni = rowIndices[j][i+1];
				nti = rowIndices[j+1][i+1];
			}
			indices.push_back(ci);
			indices.push_back(ni);
			indices.push_back(ti);
			indices.push_back(ti);
			indices.push_back(ni);
			indices.push_back(nti);
		}
	}
}

void SegmentMesh::bindDigitVertices( size_t *a1, size_t *a2, int s1, int s2 ){
	if(s1 == s2){
		int ci, ni, ti, nti;
		for(int i=0;i<s1-1;++i){
			ci = a1[i];
			ti = a2[i];

			ni = a1[i+1];
			nti = a2[i+1];

			indices.push_back(ci);
			indices.push_back(ni);
			indices.push_back(ti);
			indices.push_back(ti);
			indices.push_back(ni);
			indices.push_back(nti);
		}
	}else if(s1-1 == (s2-1)/2){
		const int sz = 9;
		size_t ci, ni, bci, bni, bmi;
		for(int i=0;i<s1-1;++i){
			ci = a2[i];
			bci = a1[i*2];
			
			ni = a2[i+1];
			bni = a1[(i+1)*2];
			
			bmi = a1[i*2+1];

			std::array<size_t, sz> indArr = { 
				ci, bci, bmi, 
				bmi, ci, ni, 
				bni, bmi, ni 
			};

			addInds<sz>(indArr);
		}
	}
}


template<int S>
void SegmentMesh::addInds(std::array<size_t, S> arr){
	for(int i=0; i<S; ++i){
		indices.push_back(arr[i]);
	}
}

void SegmentMesh::setBinder(SegmentBinderMesh *bm){
	binder = bm;
}

size_t SegmentMesh::moveIndices( size_t offset ){
	
	for(int i=0;i<indices.size();++i){
		indices[i] += offset;
		if(i < data->getDensity().first){
			rowIndices[data->getDensity().second-1][i] += offset;
			rowIndices[0][i] += offset;
		}
	}

	size_t s = data->getDensity().first * data->getDensity().second;
	
	if( binder != NULL ){
		s = binder->moveIndices( offset + s );
		bindWithBinder();
	}else
		s = offset + s;

	return s;
}

void SegmentMesh::bindWithBinder(){
	if(binder != NULL){
		int i, dw, dh, mrg, ci;
		size_t *arr1, *arr2;
		float rotation;

		dw = data->getDensity().first;
		dh = data->getDensity().second;
		arr1 = new size_t[ dw + 1 ];
		rotation = data->getChildrenRotation();
		rotation = rotation - floor(rotation / (2.f * M_PI)) * 2.f * M_PI;

		mrg = -dw * (float)(data->getChildrenRotation() / (2.f * M_PI));
		for(i=0; i<dw; ++i){
			ci = i+mrg;

			if(ci < 0) ci = dw+ci;
			if(ci >= dw) ci = ci-dw;

			if(ci < 0 || ci >= dw) ci = 0;
				
			arr1[i] = rowIndices[dh-1][ci];
		}
		arr1[dw] = arr1[0];

		//arr1 = getLastRing();

		arr2 = binder->getFirstRing();

		bindDigitVertices( arr1, arr2, dw+1, dw+1 );
	}
}

void SegmentMesh::bindWithParent(SegmentMesh *parentMesh){
	if(data->getInheritType() == 1){
		int dw1, dw2;

		dw2 = data->getDensity().first;
		dw1 = parentMesh->getData()->getDensity().first;

		if(data->getInheritValue() == 0){
			
			if(dw1 == dw2){
				bindDigitVertices(parentMesh->getLastRing(0), getFirstRing(), dw1+1, dw2+1); 
			}else if(dw2 == dw1 / 2){ 
				bindDigitVertices(parentMesh->getLastRing(0), getFirstRing(), dw2+1, dw1+1); 
			} 
		}else{ 
			if(dw1 == dw2){
				bindDigitVertices( 
						parentMesh->binder->getLastRing(data->getInheritValue()-1), 
						getFirstRing(),
						dw2+1,
						dw1+1
				);
			}else if(dw2 == dw1 / 2){
				bindDigitVertices( 
						parentMesh->binder->getLastRing(data->getInheritValue()-1), 
						getFirstRing(),
						dw2+1,
						dw1+1
				);
			}
		}
	}
}

void SegmentMesh::runProcessing(){
	makeVertices();
	makeNormals();
	makeTexCords();
	makeIndices();
}

SegmentData* SegmentMesh::getData(){
	return data;
}

size_t *SegmentMesh::getLastRing( float rot ){
	int dw, dh, mrg, i, ci;
	size_t *arr1;
	float rotation;

	dw = data->getDensity().first;
	dh = data->getDensity().second;
	arr1 = new size_t[ dw + 1 ];
	rotation = data->getChildrenRotation();
	rotation = rotation - floor(rotation / (2.f * M_PI)) * 2.f * M_PI;

	mrg = -dw * (float)(data->getChildrenRotation() / (2.f * M_PI));
	for(i=0; i<dw; ++i){
		ci = i;//+mrg;

		if(ci < 0) ci = dw+ci;
		if(ci >= dw) ci = ci-dw;
			
		arr1[i] = rowIndices[dh-1][ci];
	}
	arr1[dw] = arr1[0];

	return arr1;
}

size_t *SegmentMesh::getFirstRing(){
	int dw = data->getDensity().first;
	size_t *row = new size_t[dw+1];

	for(int i=0; i<dw; ++i){
		row[i] = rowIndices[0][i];	
	}
	row[dw] = row[0];

	return row;
}

std::vector< glm::vec3 > SegmentMesh::getVertices(){
	size_t s = data->getDensity().first * data->getDensity().second;
	auto r = std::vector< glm::vec3 >( vertices, vertices + s );
	if(binder != NULL){
		auto br = binder->getVertices();
		r.insert(r.end(), br.begin(), br.end());
	}
	return r;
}

std::vector< glm::vec3 > SegmentMesh::getNormals(){
	size_t s = data->getDensity().first * data->getDensity().second;
	auto r = std::vector< glm::vec3 >( normals, normals + s );
	if(binder != NULL){
		auto br = binder->getNormals();
		r.insert( r.end(), br.begin(), br.end() );
	}
	return r;
}

std::vector< glm::vec2 > SegmentMesh::getTexCords(){
	size_t s = data->getDensity().first * data->getDensity().second;
	auto r = std::vector< glm::vec2 >( texCords, texCords + s );
	if(binder != NULL){
		auto br = binder->getTexCords();
		r.insert(r.end(), br.begin(), br.end());
	}
	return r;
}

std::vector< size_t > SegmentMesh::getIndices(){
	auto r = indices;
	if(binder != NULL){
		auto br = binder->getIndices();
		r.insert(r.end(), br.begin(), br.end());
	}
	return r;
}
