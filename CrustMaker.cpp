#ifndef CRUST_MAKER
#define CRUST_MAKER

#include "CrustMaker.h"

void CrustMaker::initTexture(){

	texture = new GLubyte[ Width * Height * 3 ];
	normalMap = new GLubyte[ Width * Height * 3 ];

	pTexture = new Pixel*[ Height ];
	for( int i = 0; i < Height; ++i ){
		pTexture[ i ] = new Pixel[ Width ];
	}
	
	pNormalMap = new Pixel*[ Height ];
	for( int i = 0; i < Height; ++i ){
		pNormalMap[ i ] = new Pixel[ Width ];
	}

}

float interpolate(float x0, float x1, float alpha){
   return x0 * (1 - alpha) + alpha * x1;
}

void CrustMaker::genStartNoise(){

	float xCoff = 18.f, yCoff = xCoff / 3,
				moveX = rand() % 1000, moveY = rand() % 1000;

	int ci, fcr, fcg, fcb;
	float nn, nn2, nn3;

	fcr = 98 + 0;
	fcg = 62 + 0;
	fcb = 31 + 0;

	for( int i = 0; i < Width; ++i ){
		for( int j = 0; j < Height; ++j ){
			ci = 3 * ( i * Width + j );
			
			nn = glm::perlin( glm::vec3( i / xCoff, j / yCoff, 1 ) );
			nn2 = glm::perlin( glm::vec3( i / xCoff, j / yCoff, 2 ) );
			nn3 = glm::perlin( glm::vec3( i / xCoff, j / yCoff, 4 ) );

			//nn = nn * 10;
			//nn -= (int)nn;
			nn = (1.f + nn) / 2.f;
			
			nn2 = nn2 * 10;
			nn2 -= (int)nn2;
			nn2 = (1.f + nn2) / 2.f;

			/*nn3 = nn3 * 10;
			nn3 -= (int)nn3;
			nn3 = (1.f + nn3) / 2.f;*/

			nn = sinf( 1 + sinf( nn * 5.5 ) ) / 2.f;
			
			nn = interpolate( nn, nn2, nn3 );

			//nn = (nn + nn2) / 2.f;

			//nn = (nn + nn3) / 2.f;


			//nn = (1.f + sinf( ( i + glm::perlin( glm::vec2( i * 5 , j * 5 ) ) / 2.f ) * 50.f ) ) / 2.f;

			if( nn < 0 ) nn = 0.2; else nn /= 2;

			texture[ ci + 0 ] = fcr * nn;
			texture[ ci + 1 ] = fcg * nn;
			texture[ ci + 2 ] = fcb * nn;

			Pixel currentPixel;
			currentPixel.r = fcr * nn;
			currentPixel.g = fcg * nn;
			currentPixel.b = fcb * nn;

			pTexture[ i ][ j ] = currentPixel;

		}
	}
}

const double CrustMaker::intensity(const Pixel pixel){

    const double 
				r = static_cast<double>( pixel.r ),
				g = static_cast<double>( pixel.g ),
    		b = static_cast<double>( pixel.b );

    const double average = ( r + g + b ) / 3.0;

    return average / 255.0;
}

const int CrustMaker::clamp(int pX, int pMax){

    if (pX > pMax){
        return pMax;
    }
    else if (pX < 0){
        return 0;
    }
    else{
        return pX;
    }
}

const GLubyte CrustMaker::mapComponent( float pX ){
	return (pX + 1.0) * (255.0 / 2.0);
}

void CrustMaker::normalsFromHeight( float pStrength ){

	const int textureSize = static_cast<int>( Width ) - 1;
	
	int i, j;

	for( i = 0; i < Width; ++i ){
		for( j = 0; j < Height; ++j ){
			
				const Pixel topLeft     = pTexture[ clamp( i - 1, textureSize ) ][ clamp( j - 1, textureSize ) ];
				const Pixel top         = pTexture[ clamp( i - 1, textureSize ) ][ clamp( j,     textureSize ) ];
				const Pixel topRight    = pTexture[ clamp( i - 1, textureSize ) ][ clamp( j + 1, textureSize ) ];
				const Pixel right       = pTexture[ clamp( i,     textureSize ) ][ clamp( j + 1, textureSize ) ];
				const Pixel bottomRight = pTexture[ clamp( i + 1, textureSize ) ][ clamp( j + 1, textureSize ) ];
				const Pixel bottom      = pTexture[ clamp( i + 1, textureSize ) ][ clamp( j,     textureSize ) ];
				const Pixel bottomLeft  = pTexture[ clamp( i + 1, textureSize ) ][ clamp( j - 1, textureSize ) ];
				const Pixel left        = pTexture[ clamp( i,     textureSize ) ][ clamp( j - 1, textureSize ) ];

				const double tl = intensity( topLeft     );
				const double t  = intensity( top         );
				const double tr = intensity( topRight    );
				const double r  = intensity( right       );
				const double br = intensity( bottomRight );
				const double b  = intensity( bottom      );
				const double bl = intensity( bottomLeft  );
				const double l  = intensity( left        );

				const double dX = ( tr + 2.0 * r + br ) - ( tl + 2.0 * l + bl );
				const double dY = ( bl + 2.0 * b + br ) - ( tl + 2.0 * t + tr );
				const double dZ = 1.0 / pStrength;

				glm::vec3 v( dX, dY, dZ );
				glm::normalize(v);
			
				Pixel p;
				p.r = mapComponent( v.x ); 
				p.g = mapComponent( v.y ); 
				p.b = mapComponent( v.z );

				pNormalMap[ i ][ j ] = p;

		}
	}
	
}

void CrustMaker::prepareNormalMap(){
	int currentIndex;
	Pixel currentPixel;
	for( int i = 0; i < Width; ++i ){
		for( int j = 0; j < Height; ++j ){
			currentIndex = (i * Width + j) * 3;
			currentPixel = pNormalMap[ i ][ j ];
			normalMap[ currentIndex + 0 ] = currentPixel.r;
			normalMap[ currentIndex + 1 ] = currentPixel.g;
			normalMap[ currentIndex + 2 ] = currentPixel.b;
		}
	}
}

void CrustMaker::makeCrust(){
	initTexture();	
	genStartNoise();
	normalsFromHeight( 2.0 );
	prepareNormalMap();
}

GLubyte* CrustMaker::getTexture(){
	return texture;
}

GLubyte* CrustMaker::getNormalMap(){
	return normalMap;
}

CrustMaker::CrustMaker( int w, int h ){
	Width = w;
	Height = h;
	makeCrust();
}

CrustMaker::~CrustMaker(){

}

#endif
