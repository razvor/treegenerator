#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

using namespace std;

class Logger{
private:
	static Logger *instance;
public:
	static Logger *getInstance();

	Logger();	
	~Logger();

	struct Glm{
		void vec( glm::vec3 v );		
		void mat( glm::mat4 );
	};

	Glm *glm;
	
};

//Logger *Logger::instance = 0;

#endif
