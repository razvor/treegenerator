#include "BSplineCurve.h"

BSplineCurve::BSplineCurve(std::vector<glm::vec3>  points, int degree){
	assert(degree == 2);
	this->points = points;
	this->degree = degree;
	this->dimension = 3;
	if(degree == 2){
		this->baseFuncRangeInt = 2;
	}
}

BSplineCurve::~BSplineCurve(){}
	
float BSplineCurve::basisDec2(float x){
	if(-0.5 <= x && x < 0.5){
		return 0.75 - x*x;
	}else if(0.5 <= x && x <= 1.5){
		return 1.125 + (-1.5 + x/2.0)*x;
	}else if(-1.5 <= x && x < -0.5){
		return 1.125 + (1.5 + x/2.0)*x;
	}else{
		return 0;
	}	
}

float BSplineCurve::seqAt( int dim, int n ){
	int margin = degree + 1;
	int ci;
	if(n < margin){
		ci = 0;
	}else if(points.size() + margin <= n){
		ci = points.size() - 1;
	}else{
		ci = n - margin;
	}
	return points[ci][dim];
}

float BSplineCurve::getInterpol(int dim, float t){
	int tInt = floor(t),
			rangeInt = baseFuncRangeInt;
	float result = 0;
	for(int i = tInt - rangeInt; i <= tInt +rangeInt; i++){
		//std::cout << "ii " << i << std::endl;
		result += seqAt(dim, i)*basisDec2(t-i);
	}
	return result;	
}

glm::vec3 BSplineCurve::calcAt(float t){
	t = t * (float)( (degree+1)*2 + points.size());

  return glm::vec3(getInterpol(0,t), getInterpol(1,t), getInterpol(2,t));
}
