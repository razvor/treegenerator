#include <vector>
#include <array>
#include "glm/glm.hpp"
#include "Common.h"
#include "SegmentMesh.h"

#ifndef SEGMENT_BINDER_H
#define SEGMENT_BINDER_H

//SegmentMesh m;

class SegmentBinderMesh{
private:
	SegmentData *currentSegment;

	int childrenCount;
	int dw, dh;
	int dwSides, dhSides;
	
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texCords;
	std::vector<size_t> indices;
	std::vector<size_t**> rowIndicesSides;
	std::vector<size_t*> ringIndices;
	std::vector<size_t**> rowIndicesCenter;
	std::vector<size_t**> rowIndicesTop;
	glm::vec3 **sidesCenters;
	glm::vec3 **centersCenters;

	int addVert( glm::vec3 p );
	
	template<int S>
	void addInds( std::array<size_t, S> arr );
	
	glm::vec3 getRoundPoint( glm::mat3 mat, float radius, float rot );

	glm::vec3 getBezierDirection( float t, glm::vec3 p1, glm::vec3 p2, glm::vec3 cp );
	glm::vec3 getBezierPoint( float t, glm::vec3 p1, glm::vec3 p2, glm::vec3 cp );
	glm::vec3 getBezierPoint( float t, glm::vec3 p1, glm::vec3 p2, glm::vec3 cp1, glm::vec3 cp2 );
	glm::vec3 getBezierPointR( float t, float w, glm::vec3 p1, glm::vec3 p2, glm::vec3 cp );

	void makeRingVertices();
	void makeSideVertices();
	void makeCenterVertices();
	void makeTopVertices();
	void makeVertices();
	
	void makeNormals();
	void makeTexCords();

	void makeSingleChildIndices();
	
	void makeSidesIndices();
	void makeTopIndices();
	void makeCenterIndices();

	void makeIndices();

	void initDefaults();

public:

	SegmentBinderMesh( SegmentData* s );
	~SegmentBinderMesh();
	
	void runProcessing();

	size_t moveIndices( size_t mv );
	void bindVertices( size_t *arr1, size_t *arr2, int s );
	void bindDigitVertices( size_t *arr1, size_t *arr2, int s );

	size_t *getFirstRing();
	size_t *getLastRing(int ind);
	std::vector<glm::vec3> getVertices();
	std::vector<glm::vec3> getNormals();
	std::vector<glm::vec2> getTexCords();
	std::vector<size_t> getIndices();

};

#endif
