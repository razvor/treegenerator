#ifndef TREE_MESH_MAKER
#define TREE_MESH_MAKER

#include "TreeMeshMaker2.h"

using namespace std;

void TreeMeshSegment::setStartPoint( glm::vec3 sp ){
	startPoint = sp;
}

void TreeMeshSegment::setEndPoint( glm::vec3 sp ){
	endPoint = sp;
}

void TreeMeshSegment::setRadius( float s, float e ){
	startRadius = s;
	endRadius = e;	
}

void TreeMeshSegment::setQuality( int q ){
	quality = q;
	
	rowIndices = new int*[ q ];
	endIndices = new int*[ q ];
	bindIndices = new int[ q / 2 ];
	endSides = new int*[ 16 ];

	for(int i = 0; i < q; ++i) 
		rowIndices[ i ] = new int[ q ];

	for(int i = 0; i < q; ++i) 
		endIndices[ i ] = new int[ q ];

	for(int i = 0; i < 16; i++)
		endSides[i] = new int[32];
}

void TreeMeshSegment::setHeight( float h ){
	height = h;
}

void TreeMeshSegment::setAngle( glm::vec3 a ){
	angle	= a;
}

void TreeMeshSegment::setParentConnect( int pc ){
	parentConnect = pc;
}

void TreeMeshSegment::setCurvePoint( glm::vec3 p ){
	curvePoint = p;
}

void TreeMeshSegment::setChildRotation( float rt ){
	childRotation = rt;
}

void TreeMeshSegment::setParentMat( glm::mat4 pm ){
	parentMat = pm;
}

void TreeMeshSegment::setTexCords( float s, float e ){
	tcStart = s;
	tcEnd = e;
}

void TreeMeshSegment::setOutgrouthPositions( float x, float y ){
	positionX = x;
	positionY = y;
}

glm::mat4 TreeMeshSegment::getChildTranslateMat(){
	return glm::rotate( translateMat, childRotation, glm::vec3(0,1,0) );
}

void TreeMeshSegment::setParent( TreeMeshSegment *p, bool duality ){
	parent = p;
	level = p -> level + ( duality ? 1 : 0 );
}

TreeMeshSegment::TreeMeshSegment(){}

TreeMeshSegment::~TreeMeshSegment(){
	for( int i = 0; i < quality; ++i ) delete rowIndices[ i ];
	for( int i = 0; i < quality; ++i ) delete endIndices[ i ];
	for( int i = 0; i < 16; ++i ) delete endSides[ i ];
	delete bindIndices;
}

/*
 *	Begin maker
 * */

TreeMeshMaker::TreeMeshMaker(){
}

TreeMeshMaker::~TreeMeshMaker(){}

	//Common
	
int TreeMeshMaker::addVert( float x, float y, float z ){
	int s;
	verticesMutex.lock();
	vertices.push_back( glm::vec3( x, y, z ) );
	s = vertices.size() - 1;
	verticesMutex.unlock();
	return s;
}

int TreeMeshMaker::addVert( glm::vec3 p ){
	return addVert( p.x, p.y, p.z );
}

void TreeMeshMaker::addInd( int i ){
	indicesMutex.lock();
	indices.push_back( i );
	indicesMutex.unlock();
}

glm::vec3 TreeMeshMaker::getBezierPoint( float t, glm::vec3 p1, glm::vec3 p2, glm::vec3 cp ){

	const float dt = 1.f - t;

	glm::vec3 p;
	
	p = dt * dt * p1 + 2 * dt * t * cp + t * t * p2;

	return p;
}

int TreeMeshMaker::getRound( int *arr, const int size, int ind ){
	if( ind > size - 1 ){
		ind = ind % size;
	}else if( ind < 0 ){
		ind = ( ind +	size );
	}
	int nn = arr[ ind ];
	return nn;
}

int* TreeMeshMaker::copyRound( int *arr, const int size, const int fi, const int li ){
	int *res = new int[ li - fi ];
	for( int i = fi; i < li; i++ ){
		res[ i - fi ] = getRound( arr, size, i );
	}
	return res;
}

	//Segment
	
void TreeMeshMaker::makeCilinderVertices( TreeMeshSegment *seg ){
	const int qa = seg->quality;	
	
	const float 
				x1 = seg->startPoint.x,
				y1 = seg->startPoint.y,
				z1 = seg->startPoint.z,
				sr = seg->startRadius,
				er = seg->endRadius,
				h  = seg->height;

	const int heightStepCount = qa;

	const float 
				vstep = 6.28f / qa,
				hstep = h / static_cast<float>( heightStepCount ),
				rstep = ( er - sr ) / static_cast<float>( qa ),
				tstep = 1.f / qa;

	float cv, ch, ci, radius, x, y, z;
	int i,j;

	glm::mat4 trxa = glm::rotate( glm::mat4(1.0), seg->angle[0], glm::vec3( 1, 0, 0 ) );
	glm::mat4 trya = glm::rotate( glm::mat4(1.0), seg->angle[1], glm::vec3( 0, 1, 0 ) );
	glm::mat4 trza = glm::rotate( glm::mat4(1.0), seg->angle[2], glm::vec3( 0, 0, 1 ) );

	glm::mat4 trf = seg -> parentMat * trxa * trza * trya;

	seg->translateMat = trf;

	glm::vec3 ccp = seg -> curvePoint;
	glm::vec3 sp  = glm::vec3(0);
	glm::vec3 ep  = glm::vec3( glm::vec4( 0, h, 0, 0 ));

	//seg->setEndPoint( seg->startPoint + glm::vec3( trf * glm::vec4(ep, 0) ) );
	
	glm::vec3 pp;
	glm::vec3 curve;

	for( j = 0; j < heightStepCount; ++j ){
		for( i = 0; i < qa; ++i ){
			cv = vstep * i;
			ch = hstep * j;
			radius = (sr + rstep * j);
			
			curve = getBezierPoint( tstep * j, glm::vec3( 0 ), ep, ccp );
			//curve = glm::vec3(trf * glm::vec4(curve, 0));

			x = radius * cos( cv ) + curve.x;
			z = radius * sin( cv ) + curve.z;
			y = curve.y;

			pp = glm::vec3( x, y, z );
			pp = glm::mat3(trf) * pp;

			pp.x += x1;
			pp.y += y1;
			pp.z += z1;

			ci = addVert( pp.x, pp.y, pp.z );

			seg->rowIndices[ j ][ i ] = ci;

		}
	}
}
	
void TreeMeshMaker::makeCilinderIndices( TreeMeshSegment *seg ){
	const int qa = seg->quality;	

	const int heightStepCount = qa;

	int **ri = seg->rowIndices;

	int ci, ni, ti, nti, i, j;

	for( j = 0; j < heightStepCount - 1; ++j ){
		for( i = 0; i < qa; ++i ){
		
			ci = ri[j][ i ];
			ti = ri[j + 1][i];

			if( i == qa - 1 ){ 
				ni = ri[j][0]; 
				nti = ri[j+1][0];
			}else{
				ni = ri[j][ i + 1 ];
				nti = ri[ j + 1 ][ i + 1 ];
			}

			addInd( ci );
			addInd( ni );
			addInd( ti );

			addInd( ti );
			addInd( ni );
			addInd( nti );

		}
	}
}

void TreeMeshMaker::makeCilinderNormals( TreeMeshSegment *seg ){

	const int qu = seg -> quality;

	const int heightStepCount = qu;

	glm::vec3 currentVec;
	glm::mat4 xMat, zMat;

	normalsMutex.lock();
	normals.resize( vertices.size() );
	normalsMutex.unlock();

	int i,j;
	
	xMat = glm::rotate( glm::mat4(1.0f), static_cast<float>(M_PI) / 2.0f, glm::vec3(1,0,0) );

	for( j = 0; j < heightStepCount; ++j ){
		for( i = 0; i < qu; ++i ){
			currentVec = glm::vec3(0, 1.0, 0);

			zMat = glm::rotate( glm::mat4( 1.0f ), static_cast<float>(2.f * M_PI / seg->quality * i - M_PI / 2), glm::vec3( 0, 0, 1 ) );

			currentVec = glm::vec3( seg->translateMat * xMat * zMat * glm::vec4( currentVec, 0 ));

			normalsMutex.lock();
			normals[ seg->rowIndices[ j ][ i ] ] = currentVec;
			normalsMutex.unlock();

		}
	}

}

void TreeMeshMaker::makeCilinderTexCords( TreeMeshSegment *seg ){
	const float q = seg->quality,
				tcStart = seg->tcStart,
				tcEnd   = seg->tcEnd,
				iStep   = (1.f) / (q+1);

	const int heightStepCount = q;

	glm::vec2 currentCord;

	texCordsMutex.lock();
	texCords.resize( vertices.size() );
	texCordsMutex.unlock();

	for( int j = 0; j < heightStepCount; ++j ){
		for( int i = 0; i < q; ++i ){
			currentCord = glm::vec2(
				iStep * i,
				iStep * j		
			);
			
			texCordsMutex.lock();
			texCords[ seg -> rowIndices[ j ][ i ] ] = currentCord;
			texCordsMutex.unlock();
			
		}
	}

}

void TreeMeshMaker::makeCilinderNoiseValues( TreeMeshSegment *seg ){
	const int q = seg->quality;
	for( int j = 0; j < q; ++j ){
		for( int i = 0; i < q; ++i ){
			noiseVals[ seg->rowIndices[ j ][ i ] ] = rand() % 10000;
		}
	}
}
	
	//Per segment

void TreeMeshMaker::bindChildInd( int size, int *ind1, int *ind2 ){

	int ci, ni, ti, nti;
	
	for( int i = 0; i < size; i++ ){
		
		ci = ind1[ i ];
		ti = ind2[ i ];

		//if( i == size - 1 ){
		//	ni = ind1[ 0 ];
		//	nti = ind2[ 0 ];
		//}else{
			ni = ind1[ i + 1 ];
			nti = ind2[ i + 1 ];
		//}
		
		addInd( ci );
		addInd( ni );
		addInd( ti );

		addInd( ti );
		addInd( ni );
		addInd( nti );
	}

}

void TreeMeshMaker::makeEndVertices( TreeMeshSegment *seg, TreeMeshSegment *child1, TreeMeshSegment *child2	){
	
	const int quality = seg->quality;

	const float 
		radius = seg->endRadius,
		radius1 = child1->startRadius,
		radius2 = child2->startRadius,
		endX = seg->endPoint.x,
		endY = seg->endPoint.y,
		endZ = seg->endPoint.z,
		startX = seg->startPoint.x,
		startY = seg->startPoint.y,
		startZ = seg->startPoint.z,
		angle1 = child1->angle.x,
		angle2 = child2->angle.x,
		parentHeight = seg->height,
		rot = seg->childRotation;

	const glm::vec3 
		parentPos   = glm::vec3( seg -> startPoint + glm::vec3( seg -> translateMat * glm::vec4( 0, parentHeight, 0, 0 ) ) ),
		childPos1 = child1 -> startPoint,
		childPos2 = child2 -> startPoint;

	/*
	 * Terminology:
	 * Block
	 *	
	 * */

	/*
	 * Making anhor circles
	 * */

	const float 
				sideMargin1      = -radius + radius1 * cosf( abs(angle1) ),
				sideMargin2      = radius - radius2 * cosf( abs(angle2) ),
				topMargin1       = radius1 * sinf( abs(angle1) ),
				topMargin2       = radius2 * sinf( abs(angle2) ),
				blockSideHeight1 = radius,
				blockSideHeight2 = ((topMargin1 - topMargin2) * 2 + blockSideHeight1 * cosf( abs( angle1 ) )) / cosf( abs( angle2 ) ),
				circleStepCount  = quality,

				circleStep = 2.f * M_PI / circleStepCount;

	const glm::mat4 
		blockRotationMatrix = glm::rotate( glm::mat4( 1.0 ), rot,    glm::vec3( 0, 1, 0 ) ),
		blockMatrix         = seg->translateMat * blockRotationMatrix,
		rotationMatrix1     = glm::rotate( glm::mat4( 1.0 ), angle1, glm::vec3( 1, 0, 0 ) ),
		rotationMatrix2     = glm::rotate( glm::mat4( 1.0 ), angle2, glm::vec3( 1, 0, 0 ) );

	float currentRadius, 
				currentTopMargin, 
				currentSideMargin, 
				currentSideHeight,
				currentIndex,
				currentX,
				currentY,
				currentZ;

	glm::mat4 currentRotationMatrix;
	glm::vec4 currentPosition;

	for( int j = 0; j < 3; ++j ){
		
		switch( j ){
			case 0:
				currentRadius = radius1;
				currentSideMargin = sideMargin1;
				currentTopMargin = topMargin1;
				currentSideHeight = blockSideHeight1;
				currentRotationMatrix = rotationMatrix1;
				break;

			case 1:
				currentRadius = radius2;
				currentSideMargin = sideMargin2;
				currentTopMargin = topMargin2;
				currentSideHeight = blockSideHeight2;
				currentRotationMatrix = rotationMatrix2;
				break;

			case 2:
				currentRadius = radius;
				currentSideMargin = 0;
				currentTopMargin = 0;
				currentSideHeight = 0;
				currentRotationMatrix = glm::mat4( 1.0 );
				break;
		}
		
		for( int i = 0; i < circleStepCount; ++i ){
			currentX = currentRadius * cosf( circleStep * i	);
			currentZ = currentRadius * sinf( circleStep * i	);
			currentY = currentSideHeight;

			currentPosition = glm::vec4( currentX, currentY, currentZ, 0 );
			currentPosition = currentRotationMatrix * currentPosition;

			currentPosition.y += parentHeight + currentTopMargin;
			currentPosition.z += currentSideMargin;
			
			currentPosition = blockMatrix * currentPosition;

			currentIndex = addVert( 
					startX + currentPosition.x, 
					startY + currentPosition.y, 
					startZ + currentPosition.z 
			);
			seg->endIndices[ j ][ i ] = currentIndex;
		}
	}

	for( int j = 0; j < 2; ++j ){
		for( int i = 0; i < circleStepCount; ++i ){
			currentIndex = ( !j ? child1 : child2 )->rowIndices[0][ i ];
			seg->endIndices[ j ][ i ] = currentIndex;
		}
	}

	/*
	 * Making back and front cilinders for blocks
	 * */

	const float
		sideHeightStepCount = quality / 4,
		halfCircleStepCount = quality / 2 + 1,
		sideHeightStep1 = blockSideHeight1 / sideHeightStepCount,
		sideHeightStep2 = blockSideHeight2 / sideHeightStepCount,
		radiusStep1 = (radius - radius1) / sideHeightStepCount,
		radiusStep2 = (radius - radius2) / sideHeightStepCount;

	int ci, cj;
	float currentHeightStep;

	const float 
		sideBasis = currentRadius * cosf( abs( angle1 ) ),
		topBasis  = currentRadius * sinf( abs( angle1 ) );

	const int 
		qHalf = quality / 2,
		qQuad = quality / 4;			

	glm::vec3 
		childMargin1,
		childMargin2,
		currentChildMargin,
		currentChildMarginI;

	childMargin1 = (childPos1 - parentPos) / (float)(qQuad + 1);
	childMargin2 = (childPos2 - parentPos) / (float)(qQuad + 1);

	for( int k = 0; k < 2; ++k ){
		if( k == 1 ){
			currentRadius = radius1;
			currentRotationMatrix = rotationMatrix1;
			currentTopMargin = topMargin1;
			currentSideMargin = sideMargin1;	//-radius + radius1 * cos( -angle1 );
			currentHeightStep = sideHeightStep1;
			currentChildMargin = childMargin1;
		}else{
			currentRadius = radius2;
			currentRotationMatrix = rotationMatrix2;
			currentTopMargin = topMargin2;
			currentSideMargin = sideMargin2;
			currentHeightStep = sideHeightStep2;
			currentChildMargin = childMargin2;
		}

		for( int j = 0; j < sideHeightStepCount; ++j ){
			for( int i = 0; i < halfCircleStepCount; ++i ){
				
				currentSideMargin = radius - sideBasis; //radius - currentRadius * cosf( abs( angle1 ) ),
				//currentTopMargin  = topBasis; //currentRadius * sinf( abs( angle1 ) );

				//currentChildMargin += currentChildMargin * ((float)( j + 1 ));
				currentChildMarginI = currentChildMargin * (float)( j + 1 );

				if( k == 0 ){
					ci = i;
					cj = 3 + j;
					currentRadius = radius - radiusStep2 * ( j );
				}else{
					ci = i + qHalf;
					cj = 3 + qQuad + j;
					currentRadius = radius - radiusStep1 * ( j );
					currentSideMargin *= -1;
				}

				currentX = currentRadius * cosf( circleStep * ci );
				currentZ = currentRadius * sinf( circleStep * ci );
				//currentY = currentHeightStep * j;

				currentPosition = glm::vec4( currentX, currentY, currentZ, 0 );
				//currentPosition = currentRotationMatrix * currentPosition;

				//currentPosition.y += parentHeight + currentTopMargin;
				//currentPosition.z += currentSideMargin;
				currentPosition = blockMatrix * currentPosition;
				currentPosition += glm::vec4(currentChildMarginI, 0);
				currentPosition += glm::vec4(parentPos, 0);

				currentIndex = addVert( 
						/*startX + currentPosition[0],
						startY + currentPosition[1], 
						startZ + currentPosition[2] */
						glm::vec3(currentPosition)
				);

				seg->endIndices[ cj ][ i ] = currentIndex;
			}
		}
	}

	/*
	 *
	 * Making sides and tops
	 *
	 * */

		const float 
			blockHeight1 = blockSideHeight1 * cosf( abs( angle1 ) ),
			blockHeight2 = blockSideHeight2 * cosf( abs( angle2 ) ),
			blockApexHeight1 = topMargin1,
			blockApexHeight2 = topMargin2,
			spaceWidth1 = -(radius - radius1 * cosf( abs( angle1 ) ) + blockSideHeight1 * sinf( abs( angle1 ) )),
			spaceWidth2 = (radius - radius2 * cosf( abs( angle2 ) ) + blockSideHeight2 * sinf( abs( angle2 ) )),
			heightDifferent = blockHeight2 - blockHeight1,
			apexWidth1 = spaceWidth1 + radius1 * cosf( abs( angle1 ) ),
			apexWidth2 = spaceWidth2 + radius2 * cosf( abs( angle2 ) ),
			heightStepCount = quality / 4,
			widthStepCount = quality / 4,
			heightStep1 = blockHeight1 / (heightStepCount - 0),
			heightStep2 = blockHeight2 / (heightStepCount - 0),
			diffStep = heightDifferent / heightStepCount / heightStepCount,
			depthStep1 = (radius / 2) / heightStepCount,
			depthStep2 = 0,
			widthStep1 = spaceWidth1 / heightStepCount,
			widthStep2 = spaceWidth2 / heightStepCount,
			apexHeightStep1 = blockApexHeight1 / heightStepCount,
			apexHeightStep2 = blockApexHeight2 / heightStepCount,
			apexWidthStep1 = apexWidth1 / 3,
			apexWidthStep2 = apexWidth2 / 3,
			apexDepthStep1 = 0,
			apexDepthStep2 = 0;

	float currentStartHeightMargin, 
				currentStartSideMargin,
				currentStartDepthMargin,
				//currentHeightStep, 
				currentWidthStep, 
				currentDepthStep,
				currentMult;
	
	float deltaWidth, deltaHeight, moveAngle;

	/*for( int s = 0; s < 2; ++s ){
		for( int j = 0; j < qQuad + 1; ++j ){
			for( int k = 0; k < 1; k++ ){
				for( int i = 0; i < heightStepCount-1; ++i ){

					if( j == 1 || j == 2 ){
						currentMult = 0.35;
					}else{
						currentMult = 0.7;
					}

					if( !k ){
					
						currentStartHeightMargin = parentHeight;
						
						//currentHeightStep = min( heightStep1, heightStep2 );//+ diffStep * j;

						if( j < 2 ){
							currentHeightStep = heightStep1;
							currentWidthStep = widthStep1;
							currentDepthStep = depthStep1;		
							currentStartHeightMargin += topMargin1;
						}else{
							currentHeightStep = heightStep2;
							currentWidthStep = widthStep2;
							currentDepthStep = depthStep1;		
							currentStartHeightMargin += topMargin2;
						}

						if( !s ){
							currentStartDepthMargin = -radius;
							currentDepthStep = depthStep1;
						}else{
							currentStartDepthMargin = radius;
							currentDepthStep = -depthStep1;
						}

						if( j == 1 || j == 2 ){
							currentDepthStep *= 0.5;
						}else{
							currentDepthStep *= 0.1;
						}

						currentStartSideMargin = 0;

					}else{
									//= abs(apexWidth1 * currentMult - currentMult * apexWidth1);
						currentStartHeightMargin = parentHeight;
						currentHeightStep = min( heightStep1, heightStep2 );//+ diffStep * j;
						moveAngle = -M_PI / 2 + M_PI / 2 / heightStepCount * ( i + 0 );
						if( j < 2 ){
							currentStartHeightMargin += min(topMargin1 + blockHeight1, topMargin2 + blockHeight2) + 1;
							currentStartSideMargin = spaceWidth1 * currentMult;
							currentHeightStep = apexHeightStep1;
							currentWidthStep = -widthStep1;
							deltaWidth = abs(apexWidth1 * currentMult - currentMult * spaceWidth1);
							deltaHeight = blockApexHeight1;
						}else{
							currentStartHeightMargin += min(topMargin1 + blockHeight1, topMargin2 + blockHeight2) + 1;
							currentStartSideMargin = spaceWidth2 * currentMult;
							currentHeightStep = apexHeightStep2;
							currentWidthStep = -widthStep1;
							deltaWidth = -abs(apexWidth2 * currentMult - currentMult * spaceWidth2);
							deltaHeight = blockApexHeight2;
						}
						currentStartDepthMargin = radius - 0.4;
						currentDepthStep = -radius / 4;
						if( !s ){
							currentStartDepthMargin *= (-1);
							currentDepthStep *= (-1);	
						}

					}

					if( k == 0 ){
						currentX = currentStartDepthMargin + currentDepthStep * i;
						currentY = currentStartHeightMargin + currentHeightStep * (i + 1);
						currentZ = currentStartSideMargin + currentWidthStep * currentMult * (i + 1);
					}else{
						currentX = currentStartDepthMargin + currentDepthStep * currentMult * (i);
						currentY = currentStartHeightMargin + 1 * deltaHeight * sinf( (moveAngle) );
						currentZ = currentStartSideMargin +  deltaWidth * cosf( moveAngle );
					}

					currentPosition = glm::vec4( currentX, currentY, currentZ, 0 );
					currentPosition = blockMatrix * currentPosition;

					currentPosition.x += startX;
					currentPosition.y += startY;
					currentPosition.z += startZ;

					currentIndex = addVert( currentPosition.x, currentPosition.y, currentPosition.z );
					seg->endSides[ s * 8 + k * 4 + j ][ i ] = currentIndex;
				}
			}
		}
	}*/

	/*
	 *	Sides 2
	 * */

	int cil, cir;
	int sideStep = 8; // widthStepCount * heightStepCount;
	int csi;
	
	for( int s = 0; s < 2; ++s ){
		csi = s * sideStep;
		for( int j = 0; j < widthStepCount; ++j ){
			for( int i = 0; i < heightStepCount; ++i ){
				
				if( !s ){
					cil = quality / 2;
					cir = 0;
				}else{
					cil = 0;
					cir = quality / 2;
				}

				glm::vec3 leftPoint  = vertices[ seg -> endIndices[ 3 + i ][ cil ] ];
				glm::vec3 rightPoint = vertices[ seg -> endIndices[ 3 + qQuad + i ][ cir ] ];
				glm::vec3 distance   = (leftPoint - rightPoint) / (float)(widthStepCount + 1);
				glm::vec3 margin     = rightPoint + distance * (float)( j + 1 );

				currentIndex = addVert( margin );
				seg -> endSides[ csi + i ][ j ] = currentIndex;
				
			}
		}
	}

	int apexStepCount = quality / 4 + 1;

	for( int s = 0; s < 2; ++s ){
		for( int j = 0; j < 4; ++j ){
			for( int i = 0; i < 4; ++i ){
				glm::vec3 firstPoint, secondPoint, diff, step, move;
				float widthStep, heightStep, depthStep, marginDepth, dm;
				int d1 = ( !s ? quality / 2 - i : i );
				int d2 = ( !s ? quality * 1.5 + i:  quality - i );

				int v1 = getRound( seg->endIndices[ 0 ], quality, d1 );
				int v2 = getRound( seg->endIndices[ 1 ], quality, d2 );

				firstPoint = vertices[ v1 ];
				secondPoint = vertices[ v2 ];

				diff = secondPoint - firstPoint;
				step = diff / (float)(apexStepCount + 1);
				widthStep  = step.z; 
				heightStep = step.y; 
				depthStep  = step.x; 

				if( j == 1 || j == 2 ) dm = 0.4; else dm = 0.1;

				marginDepth = (depthStep1 * dm) * 4 / ( i + 1 );
				
				if(s)
					marginDepth *= (-1);

				move = glm::vec3( marginDepth, -abs(marginDepth), 0 );
				move = glm::vec3( blockMatrix * glm::vec4( move, 0 ) );

				currentX = firstPoint.x + depthStep * (j + 1) + move.x; //+ marginDepth;
				currentY = firstPoint.y + heightStep * (j + 1) + move.y; //- marginDepth;
				currentZ = firstPoint.z + widthStep * (j + 1) + move.z;

				currentIndex = addVert( currentX, currentY, currentZ );
				
				seg->endSides[ s * 8 + 4 + j ][ i ] = currentIndex;
				
				if( i == 0 )
					seg->endSides[ s * 8 + j ][ 3 ] = currentIndex;

			}
		}
	}
	
}

void TreeMeshMaker::makeEndIndices( TreeMeshSegment *seg, TreeMeshSegment *child1, TreeMeshSegment *child2 ){

	int **inds = seg->endIndices;

	int quality = seg->quality;

	int ci, ni, ti, nti;

	int margin = quality / 2 - 1;
	int half = quality / 2,
			quad = half / 2;

	//Bind child1 pre

	int *row11 = new int[ half + 1 ],
			*row12 = new int[ half + 1 ];

	int cr = 3,
			pr = cr - 1;
	int j;
	
	for( int i = 0; i < half / 2 + 1; i++ ){

		row11 = new int[ half + 1 ];
		row12 = new int[ half + 1 ];

		if( i == half / 2 ) cr = 1;

		copy( &inds[ cr ][ 0 ], &inds[ cr ][ half + 1 ], row11 );
		copy( &inds[ pr ][ 0 ], &inds[ pr ][ half + 1 ], row12 );
		bindChildInd( half,	row11, row12 );

		pr = cr;
		cr = 3 + i + 1;
	}

	//pr = 2;
	cr = 3 + quality / 4;
	pr = 2;

	for( int i = 0; i < half / 2 + 1; i++ ){

		row11 = new int[ half + 1 ];
		row12 = new int[ half + 1 ];

		if( i == half / 2 ){ 
			cr = 0;
			copy( &inds[ cr ][ half ], &inds[ cr ][ quality ], row11 );
			row11[ half ] = inds[ cr ][ 0 ];
		}else{
			copy( &inds[ cr ][ 0 ], &inds[ cr ][ half + 1 ], row11 );
		}
		
		if( pr == 2 ){
			copy( &inds[ pr ][ half ], &inds[ pr ][ quality ], row12 );
			row12[ half ] = inds[ pr ][ 0 ];
		}else{
			copy( &inds[ pr ][ 0 ], &inds[ pr ][ half + 1 ], row12 );
		}

		bindChildInd( half,	row11, row12 );

		pr = cr;
		cr = 3 + i + 1 + quality / 4;
		
	}

	//Bind sides
	
	row11 = new int[ half + 1 ];
	row12 = new int[ half + 1 ];

	int csi = 0;

	//for( int j = 0; j < quad; ++j ){
		/*for( int i = 0; i < quad; ++i ){
					
		}*/
	//}
	
	const int sideStep = 8; // quad * quad;
	int csr, csl, cst;

	for( int s = 0; s < 2; ++s ){
		csi = s * sideStep;
		for( int i = 1; i < quad; ++i ){
			if( !s ){
				csl = half;
				csr = 0;
				cst = 0;
			}else{
				csl = 0;
				csr = half;
				cst = half - 1;
			}
			
			row11[ 0 ] = inds[ 3 + i - 1 + quad ][ csr ];
			row12[ 0 ] = inds[ 3 + i + quad ][ csr ];

			copy( &seg -> endSides[ i + csi - 1 ][ 0 ], &seg -> endSides[ i + csi - 1 ][ quad ], &row11[1] );
			copy( &seg -> endSides[ i + csi ][ 0 ], &seg -> endSides[ i + csi ][ quad ], &row12[1] );
			
			row11[ quad + 1 ] = inds[ 3 + i - 1 ][ csl ];
			row12[ quad + 1 ] = inds[ 3 + i ][ csl ];

			bindChildInd( quad + 1, row11, row12 );

			if( i == 1 ){
				fill( &row12[ 2 ], &row12[ quad + 2 ], inds[ 2 ][ csl ] );
				bindChildInd( quad + 1, row11, row12 );
			}

			if( i == quad - 1 ){
				for( int j = 0; j < quad + 2; ++j ) row11[ j ] = seg -> endSides[ 4 + j ][ cst ];
				bindChildInd( quad + 1, row11, row12 );
			}
		}
	}
	

	/*for( int s = 0; s < 2; ++s ){
		csi = s * 8;
		copy( &seg->endSides[ csi + quad ][ 0 ], &seg->endSides[ csi + quad ][ half / 2 + 1 ], row11 );
		for( int i = 0; i < half / 2; ++i ) row12[ i ] = inds[ i + 3 ][ !s ? half : 0 ];
		row12[ half / 2 ] = inds[ 1 ][ !s ? half : 0 ];
		bindChildInd( half / 2 , row11, row12 );

		copy( &seg->endSides[ csi + 0 ][ 0 ], &seg->endSides[ csi + 0 ][ half / 2 + 1 ], row11 );
		for( int i = 0; i < half / 2; ++i ) row12[ i ] = inds[ i + quality/4 + 3 ][ s ? half : 0 ];
		row12[ half / 2 ] = inds[ 0 ][ !s ? half : 0 ];
		bindChildInd( half / 2 , row11, row12 );

		for( int i = 0; i < quad; i++ ){
			copy( &seg->endSides[ csi + i ][ 0 ], &seg->endSides[ csi + i ][ half / 2 + 1 ], row11 );
			copy( &seg->endSides[ csi + i + 1 ][ 0 ], &seg->endSides[ csi + i + 1 ][ half / 2 + 1 ], row12 );
			bindChildInd( half / 2 , row11, row12 );
		}
	}*/

	//Bind sides with block bottom

	/*for( int s = 0; s < 2; ++s ){

		fill_n( row11, half/2 + 1, inds[ 2 ][ s ? 0 : half ] );
		for( int i = 0; i < half/2+1; ++i ) row12[i] = seg->endSides[ s * 8 + i ][ 0 ];

		bindChildInd( half / 2, row11, row12 );

		addInd( inds[ 2 ][ s ? 0 : half ] );
		addInd( seg->endSides[ s * 8 + 3 ][ 0 ] );
		addInd( seg->endIndices[ 3 ][ s ? 0 : half ] );

		addInd( inds[ 2 ][ s ? 0 : half ] );
		addInd( seg->endSides[ s * 8 + 0 ][ 0 ] );
		addInd( seg->endIndices[ 6 ][ !s ? 0 : half ] );
	}*/
	
	//Bind top sides
	
	for( int s = 0; s < 2; ++s ){
		csi = s * 8;
		copy( &seg->endSides[ 4 + csi ][ 0 ], &seg->endSides[ 4 + csi ][ half/2 + 1 ], row11 );
		reverse_copy( &inds[ 0 ][ !s ? half/2 : 0 ], &inds[ 0 ][ !s ? half + 1 : half/2 + 1 ], row12 );
		if(s) reverse( &row12[0], &row12[half/2+1] );
		bindChildInd( half/2, row11, row12 );

		copy( &seg->endSides[ 7 + csi ][ 0 ], &seg->endSides[ 7 + csi ][ half/2 + 1 ], row11 );
		copy( &inds[ 1 ][ !s ? half : half / 2 + half - 0 ], &inds[ 1 ][ !s ? half + half/2 + 1 : half * 2 + 0 ], row12 );
		if(s){ 
			reverse( &row12[0], &row12[half/2+1] );
			row12[ 0 ] = inds[ 1 ][ 0 ];
		}
		bindChildInd( half/2, row11, row12 );

		for( int i = 0; i < 3; ++i ){
			copy( &seg->endSides[ 4 + csi + i ][ 0 ], &seg->endSides[ 4 + csi + i ][ half / 2 + 1 ], row11 );
			copy( &seg->endSides[ 4 + csi + i + 1 ][ 0 ], &seg->endSides[ 4 + csi + i + 1 ][ half / 2 + 1 ], row12 );
			bindChildInd( half / 2 , row11, row12 );
		}
	}

	//Bind chid2 pre

	int *row21 = new int[ half + 1 ],
			*row22 = new int[ half + 1 ];
	/*copy( &inds[ 2 ][ 0 ], &inds[ 2 ][ half+1 ], &row21[0] );
	copy( &inds[ 1 ][ 0 ], &inds[ 1 ][ half+1 ], &row22[0] );
	bindChildInd( half, row21, row22 );*/

	//Bind Sides

	/*addInd( row11[0] );
	addInd( row12[0] );
	addInd( row22[ half ] );

	addInd( row11[ half ] );
	addInd( row12[ half ] );
	addInd( row22[ 0 ] );*/

	/*for( auto tr : seg.endTri ){
		bindTriangle(tr);
	}*/

	delete[] row11;
	delete[] row12;
	delete[] row21;
	delete[] row22;

	row11 = new int[ half + 2 ];
	row12 = new int[ half + 2 ];

	copy( &inds[0][0], &inds[0][ half + 1 ], row11 );
	copy( &inds[1][half], &inds[1][ quality ], row12 );
	std::reverse( &row11[0], &row11[half+1] );
	row12[half] = inds[1][0];
	//bindChildInd( half-0, row11, row12 );

	delete[] row11;
	delete[] row12;

	//Bind end with main brnch
	row11 = new int[ quality + 1 ];
	row12 = new int[ quality + 1 ];
	margin = -quality * (seg->childRotation / (2.f * M_PI)) - 1;
	for(int i = 0; i < quality+1; i++){
		row11[i] = getRound( seg->rowIndices[quality-1], quality, margin + i );
		row12[i] = getRound( inds[2], quality, i );
	}
	bindChildInd( quality, row11, row12 );
	delete[] row11;
	delete[] row12;

	//Bind children 
	for( int i = 0; i < 2; i++ ){
		TreeMeshSegment *currentChild = ( i == 0 ) ? child1 : child2;
		row11 = new int[ quality + 1 ];
		row12 = new int[ quality + 1 ];
		copy( &currentChild->rowIndices[0][0], &currentChild->rowIndices[0][ quality ], row11 );
		copy( &inds[i][0], &inds[i][ quality ], row12 );
		row11[quality] = currentChild->rowIndices[0][0];
		row12[quality] = inds[i][0];
		//bindChildInd( quality, row11, row12 );
		delete[] row11;
		delete[] row12;
	}

}

void TreeMeshMaker::makeEndNormals( TreeMeshSegment *seg ){

	//normalsMutex.lock();
	normals.resize( vertices.size() );
	//normalsMutex.unlock();
	
	return;

	const int quality = seg->quality;
	const float rotationStep = 2.f * M_PI / quality;
	const float rotationStart = -M_PI / 2.f;
	
	glm::vec3 currentVec;
	glm::mat4 xMat, zMat;
	

	//Anchor circles
	
	for( int j = 2; j < 3; ++j ){
		for( int i = 0; i < quality; ++i ){
			
			currentVec = glm::vec3( 0, 1, 0 );
			xMat = glm::rotate( glm::mat4(1.0f), (float)M_PI / 2.0f, glm::vec3(1,0,0) );
			zMat = glm::rotate( glm::mat4(1.0f), (float)( rotationStep * i + rotationStart ), glm::vec3(0,0,1) );
			currentVec = glm::vec3( seg->getChildTranslateMat() * xMat * zMat * glm::vec4( currentVec, 0 ));
			
			normalsMutex.lock();
			normals[ seg->endIndices[ j ][ i ] ] = currentVec;
			normalsMutex.unlock();

		}
	}

	//Back front sides
	
	float startAngle, rotAngle;
	int cj, ci;

	for( int k = 0; k < 2; ++k ){
		for( int j = 0; j < quality / 4; ++j ){
			for( int i = 0; i < quality/2 + 1; ++i ){

				startAngle = ( !k ? 0 : -M_PI ) - M_PI / 2;
				rotAngle = 2.f * M_PI / quality * i + startAngle;
									
				currentVec = glm::vec3( 0, 1, 0 );
				xMat = glm::rotate( glm::mat4(1.0f), (float)M_PI / 2.0f, glm::vec3(1,0,0) );
				zMat = glm::rotate( glm::mat4(1.0f), (float)(rotAngle), glm::vec3(0,0,1) );
				currentVec = glm::vec3(seg->getChildTranslateMat() * xMat * zMat *  glm::vec4( currentVec, 0 ));

				if( k == 0 ){
					ci = i;
					cj = 3 + j;
				}else{
					ci = i + quality / 2;
					cj = 3 + quality / 4 + j;
				}

				normals[ seg->endIndices[ cj ][ i ] ] = currentVec;
			}
		}
	}

	//Sides
	
	float sideStepCount = quality / 4 + 1;
	float za, xa,coff;

	for( int s = 0; s < 2; ++s ){
		for( int j = 0; j < sideStepCount; ++j ){
			for( int i = 0; i < sideStepCount; ++i ){

				/*xa = ( j == 1 || j == 2 ) ? xa = 0 : M_PI  / 6;
				xa = ( j < 2 ) ? xa : xa * -1;
				za = -M_PI / 8.f * ( 3 - i );
				if( !s ) za *= -1;*/

				coff = 1 / (sideStepCount-1) * i; 

				xa = ( j == 1 || j == 2 ) ? M_PI / 9.f : M_PI / 15.f;
				xa *= coff;
				if( j < 2 ) xa *= -1;
				za = ( coff ) * M_PI / 9.f;
				if( !s ) za *= -1;
				if( !s ) xa *= -1;

				currentVec = glm::vec3( 0, 1, 0 );
				xMat = glm::rotate( glm::mat4(1.0f), (float)xa, glm::vec3(0,1,0) );
				zMat = glm::rotate( glm::mat4(1.0f), (float)( M_PI / 2.f * (s ? -1 : 1) + za ), glm::vec3(0,0,1) );
				currentVec = glm::vec3( seg->getChildTranslateMat() * xMat * zMat * glm::vec4( currentVec, 0 ));

				normals[ seg->endSides[s * 8 + j ][ i ] ] = currentVec;
			}
		}
	}

	//Apexes

	
	for( int s = 0; s < 2; ++s ){
		for( int j = 0; j < sideStepCount; ++j ){
			for( int i = 0; i < sideStepCount; ++i ){

				xa = ( j == 1 || j == 2 ) ? xa = 0 : M_PI  / 6;
				xa = ( j < 2 ) ? xa : xa * -1;
				//if( s ) xa = xa * -1;

				za = -M_PI / 8.f * ( 3 - i );
				if( !s ) za *= -1;

				currentVec = glm::vec3( 0, 1, 0 );
				xMat = glm::rotate( glm::mat4(1.0f), (float)xa, glm::vec3(1,0,0) );
				zMat = glm::rotate( glm::mat4(1.0f), (float)za, glm::vec3(0,0,1) );
				currentVec = glm::vec3(seg->getChildTranslateMat() * xMat * zMat *  glm::vec4( currentVec, 0 ));

				normals[ seg->endSides[s * 8 + 4 + j ][ i ] ] = currentVec;
			}
		}
	}

}

void TreeMeshMaker::makeEndTexCords( TreeMeshSegment *seg ){

	float sc, ec;
	sc = 0; ec = 1;		
	int q = seg->quality;

	texCordsMutex.lock();
	texCords.resize( vertices.size() );
	texCordsMutex.unlock();

	return;

	//Anhor Point

	int ci;

	float iStep = 1.f / q,
				iStepF = 1.f / ( q + q / 2 );
	float cx, cy;

	for( int j = 0; j < 3; ++j ){
		for( int i = 0; i < seg->quality; ++i ){
			ci = seg->endIndices[ j ][ i ];
			if( i == 2 ){
				cx = iStep * i;
				cy = 1;
			}else if( i == 0 ){
				cx = iStep * i;
				cy = 0;
			}else if( i == 1 ){
				cx = iStep * i;
				cy = 0;	
			}

			texCordsMutex.lock();
			texCords[ ci ] = glm::vec2( cx, cy );
			texCordsMutex.unlock();
		}
	}

	//Back front
	
	float heightStepCount = q / 4,
				hStep = 1.f / heightStepCount;
	
	for( int k = 0; k < 2; ++k ){
		for( int j = 0; j < heightStepCount; ++j ){
			for( int i = 0; i < q / 2; ++i ){
				ci = seg->endIndices[ k * q / 4 + j + 3 ][ i ];
				cx = iStepF * i;
				cy = hStep * j;
				texCords[ ci ] = glm::vec2( cx, cy );
			}
		}
	}

	heightStepCount = q / 4 + 1;

	for( int s = 0; s < 2; ++s ){
		for( int j = 0; j < heightStepCount; ++j ){
			for( int i = 0; i < heightStepCount; ++i ){
				ci = seg->endSides[ s * 8 + j ][ i ];
				texCords[ ci ] = glm::vec2( iStepF * ( 4 + i ), hStep / 2 * j );
			}	
		}
	}

	for( int s = 0; s < 2; ++s ){
		for( int j = 0; j < heightStepCount; ++j ){
			for( int i = 0; i < heightStepCount; ++i ){
				ci = seg->endSides[ s * 8 + 4 + j ][ i ];
				texCords[ ci ] = glm::vec2( iStepF * ( 4 + i ), hStep / 2 * (j+4) );
			}	
		}
	}

}

glm::vec3 TreeMeshMaker::getCilinderPoint( TreeMeshSegment *seg, float positionX, float positionY ){

	const float
		startRadius = seg->startRadius,
		endRadius   = seg->endRadius;
	
	const float 
		currentRadius = startRadius + ( endRadius - startRadius ) * positionY,
		currentAngle  = 2.f * M_PI * positionX;

	const glm::vec3 
		centerPoint = getBezierPoint( positionY, glm::vec3( 0 ), glm::vec3( 0, seg->height, 0 ), seg->curvePoint ),
		edgePoint = centerPoint + glm::vec3(
			currentRadius * cosf( currentAngle ),
			0,
			currentRadius * sinf( currentAngle )
		),
		transformedPoint = seg->startPoint + glm::mat3( seg->translateMat ) * edgePoint;

	return transformedPoint;

}

void TreeMeshMaker::makeOuthgrouthBind( TreeMeshSegment *par, TreeMeshSegment *out ){
	
	const int
		pQuality        = par->quality,
		oQuality        = out->quality,
		circleStepCount = oQuality;
	
	const float
		oRadius      = out->startRadius,
		pStartRadius = par->startRadius,
		pEndRadius   = par->endRadius,
		pHeight      = par->height,
		pRadius      = pEndRadius + ( pStartRadius - pEndRadius ) * ( oRadius / pHeight ),
		positionX    = out->positionX,
		positionY    = out->positionY;

	int i, 
			j, 
			currentIndex,
			prevIndex,
			basisIndex,
			stepCount;
	
	float
			currentPositionX,
			currentPositionY,
			quadPositionX,
			quadPositionY,
			stepX,
			stepY,
			currentRotation;

	glm::vec3 currentPoint, nextPoint, currentNormal;
	
	//Making base quad on parent surface
	
	const float 
		basisScaleFactor = 0.3f,
		quadWidth        = oRadius / pRadius * basisScaleFactor,
		quadHeight       = oRadius / pHeight * basisScaleFactor,
		rStep            = 2.f * M_PI / oQuality,
		rMargin          = M_PI * 1.2;

	const int quadStep = oQuality / 4;

	const float quadVertices[] = {
		-quadWidth, -quadHeight,
		-quadWidth,  quadHeight,
		quadWidth,  quadHeight,
		quadWidth, -quadHeight
	};

	cout << "QW" << quadWidth << " " << quadHeight << endl;

	out -> outgrouthBindIndices.resize( 2, vector< int >() );

	for( i = 0; i < 4; ++i ){

		quadPositionX = positionX + quadVertices[ i * 2 + 0 ];
		quadPositionY = positionY + quadVertices[ i * 2 + 1 ];
		
		if( i < 3 ){
			currentPositionX = positionX + quadVertices[ (i + 1) * 2 + 0 ];
			currentPositionY = positionY + quadVertices[ (i + 1) * 2 + 1 ];
		}else{
			currentPositionX = positionX + quadVertices[ 0 ];
			currentPositionY = positionY + quadVertices[ 1 ];
		}

		stepX = (currentPositionX - quadPositionX) / ( quadStep + 1 );
		stepY = (currentPositionY - quadPositionY) / ( quadStep + 1 );

		for( j = 0; j < quadStep; ++j ){
			currentPoint = getCilinderPoint( par, quadPositionX + stepX * ( j + 1), quadPositionY + stepY * ( j + 1 ) );
			currentIndex = addVert( currentPoint );
			//addInd( currentIndex );
			out -> outgrouthBindIndices[ 0 ].push_back( currentIndex );
		}
		
		/*currentPoint = getCilinderPoint( par, quadPositionX, quadPositionY );
		currentIndex = addVert( currentPoint );
		out -> outgrouthBindIndices[ 0 ].push_back( currentIndex );*/
	}

	for( i = 0; i < oQuality; ++i ){
		
		currentRotation = i * rStep;
		currentPositionX = 3 * oRadius * cosf( currentRotation + rMargin );
		currentPositionY = 3 *  oRadius * sinf( currentRotation + rMargin );

		currentPoint = out -> startPoint + glm::mat3( out -> translateMat ) * glm::vec3( currentPositionX, 0, currentPositionY );
		currentIndex = addVert( currentPoint );

		out -> outgrouthBindIndices[ 1 ].push_back( currentIndex );

	}

	out -> outgrouthBindIndices[ 1 ] = out -> outgrouthBindIndices[ 0 ];

	//bindChildInd( 8, &out -> outgrouthBindIndices[ 0 ][0], &out -> outgrouthBindIndices[ 1 ][0] );

	/*for( i = 0; i < oQuality - 1; ++i ){

		j = i / quadStep;

		basisIndex = out -> outgrouthBindIndices[ 0 ][ j ];

		currentIndex = out -> outgrouthBindIndices[ 1 ][ i + 1 ];
		prevIndex = out -> outgrouthBindIndices[ 1 ][ i ];

		addInd( prevIndex );
		addInd( currentIndex );
		addInd( basisIndex );

		if( j != (i+1) / quadStep ){
		}
	}*/

	normals.resize( vertices.size() );
	texCords.resize( vertices.size() );

	/*const float
		xRadius = oRadius / (M_PI * pRadius),
		yRadius = oRadius / pHeight * 1.5,
		rStep   = 2.f * M_PI / oQuality,
		rotAdd = M_PI / 1.f;
	
	int i, 
			j, 
			currentIndex,
			stepCount;
	
	float
			currentPositionX,
			currentPositionY;

	glm::vec3 currentPoint, currentNormal;

	stepCount = 0;

	for( j = 0; j < 1; ++j ){	
		//TODO: more smoothy for big radius outgrouths
		out->outgrouthBindIndices.resize( 2, vector<int>() );
		for( i = 0; i < circleStepCount; ++i ){
			
			currentPositionX = positionX + xRadius * cosf( rStep * i + rotAdd );
			currentPositionY = positionY + yRadius * sinf( rStep * i + rotAdd );

			float nextPositionX = positionX + xRadius * cosf( rStep * (i+1) + rotAdd );
			int curPos = floor(currentPositionX * pQuality);
			int nextPos = floor(nextPositionX * pQuality);

			if( floor(currentPositionX * pQuality) < floor(nextPositionX * pQuality) ){
				float additionPositionX = floor( nextPositionX * pQuality ) / pQuality;
				glm::vec3 currentPointA = getCilinderPoint( par, additionPositionX, currentPositionY );
				currentIndex = addVert(
					currentPointA.x + j,
					currentPointA.y,
					currentPointA.z
				);
				out->outgrouthBindIndices[j].push_back( currentIndex );
				
				if( j == 0 ) currentNormal = normals[ floor(currentPositionX * pQuality)];
				if( j == 1 ) currentNormal = normals[ out -> rowIndices[ 0 ][ curPos ] ];

				normals.push_back( currentNormal );
				
				++stepCount;
			}

			currentPoint = getCilinderPoint( par, currentPositionX, currentPositionY );

			currentIndex = addVert(
				currentPoint.x + j,
				currentPoint.y,
				currentPoint.z
			);

			if( j == 0 ) normals.push_back( normals[floor(currentPositionX * pQuality)]);
			if( j == 1 ) normals.push_back( normals[ out -> rowIndices[ 0 ][ curPos ] ] );

			out->outgrouthBindIndices[j].push_back( currentIndex );
			++stepCount;
		}
	}

	for( i = 0; i < stepCount; ++i ){
		currentPoint = glm::vec3(
			oRadius * cosf( 2.f * M_PI * i / stepCount ),
			0,
			oRadius * sinf( 2.f * M_PI * i / stepCount )
		);
		currentPoint = out -> startPoint + glm::vec3(0) + glm::vec3( glm::mat3( out -> translateMat ) * currentPoint );
		currentIndex = addVert( currentPoint );
		out -> outgrouthBindIndices[ 1 ].push_back( currentIndex );
	}

	///bindChildInd( stepCount - 1, &out->outgrouthBindIndices[0][0], &out->rowIndices[0][0] );
	
	out -> outgrouthBindIndices[ 1 ].push_back( out -> outgrouthBindIndices[ 1 ][ 0 ] );
	out -> outgrouthBindIndices[ 0 ].push_back( out -> outgrouthBindIndices[ 0 ][ 0 ] );
	bindChildInd( stepCount, &out->outgrouthBindIndices[0][0], &out->outgrouthBindIndices[1][0] );
	bindChildInd( 1, &out->outgrouthBindIndices[0][0], &out->outgrouthBindIndices[1][0] );
	
	for( j = 0; j < 0; ++j ){
		for( i = 0; i < stepCount; ++i ){
			glm::mat4 tm1 = glm::rotate( glm::mat4(1), 2.f * static_cast<float>(M_PI * i / stepCount - M_PI * 0.f), glm::vec3( 0, 0, 1 ) );
			glm::mat4 tm2 = glm::rotate( glm::mat4(1), static_cast<float>(M_PI) / 2.f, glm::vec3( 0, 1, 0 ) );

			glm::vec3 nrm = glm::vec3( out -> translateMat * glm::vec4( 0, 1, 0, 0 ) );

			normals.push_back( nrm );
		}
	}
	texCords.resize( vertices.size() );*/

		
}

void TreeMeshMaker::makeFinish( TreeMeshSegment *seg ){
	
	const int quality = seg->quality;
	
	const float 
		radius = seg->endRadius,
		height = seg->height,
		finishHeight = radius * tanf( M_PI * 0.3 );	
	
	const glm::mat4 mat = seg->translateMat;

	int finishIndex;
	int *rowIndices1 = new int[ quality + 1 ];
	int *rowIndices2 = new int[ quality + 1 ];
	int *inds = seg->rowIndices[ quality - 1 ];

	glm::vec3 finishVertex = glm::vec3( 0, height + finishHeight, 0 );
	finishVertex = glm::mat3( mat ) * finishVertex;
	finishVertex += seg->startPoint;

	finishIndex = addVert( finishVertex.x, finishVertex.y, finishVertex.z );

	copy( &rowIndices1[ 0 ], &rowIndices1[ quality ], inds );	
	rowIndices1[ quality ] = inds[ 0 ];
	fill( &rowIndices2[ 0 ], &rowIndices2[ quality ], finishIndex );

	bindChildInd( quality, rowIndices1, rowIndices2 );

	delete[] rowIndices1;
	delete[] rowIndices2;

}

glm::vec3 TreeMeshMaker::getChildPos( TreeMeshSegment *par, float angle, float radius, int n ){

	glm::vec3 cp;
	
	if( n == 0 ){

		cp = par->startPoint + glm::mat3( par->translateMat ) * glm::vec3( 0, par -> height, 0 );
	
	}else{

		float a = abs( angle );

		float 
			sideMargin = par -> endRadius - radius * cosf( a ),
			topMargin  = radius * sinf( a ),
			sideHeight = par -> endRadius * 1 + radius * 1;

		if( angle < 0 ) sideMargin *= -1;

		glm::vec4 cv = glm::vec4(
			0,
			sideHeight,
			0,
			0
		);
		glm::mat4 rot = glm::rotate( glm::mat4(1.0), angle, glm::vec3(1, 0, 0) );
		
		cv = rot * cv;

		cv.z += sideMargin;
		cv.y += par->height + topMargin;
		cv = par->getChildTranslateMat() * cv;

		cp = glm::vec3(cv);

		cp.x += par->startPoint.x;
		cp.y += par->startPoint.y;
		cp.z += par->startPoint.z;

	}

	return cp;
}

glm::mat4 TreeMeshMaker::getCurveMat( TreeMeshSegment *par, bool isStart ){
	const glm::vec3 curvePoint = par->curvePoint;
	float tanBasis = isStart ? (curvePoint.y) : (par->height - curvePoint.y),
				tanSideX = curvePoint.x,
				tanSideZ = curvePoint.z;

	float angleX = atan2f( tanSideX, tanBasis ),
				angleZ = atan2f( tanSideZ, tanBasis );

	glm::mat4  
		matX     = ( glm::rotate( glm::mat4(1), -angleZ, glm::vec3( 1, 0, 0 ) ) ),
		matZ     = ( glm::rotate( glm::mat4(1), angleX, glm::vec3( 0, 0, 1 ) ) ),
		finalMat = matX * matZ;


	return finalMat;
}

const glm::vec3 TreeMeshMaker::getOutgrouthStartPos( TreeMeshSegment *seg, int a, int b ){
	cout << "Old get out pos, not use it" << endl;
	return glm::vec3(0,0,0);
}

const glm::vec3 TreeMeshMaker::getOutgrouthStartPos( TreeMeshSegment *seg, TreeMeshSegment *out ){

	//return vertices[ seg->rowIndices[ vpos ][ hpos ] ];	
	
	const float 
		positionX = out -> positionX,
		positionY = out -> positionY,
		margin    = out -> startRadius * sinf( abs( out -> angle.x ) );
	
	const float
		startRadius   = seg -> startRadius,
		endRadius     = seg -> endRadius,
		currentRadius = -margin + startRadius + ( endRadius - startRadius ) * positionY,
		currentAngle  = 2.f * M_PI * positionX;

	const glm::vec3 
		centerPoint   = getBezierPoint( positionY, glm::vec3( 0 ), glm::vec3( 0, seg->height, 0 ), seg -> curvePoint ),
		edgePoint     = centerPoint + glm::vec3(
			currentRadius * cosf( currentAngle ),
			0,
			currentRadius * sinf( currentAngle )	
		),
		sidePoint     = seg -> startPoint + glm::mat3( seg -> translateMat ) * edgePoint;

	return sidePoint;
}

glm::mat4 TreeMeshMaker::getStartOutgrouthMat( TreeMeshSegment *seg, int hpos, int vpos ){
	glm::vec3 currentVertex, besideVertex, direction;
	float xAngle, yAngle, zAngle;
	glm::mat4 xRot, yRot, zRot;

	xAngle = M_PI / 2;
	zAngle = M_PI * 2.f / seg->quality * hpos - M_PI / 2;
	
	xRot = glm::rotate( glm::mat4(1.0), xAngle, glm::vec3( 1, 0, 0 ) );
	zRot = glm::rotate( glm::mat4(1.0), zAngle, glm::vec3( 0, 0, 1 ) );

	return seg->translateMat * xRot * zRot;
}

void TreeMeshMaker::bindChildren( TreeMeshSegment *par, TreeMeshSegment *c1, TreeMeshSegment *c2 ){
	makeEndVertices( par, c1, c2 );
	makeEndIndices( par, c1, c2 );
	makeEndNormals( par );
	makeEndTexCords( par );
}

void TreeMeshMaker::bindChild( TreeMeshSegment *par, TreeMeshSegment *child ){
	const int quality = par->quality;
	int *pri, *cri;
	int *rowIndices1, *rowIndices2; 

	pri = par->rowIndices[ quality - 1 ];
	cri = child->rowIndices[ 0 ];
	rowIndices1 = new int[ quality + 1 ];
	rowIndices2 = new int[ quality + 1 ];

	copy( &pri[0], &pri[ quality ], rowIndices1 );
	copy( &cri[0], &cri[ quality ], rowIndices2 );

	rowIndices1[ quality ] = pri[0];
	rowIndices2[ quality ] = cri[0];

	bindChildInd( quality, rowIndices1, rowIndices2 );	
}

//Full segment
	
void TreeMeshMaker::makeSegment( TreeMeshSegment *cur ){
	makeCilinderVertices( cur );	
	makeCilinderIndices( cur );	
	makeCilinderNormals( cur );

	//thread tn( &TreeMeshMaker::makeCilinderNormals, this, cur );
	//tn.join();
	
	//thread tc( &TreeMeshMaker::makeCilinderTexCords, this, cur );
	//tc.join();

	makeCilinderTexCords( cur );
	//makeBinderVertices( cur );
}

void TreeMeshMaker::makeTestSegment(){

	int q = 8;

	auto tms = new TreeMeshSegment();
	tms->setHeight( 20 );
	tms->setRadius( 3, 2 );
	tms->setQuality( q );
	tms->setStartPoint( glm::vec3(0, 0, 0) );
	tms->setCurvePoint( glm::vec3( 0, 10, 4 ) );
	tms->setAngle( glm::vec3( M_PI * 0.0f, 0, M_PI * 0 ) );
	tms->setChildRotation( 3.14f );
	tms->setTexCords( 0, 1 );

	makeSegment( tms );

	/*auto tms2 = new TreeMeshSegment();
	tms2->setHeight( 10 );
	tms2->setRadius( 1.4, 1.2 );
	tms2->setQuality( q );
	tms2->setStartPoint( glm::vec3(0, 28, 3) );
	tms2->setStartPoint( getChildPos( tms, M_PI * 0.3f, 1.4, 2 ) );
	tms2->setCurvePoint( glm::vec3( 1, 5, 2 ) );
	tms2->setAngle( glm::vec3( M_PI * 0.2f, 0, 0.0f ) );
	tms2->setParentMat( tms->getChildTranslateMat() * getCurveMat( tms2, true ) );
	tms2->setChildRotation( M_PI / 3 );
	tms2->setTexCords( 0, 1 );

	makeSegment( tms2 );
	//bindChild( tms2, tms );

	auto tms3 = new TreeMeshSegment();
	tms3->setHeight( 10 );
	tms3->setRadius( 1.9, 1.7 );
	tms3->setQuality( q );
	tms3->setStartPoint( getChildPos( tms, -M_PI * 0.0f, 1.9, 1 ) );
	tms3->setEndPoint( glm::vec3( 0, 30, 0 ) );
	tms3->setAngle( glm::vec3( -M_PI * 0.0f, 3.14f / 2*0,  3.14f * 0.0f ) );
	tms3->setCurvePoint( glm::vec3( 0, 5, 0 ) );
	tms3->setParentMat( tms->getChildTranslateMat() );

	makeSegment( tms3 );
	bindChildren( tms, tms3, tms2 );
	//makeFinish( tms2 );
	//bindChild( tms3, tms );

	auto tms4 = new TreeMeshSegment();
	tms4->setHeight( 6 );
	tms4->setRadius( 1.1, 0.7f );
	tms4->setQuality( 12 );
	tms4->setStartPoint( getChildPos( tms2, -M_PI * 0.1f, 1.1f, 1 ) );
	tms4->setAngle( glm::vec3( -M_PI * 0.1f, 0, 3.14f * 0.0f ) );
	tms4->setCurvePoint( glm::vec3( -0, 3, 0 ) );
	tms4->setParentMat( tms2->getChildTranslateMat() );

	//makeSegment( tms4 );
	//bindChild( tms3, tms );

	auto tms5 = new TreeMeshSegment();
	tms5->setHeight( 5 );
	tms5->setRadius( 1.1f, 0.7f );
	tms5->setQuality( 12 );
	tms5->setStartPoint( getChildPos( tms2, M_PI * 0.1f, 1.1f, 2 ) );
	tms5->setCurvePoint( glm::vec3( 0, 2.5f, 0 ) );
	tms5->setAngle( glm::vec3( M_PI * 0.1f, 0,  0 ) );
	tms5->setParentMat( tms2->getChildTranslateMat() );

	//makeSegment( tms5 );
	//bindChildren( tms2, tms4, tms5 );

	//Outgrouth from tms
	auto tms6 = new TreeMeshSegment();
	tms6->setHeight( 5 );
	tms6->setRadius( 0.4, 0.2 );
	tms6->setQuality( 12 );
	tms6->setCurvePoint( glm::vec3( 0, 5, 0 ) );
	tms6->setAngle( glm::vec3( -M_PI / 4, 0, M_PI * 0 ) );
	tms6->setParentMat( this->getStartOutgrouthMat( tms2, 7, 6 ) );
	tms6->setOutgrouthPositions( 7.f / 12.f, 6.f / 12.f );
	tms6->setStartPoint( getOutgrouthStartPos( tms2, tms6 ) );

	//makeSegment( tms6 );
	//makeOuthgrouthBind( tms2, tms6 );

	auto tms7 = new TreeMeshSegment();
	tms7->setHeight( 5 );
	tms7->setRadius( 1.2f, 1.1f );
	tms7->setQuality( 12 );
	tms7->setCurvePoint( glm::vec3( 0, 5, 0 ) );
	tms7->setAngle( glm::vec3( 0, 0, M_PI * 0 ) );
	tms7->setParentMat( tms2->translateMat * getCurveMat( tms2, false ) );
	tms7->setStartPoint( getChildPos( tms2, 0.f, 0, 0 )  );

	//makeSegment( tms7 );
	//bindChild( tms2, tms7 );*/

}

void TreeMeshMaker::makeTestTree(){
	//genTestSegment();	
	genS( nullptr );
}

void TreeMeshMaker::genS( TreeMeshSegment *seg ){

	float pHeight, pRadius, height, startRadius, endRadius, oPosX, oPosY;
	glm::vec3 pCurve, pAngle, curve, angle, startPoint;

	if( seg ){
		pHeight = seg -> height;
		pRadius = seg -> endRadius;
		pCurve  = seg -> curvePoint;
		pAngle  = seg -> angle;
	}else{
		pHeight = 1.1f * 6.f;
		pRadius = 0.1f * 6.f;
		pCurve  = glm::vec3( 0.f, 0.5f, 0.1f );
		pAngle  = glm::vec3( M_PI * 0.1f, 0, 0 );
	}

	height      = pHeight * 0.9;
	startRadius = pRadius;
	endRadius   = startRadius * 0.9;
	//curve = -pCurve * 0.9f;
	angle       = - pAngle * 0.9f;

	startPoint = seg ? getChildPos( seg, angle.x, startRadius, 0 ) : glm::vec3( 0 );

	auto tms = new TreeMeshSegment();
	tms -> setHeight( height );
	tms -> setRadius( startRadius, endRadius );
	tms -> setQuality( 12 );
	tms -> setStartPoint( startPoint );
	tms -> setCurvePoint( curve );
	tms -> setAngle( angle );
	tms -> setTexCords( 0, 1 );
	
	if( seg ) tms -> setParentMat( seg -> translateMat );

	makeSegment( tms );
	if( seg ) bindChild( seg, tms );

	oPosX = angle.x < 0 ? 0.9f : 0.4f;
	oPosY = 0.7f;

	auto cld = new TreeMeshSegment();
	cld -> setHeight( height * 2 );
	cld -> setRadius( startRadius / 1.5f, endRadius / 1.8f );
	cld -> setQuality( 12 );
	cld -> setCurvePoint( glm::vec3( 0, 0.5f, 0.6f ) );
	cld -> setAngle( glm::vec3( -M_PI * 0.1f, 0, 0 ) );
	cld -> setTexCords( 0, 1 );
	cld -> setOutgrouthPositions( oPosX, oPosY );
	cld -> setStartPoint( getOutgrouthStartPos( tms, cld ) );
	cld -> setParentMat( getStartOutgrouthMat( tms, 12 * oPosX, 12 * oPosY ) );
	
	if( endRadius >= 0.1 * 3 ) genS( tms );
	makeSegment( cld );
	makeOuthgrouthBind( tms, cld );

}

void TreeMeshMaker::genTestSegment(){
	auto tms = new TreeMeshSegment();
	tms -> setHeight( 5 );
	tms -> setRadius( 0.8f, 0.7f );
	tms -> setQuality( 12 );
	tms -> setStartPoint( glm::vec3(0, 0, 0) );
	tms -> setCurvePoint(glm::vec3( 0, 5, 0.1 ));
	tms -> setAngle(glm::vec3( M_PI * 0.0f, 0, M_PI * 0 ));
	tms -> setTexCords( 0, 1 );

	//thread ms( &TreeMeshMaker::makeSegment, this, tms );
	//ms.join();
	makeSegment( tms );

	//genChildren( tms );
	
	float coef, ph, pw;

	const int hc = 6, wc = 5;
	
	/*for( int i = 0; i < hc; ++i ){
		for( int j = 0; j < wc; ++j ){
			cout << "1111" << endl;

			coef = 1.f - 1.f / (hc) * (i);
			ph   = 0.7f / (hc) * (i) + 0.25f;
			pw   = 1.f / wc * j;

			auto out = new TreeMeshSegment();
			out -> setHeight( coef * 3.f + 3.f );
			out -> setRadius( sqrt(coef) * 0.2f, sqrt(coef) * 0.01f );
			out -> setQuality( 12 );
			//out -> setStartPoint( glm::vec3(0, 0, 0) );
			out -> setCurvePoint(glm::vec3( 0, 0, 0.1 ));
			out -> setAngle(glm::vec3( M_PI * 0.1f, 0, M_PI * 0 ));
			out -> setTexCords( 0, 1 );
			out -> setParentMat( getStartOutgrouthMat( tms, 1.f / wc * j * 12, 1.f / hc * i * 12 ) );
			out -> setOutgrouthPositions( pw, ph );
			out -> setStartPoint( getOutgrouthStartPos( tms, out ) );

			makeSegment( out );

			genBB( out );

			//makeOuthgrouthBind( tms, out );
		}
	}*/
}

void TreeMeshMaker::genBB( TreeMeshSegment *par ){

	if( par -> startRadius <= 0.02 ) return;

	float
		pHeight = par -> height,
		psr     = par -> startRadius,
		per     = par -> endRadius;

	float coef, ph, pw, cr;

	const int hc = 6, wc = 2;
	
	for( int i = 0; i < hc; ++i ){
		for( int j = 0; j < wc; ++j ){

			coef = 1.f - 1.f / (hc) * (i);
			ph   = 0.5f / (hc) * (i) + 0.50f;
			pw   = 1.f / wc * j;
			cr = psr + ( per - psr ) * (1.f - coef);

			auto out = new TreeMeshSegment();

			out -> setHeight( pHeight * sqrt(coef) * 0.4 );
			out -> setRadius( cr * 0.35f, cr * 0.001f );
			out -> setQuality( 12 );
			out -> setCurvePoint(glm::vec3( 0, 0, 0.0 ));
			out -> setAngle(glm::vec3( -M_PI * 0.25f, M_PI * 0.5, 0 ));
			out -> setTexCords( 0, 1 );
			out -> setParentMat( getStartOutgrouthMat( par, 1.f / wc * j * 12, 1.f / hc * i * 12 ) );
			out -> setOutgrouthPositions( pw, ph );
			out -> setStartPoint( getOutgrouthStartPos( par, out ) );

			makeSegment( out );
			genBB( out );
		}
	}
}

TreeMeshSegment* TreeMeshMaker::genTestSegmentC( TreeMeshSegment *par, int childType ){

	const float 
		pHeight = par -> height,
		pRadius = par -> endRadius;	
	
	float 
				height = pHeight * 0.8,
				startRadius = pRadius,
				endRadius = startRadius * 0.7,
				pointX = height * 0.1,
				pointZ = height * 0.1,
				pointY = height * 0.4,
				sideRotation = M_PI / 6,
				blockRotation = M_PI / 3;

	if( childType == 1 ){ 
		pointZ *= -1;
		sideRotation *= -1;
	}

	TreeMeshSegment *s = new TreeMeshSegment();
	s -> setRadius( startRadius, endRadius );
	s -> setHeight( height );
	s -> setQuality( 12 );
	s -> setStartPoint( getChildPos( par, sideRotation, pRadius, childType ) );
	s -> setAngle( glm::vec3( sideRotation, 0, 0 ) );
	s -> setCurvePoint( glm::vec3( pointX, pointY, pointZ ) );
	s -> setParentMat( par -> getChildTranslateMat() );
	s -> setTexCords( 0, 1 );
	s -> setChildRotation( blockRotation );
	s -> setParentConnect( childType );
	
	//thread gc( &TreeMeshMaker::genChildren, this, s );
	//gc.join();

	//makeSegment( s );

	thread ms( &TreeMeshMaker::makeSegment, this, s );
	ms.join();
	
	thread gc( &TreeMeshMaker::genChildren, this, s );
	gc.join();
	
	/*if( seg -> endRadius >= 0.01 ){
		TreeMeshSegment *child1, *child2;
		child1 = genTestSegmentC( seg, 1 );
		child2 = genTestSegmentC( seg, 2 );
		bindChildren( seg, child1, child2 );
	}*/

	//thread ms( &TreeMeshMaker::makeSegment, this, s );
	//ms.join();

	//genChildren( s );
	

	return s;
}

TreeMeshSegment* TreeMeshMaker::kk( TreeMeshSegment *b, int e ){

	auto s = new TreeMeshSegment;
	return s;
}

void TreeMeshMaker::genChildren( TreeMeshSegment *seg ){
	if( seg -> endRadius >= 0.01 ){

		TreeMeshSegment *child1, *child2;

		//TreeMeshSegment *seg1 = new TreeMeshSegment();
		//*seg1 = *seg;
		
		//cout << "Children" << endl;

		//auto segFuture1 = async( &TreeMeshMaker::genTestSegmentC, this, seg, 1 );
		//auto segFuture2 = async( &TreeMeshMaker::genTestSegmentC, this, seg, 2 );

		/*child1 = segFuture1.get();
		child2 = segFuture2.get();*/
		
		child1 = genTestSegmentC( seg, 1 );
		child2 = genTestSegmentC( seg, 2 );

		bindChildren( seg, child1, child2 );

		//thread t1( &TreeMeshMaker::genTestSegmentC, this, seg, 1 );
		//thread t2( &TreeMeshMaker::genTestSegmentC, this, seg, 2 );

		//t1.join();
		//t2.join();

		/*child1 = genTestSegmentC( seg, 1 );
		child2 = genTestSegmentC( seg, 2 );

		bindChildren( seg, child1, child2 );*/
	}	
}

void TreeMeshMaker::genTestSegment( TreeMeshSegment *par, int w, int h ){

}

#endif
