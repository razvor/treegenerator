#ifndef TEXTURE
#define TEXTURE

#include <GL/glew.h>

class Texture{
private:

	GLuint handler;
	int width, height;

	void genTexture(){
		glGenTextures(1, &handler);
	}

	void release(){
		glDeleteTextures(1, &handler);	
	}

public:

	void setParams(){
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);
	}

	void setSize( int w, int h ){
		width = w;
		height = h;
	}

	void setImage( GLubyte *image ){

		glBindTexture(GL_TEXTURE_2D, handler);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);

		setParams();
	}

	void render( int uniform, int n ){
		glActiveTexture(GL_TEXTURE0 + n);	
		glBindTexture(GL_TEXTURE_2D, handler);
		glUniform1i(uniform, n);
	}

	GLuint getInstance(){
		return handler;
	}

	Texture(){
		genTexture();
	}

	~Texture(){
		release();
	}
};

#endif
