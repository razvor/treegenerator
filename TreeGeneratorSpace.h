#ifndef TREE_GENERATOR_SPACE
#define TREE_GENERATOR_SPACE

#include <vector>
#include <iostream>
#include "glm/glm.hpp"
#include "TreeMeshMaker.h"

class TreeGeneratorSpace{
private:

	int minDistance, maxDistance;

	float segmentLength;

	std::vector<glm::vec3> growPoints;

	SegmentData *rootSegment;
	TreeMeshMaker *maker;
	
	void genGrowPoints();

	glm::vec3 *findGrowPoint(glm::vec3 pos);
	
	void growNext( SegmentData *prev );

	void processSegments();

	void runMaker();

public:

	TreeGeneratorSpace();
	~TreeGeneratorSpace();

	TreeModel *getModel();

};

#endif
