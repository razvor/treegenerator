#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <fstream>
#include <streambuf>
#include <cmath>
#include <iomanip>
#include <array>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Program.cpp"
#include "Matrices.cpp"
#include "VertexBuffer.cpp"
#include "Texture.cpp"

#include "TreeMeshMaker.h"
//#include "TreeMeshMaker2.h"
#include "TreeGenerator.h"
#include "CrustMaker.h"
//#include "LeafMaker.h"

using namespace std;

int viewportWidth = 1300,
		viewportHeight = 880;

Program *mainProg, *pointProg, *mainProg2, *groundProg, *depthProg, *leafProg;
Texture *tex, *normalMap, *shadowTex;

bool isLineMode = false;
bool isNormalMode = false;

GLuint frameBuffer;

typedef vector< glm::vec3 > Vertices;

//Logic

int frames;
int deltaTime;

int lastFrameTime,
		timePerFrames;

float treeTopMargin;

Vertices *borders, brd;
Vertices *normals, nrm;
vector< int > *indices;
vector< glm::vec2 > texCords;

VertexBuffer *vbuffer, 
						 *nbuffer,
						 *ibuffer, 
						 *tbuffer, 
						 *groundVertexBuffer, 
						 *groundNormalsBuffer, 
						 *groundIndicesBuffer,
						 *shadowViewVertexBuffer;

vector< VertexBuffer* > leafVertexBuffers;
vector< VertexBuffer* > leafNormalBuffers;
vector< VertexBuffer* > leafIndexBuffers;
vector< glm::mat4 > leafMatrices;
vector< int > leafIndicesCount;

int treeIndicesSize;

glm::mat4 viewMatrixG;
glm::mat4 projectionMatrixG;
glm::mat4 modelMatrixG;
glm::mat4 groundModel;
glm::mat4 shadowMatrix;

void showIndice( int i );
string showVector( glm::vec3 v );

void initTestSegments(){
	double time1 = glfwGetTime();

	TreeMeshMaker m(NULL);
	m.makeTestSegment();
	borders = new Vertices(m.getModel()->vertices);
	auto k = m.getModel()->indices;
	indices = new vector<int>();
	cout << "msm " << k.size() << endl;
	for( auto ii : k){
		//cout << ii << " ";
	}
	for(auto ki : k) indices->push_back( (int)ki );
	normals = new Vertices(m.getModel()->normals);
	texCords = m.getModel()->texCords;

	double time2 = glfwGetTime();
	cout << "Make time " << (time2 - time1) << endl;
}

void initGenerator(){

	double time1 = glfwGetTime();
	TreeGenerator g;
	g.runProcessing();
	auto md = g.getModel();
	borders = new Vertices( md->vertices );
	auto k = md->indices;
	indices = new vector<int>();
	for(auto ki : k) indices->push_back( ki );
	normals = new Vertices( md->normals );
	texCords = md->texCords;
	//todo

	cout << "End gen processing" << endl;

	//g.clearSegmentsData();
	double time2 = glfwGetTime();
	cout << "--Gen time: " << time2 - time1 << endl;

	glm::mat4 translate, rotate, scale;

	/*auto lfs = g.getLeafs();
	cout << "Start leaf proc" << endl;
	for( auto lf : lfs ){

		translate = glm::translate( glm::mat4(1.0), lf.position );
		rotate    = glm::rotate( glm::mat4(1.0), lf.rotation.x, glm::vec3( 1, 0, 0 ) );
		rotate   *= glm::rotate( glm::mat4(1.0), lf.rotation.y, glm::vec3( 0, 1, 0 ) );
		rotate   *= glm::rotate( glm::mat4(1.0), lf.rotation.z, glm::vec3( 0, 0, 1 ) );
		scale     = glm::scale( glm::mat4(1.0), glm::vec3( 0.18 ) );

		leafMatrices.push_back( translate * rotate * scale );

	}
	
	leafVertexBuffers.push_back( new VertexBuffer( 1, lfs.back().vertices ) );
	leafNormalBuffers.push_back( new VertexBuffer( 1, lfs.back().normals ) );
	leafIndexBuffers.push_back(  new VertexBuffer( 2, lfs.back().indices ) );
	leafIndicesCount.push_back( lfs.back().indices.size() );


	cout << "TT: " << leafVertexBuffers.size() << endl;*/
	
	cout << "End leaf proc" << endl;
}

void initTestTree(){
	
	double time1 = glfwGetTime();

	//TreeMeshMaker m(NULL);
	/*m.makeTestTree();
	borders = new Vertices( m.vertices );
	indices = new vector<int>( m.indices );
	normals = new Vertices( m.normals );
	texCords = vector< glm::vec2 >( m.texCords );*/

	double time2 = glfwGetTime();

	cout << "Make time " << (time2 - time1) << endl;
}

void initMeshMaker(){

	//initTestSegments();
	initGenerator();
	//initTestTree();
	
	treeIndicesSize = indices -> size();

	cout << "Mesh" << borders -> size() << " " << indices->size() << " " << texCords.size() << endl;
	cout << "Sizes in mb" << endl;
	cout << "Vertices: " << sizeof( glm::vec3 ) * borders -> size() / 1024.f / 1024.f << endl;
	cout << "Normals: " << sizeof( glm::vec3 ) * normals -> size() / 1024.f / 1024.f << endl;
	cout << "Vertices: " << sizeof( size_t ) * indices -> size() / 1024.f / 1024.f << endl;
	cout << "TexCords: : " << sizeof( glm::vec2 ) * texCords.size() / 1024.f / 1024.f << endl;
}

const int clamp(int pX, int pMax){

    if (pX > pMax){
        return pMax;
    }
    else if (pX < 0){
        return 0;
    }
    else{
        return pX;
    }
}

void initGround(){

	const int 
		widthQuality  = 100,
		heightQuality = 100,
		randomSeed = rand() % 100000;

	const float 
		widthStep = 1.f / widthQuality,
		heightStep = 1.f / heightQuality,
		widthScale = 2.f,
		heightScale = 2.f;

	float currentHeight, currentX, currentY;

	vector< glm::vec3 > groundVertices;
	vector< vector< float > > verts;
	vector< int > groundIndices;
	vector< glm::vec3 > groundNormals;

	verts.resize( widthQuality, vector< float >(widthQuality) );
	
	float 
		top, topLeft, topRight,
		bottom, bottomLeft, bottomRight,
		right, left;

	const int s = widthQuality - 1;

	for( int i = 0; i < widthQuality; ++i ){
		for( int j = 0; j < heightQuality; ++j ){
			currentHeight = glm::perlin( glm::vec2( 
						i * widthStep * widthScale + randomSeed, 
						j * heightStep * heightScale + randomSeed 
			) );

			currentHeight /= 8.f;
			currentX = -0.5f + i * widthStep;
			currentY = -0.5f + j * heightStep;
			
			groundVertices.push_back( glm::vec3( currentX, currentY, currentHeight ) );
			verts[ i ][ j ] = groundVertices.back().z;

			if( i == widthQuality / 2 && j == heightQuality / 2 ) treeTopMargin = currentHeight;
		}
	}

	for( int i = 0; i < widthQuality; ++i ){
		for( int j = 0; j < heightQuality; ++j ){
			top         = verts[ clamp( i - 1, s ) ][ clamp( j - 1, s ) ];
			topLeft     = verts[ clamp( i - 1, s ) ][ clamp( j    , s ) ];					
			topRight    = verts[ clamp( i - 1, s ) ][ clamp( j + 1, s ) ];					
			bottom      = verts[ clamp( i    , s ) ][ clamp( j + 1, s ) ];					
			bottomLeft  = verts[ clamp( i + 1, s ) ][ clamp( j + 1, s ) ];					
			bottomRight = verts[ clamp( i + 1, s ) ][ clamp( j    , s ) ];					
			left        = verts[ clamp( i + 1, s ) ][ clamp( j  - 1, s ) ];					
			right       = verts[ clamp( i,     s ) ][ clamp( j  - 1, s ) ];					

			const float dX = ( topRight + 2.f * right + bottomRight ) - ( topLeft + 2.f * left + bottomLeft );
			const float dY = ( bottomLeft + 2.f * bottom + bottomRight ) - ( topLeft + 2.f * top + topRight );
			const float dZ = 1.f / 2.f;

			groundNormals.push_back( glm::vec3( dX, dY, dZ ) );
			
		}
	}

	int ci, ni, ti, nti;

	for( int i = 0; i < widthQuality - 1; ++i ){
		for( int j = 0; j < heightQuality - 1; ++j ){
			ci = i * heightQuality + j;
			ni = ci + 1;
			ti = ci + heightQuality;
			nti = ti + 1;

			groundIndices.push_back( ci );
			groundIndices.push_back( ni );
			groundIndices.push_back( ti );
			groundIndices.push_back( ti );
			groundIndices.push_back( ni );
			groundIndices.push_back( nti );

		}
	}

	GLuint groundTexture[ widthQuality * heightQuality * 3 ];
	int fcr = 30, fcg = 60, fcb = 10;

	for( int i = 0; i < widthQuality; ++i ){
		for( int j = 0; j < heightQuality; ++j ){
			ci = i * widthQuality + i;
			ci *= 3;

			const float noi1 = glm::perlin( glm::vec2( i * widthQuality * 14.f, j * heightStep * 12.f ) );
			const float noi2 = glm::perlin( glm::vec2( i * widthQuality * 14.f, j * heightStep * 12.f ) );
			//const float noi3 = glm::perlin( glm::vec2( i * widthQuality * 14.f, j * heightStep * 12.f ) );

			const float noi = min( noi1, noi2 );

			groundTexture[ ci + 0 ] = fcr * noi;
			groundTexture[ ci + 1 ] = fcg * noi;
			groundTexture[ ci + 2 ] = fcb * noi;
		}
	}

	groundVertexBuffer  = new VertexBuffer( 1, groundVertices );	
	groundNormalsBuffer = new VertexBuffer( 1, groundNormals );	
	groundIndicesBuffer = new VertexBuffer( 2, groundIndices );	

}

void initShadowView(){

	vector<glm::vec3> vr = {
		glm::vec3( -0.5f, -0.5f, 0.0 )
	};

	shadowViewVertexBuffer = new VertexBuffer( 1, vr );
}

void initLeafs(){
	/*LeafMaker *leafMaker = new LeafMaker();
	LeafParams p;
	p.branchCount = 1;
	p.outCount = 6;
	p.size = 1.f;
	p.leafCount = 20;
	leafMaker -> setParams(p);
	leafMaker -> makeLeafs( nullptr );

	glm::mat4 translate, rotate, scale;

	auto lfs = leafMaker -> getLeafs();
	for( auto lf : lfs ){

		translate = glm::translate( glm::mat4(1.0), lf.position );
		rotate    = glm::rotate( glm::mat4(1.0), lf.rotation.x, glm::vec3( 1, 0, 0 ) );
		rotate   *= glm::rotate( glm::mat4(1.0), lf.rotation.y, glm::vec3( 0, 1, 0 ) );
		rotate   *= glm::rotate( glm::mat4(1.0), lf.rotation.z, glm::vec3( 0, 0, 1 ) );
		scale     = glm::scale( glm::mat4(1.0), glm::vec3( 5 ) );

		leafVertexBuffers.push_back( new VertexBuffer( 1, lf.vertices ) );
		leafNormalBuffers.push_back( new VertexBuffer( 1, lf.normals ) );
		leafIndexBuffers.push_back( new VertexBuffer( 2, lf.indices ) );
		leafMatrices.push_back( translate * rotate * scale );
		leafIndicesCount.push_back( lf.indices.size() );

	}
	*/

}

void showIndice( int i ){
	//cout << "ii" << indices[ i ] << endl; 
	//cout << "ind " << showVector(borders[ indices[ i ] ]) << endl;
}

string showVector( glm::vec3 v ){
	return to_string( v.x ) + " " + to_string( v.y ) + " " + to_string( v.z );
}

void initBuffers(){

	//nrm = normals;
	//brd = borders;

	vbuffer = new VertexBuffer( 1, *borders );	
	nbuffer = new VertexBuffer( 1, *normals );	
	ibuffer = new VertexBuffer( 2, *indices );	
	tbuffer = new VertexBuffer( 1, texCords );


	//delete borders;
	//delete normals;
	//delete indices;

	const int width = 512, height = 512;

	CrustMaker *crm = new CrustMaker( width, height );
	//crm -> genStartNoise();

	tex = new Texture(); 
	tex -> setSize( width, height );
	tex -> setImage( crm -> getTexture() );

	normalMap = new Texture();
	normalMap -> setSize( width, height );
	normalMap -> setImage( crm -> getNormalMap() );
}

void initFramebuffer(){
	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	shadowTex = new Texture();
	shadowTex -> setSize( 1024, 1024 );
	glBindTexture(GL_TEXTURE_2D, shadowTex -> getInstance() );
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, shadowTex -> getInstance(), 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
		cerr << "FBO EROR" << endl;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

float getRandom( float start, float end ){
	return rand() % (int)( end - start ) + start;
}

//Rendering

struct CameraPosition{
	float rotX, rotY, rotZ;
	float posX, posY, posZ;
	float scale;
} cameraPosition;

float rotY, rotX, rotZ;
float scale = 1.f;
float top = 0.f;

float *orthoMatrix;
float *viewMatrix;
float *eyeMatrix;
vector< float* > pointsMatrices;


void initGL(){
	mainProg   = new Program( "main", "main" );
	mainProg2  = new Program( "test", "test" );
	groundProg = new Program( "ground", "ground" );
	depthProg  = new Program( "depth", "depth" );
	leafProg   = new Program( "leaf", "leaf" );
	mainProg   -> init();
	groundProg -> init();
	mainProg2  -> init();
	depthProg  -> init();
	leafProg   -> init();

	pointProg = new Program( "point", "point" );
	pointProg -> init();

	float aspect = (float)viewportWidth / (float)viewportHeight;
	orthoMatrix = getProjectionMatrix( 30.0f, aspect, 1.f, 100.f );
	viewMatrix  = getViewMatrix( Vec( 0, 2, 0  ), Vec( 0, 0, 1 ), Vec( 0, 1, 0 ) );
	eyeMatrix   = getEyeMatrix( Vec( 0, 1, 1  ) );

	projectionMatrixG = glm::perspective(30.f, aspect, 0.1f, 1000.0f);
	viewMatrixG       = glm::lookAt(glm::vec3(0.0, 0.0, 10.0), glm::vec3(0.0, 0.0, 0), glm::vec3(0.0, -1.0, 0.0));
	modelMatrixG      = glm::translate( glm::mat4(1.0), glm::vec3( 0, -10 + treeTopMargin, 0 ) );

	glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_MULTISAMPLE);
	//glDepthFunc(GL_LEQUAL);
	glDisable(GL_DEPTH_CLAMP);
	
	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);
}

void clear(){
	glClearColor( 0.8, 0.8, 0.84, 1 );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

void renderBorders(){

	mainProg -> use();

	int positionLocation = 0; //mainProg -> attribute("aPosition");
	int normalLocation  = 1; //mainProg -> attribute("aNormal");
	int cordsLocation = 2; //mainProg -> attribute("texCord");

	int transformLocation = mainProg -> uniform( "model" );
	int orthoLocation = mainProg -> uniform("projection");
	int viewLocation = mainProg -> uniform("view");
	int vinvLocation = mainProg -> uniform("vinv");
	int textureLocation = mainProg -> uniform("uTexture");
	int normalMapLocation = mainProg -> uniform("uNormalMap");

	//vbuffer -> render( positionLocation );
	//nbuffer -> render( normalLocation );
	//tbuffer -> render( cordsLocation );

	glUniformMatrix4fv( orthoLocation, 1, GL_FALSE, &projectionMatrixG[0][0] );
	glUniformMatrix4fv( viewLocation, 1, GL_FALSE, &viewMatrixG[0][0] );
	glUniformMatrix4fv( transformLocation, 1, GL_FALSE, &modelMatrixG[0][0] );
	glUniformMatrix4fv( vinvLocation, 1, GL_FALSE, &glm::inverse(viewMatrixG)[0][0] );
	
	tex -> render( textureLocation, 0 );
	normalMap -> render( normalMapLocation, 1 );

	glBindBuffer( GL_ARRAY_BUFFER, vbuffer -> getInstance() );
	glEnableVertexAttribArray( 0 );
	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL
	);
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, nbuffer -> getInstance() );
	glEnableVertexAttribArray( 1 );
	glVertexAttribPointer(
		1,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL
	);
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, tbuffer -> getInstance() );
	glEnableVertexAttribArray( 2 );
	glVertexAttribPointer(
		2,
		2,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL
	);
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, ibuffer -> getInstance() );
	
	//ibuffer -> render( 0 );
	
	if( isLineMode ){
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}else{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	glLineWidth( 2 );
	glDrawElements( GL_TRIANGLES, treeIndicesSize,  GL_UNSIGNED_INT, NULL );
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	//glBindTexture(GL_TEXTURE_2D, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDisableVertexAttribArray( positionLocation );
	glDisableVertexAttribArray( normalLocation );
	glDisableVertexAttribArray( cordsLocation );
	
	glUseProgram(0);

	//mainProg -> unuse();
}

void renderNormal( glm::vec3 p, glm::vec3 n ){

	vector<glm::vec3> aa = {
		p, n
	};

	auto b = VertexBuffer( 1, aa );
	
	glBindBuffer( GL_ARRAY_BUFFER, b.getInstance() );
	glEnableVertexAttribArray( 0 );
	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,		
		NULL
	);
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	glDrawArrays( GL_LINES, 0, 2 );

	glDisableVertexAttribArray( 0 );
}

void renderNormals(){

	mainProg2 -> use();
	
	int transformLocation = mainProg2 -> uniform( "model" );
	int orthoLocation = mainProg2 -> uniform("projection");
	int viewLocation = mainProg2 -> uniform("view");
	glUniformMatrix4fv( orthoLocation, 1, GL_FALSE, &projectionMatrixG[0][0] );
	glUniformMatrix4fv( viewLocation, 1, GL_FALSE, &viewMatrixG[0][0] );
	glUniformMatrix4fv( transformLocation, 1, GL_FALSE, &modelMatrixG[0][0] );

	for( int i = 0; i < normals -> size(); i++ ){
		glm::vec3 n = normals -> at( i );
		glm::vec3 b = borders -> at( i );
		renderNormal( b, b + n );
	}

	glUseProgram( 0 );
}

void renderGround(){

	groundProg -> use();

	int transformLocation = groundProg -> uniform( "model" );
	int orthoLocation = groundProg -> uniform( "projection" );
	int viewLocation = groundProg -> uniform( "view");
	int shadowLocation = groundProg -> uniform( "shadow");
	int smvpLocation = groundProg -> uniform( "smvp" );

	glm::mat4 groundRotation = glm::rotate( glm::mat4(1.0), static_cast<float>(M_PI / 2.f), glm::vec3( 1, 0, 0 )	);
	glm::mat4 groundScale    = glm::scale( glm::mat4(1.0), glm::vec3( 200 ) );
	glm::mat4 groundMove     = glm::translate( glm::mat4(1.0), glm::vec3( 0, -10, 0 ) );

	glm::mat4 groundTransform = groundMove 
		* groundRotation 
		* groundScale;

	groundModel = groundTransform;

	glUniformMatrix4fv( orthoLocation,     1, GL_FALSE, &projectionMatrixG[0][0] );
	glUniformMatrix4fv( viewLocation,      1, GL_FALSE, &viewMatrixG[0][0] );
	glUniformMatrix4fv( transformLocation, 1, GL_FALSE, &groundTransform[0][0] );
	glUniformMatrix4fv( smvpLocation, 1, GL_FALSE, &shadowMatrix[0][0] );

	//groundVertexBuffer  -> render( 0 );
	//groundNormalsBuffer -> render( 1 );
	//groundIndicesBuffer -> render( 0 );

	glBindBuffer( GL_ARRAY_BUFFER, groundVertexBuffer -> getInstance() );
	glEnableVertexAttribArray( 0 );
	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL
	);
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, groundNormalsBuffer -> getInstance() );
	glEnableVertexAttribArray( 1 );
	glVertexAttribPointer(
		1,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL
	);
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	shadowTex -> render( shadowLocation, 0 );

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, groundIndicesBuffer -> getInstance() );
	glDrawElements( GL_TRIANGLES, 100 * 100 * 6,  GL_UNSIGNED_INT, NULL );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	glDisableVertexAttribArray( 0 );
	glDisableVertexAttribArray( 1 );

	glUseProgram( 0 );

}


void renderLeaf( int index ){
	int model2L     = leafProg->uniform( "model2" );
	glUniformMatrix4fv( model2L, 1, GL_FALSE, &leafMatrices[index][0][0] );
	/*glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	leafProg -> use();

	int model1L     = leafProg->uniform( "model1" );
	int model2L     = leafProg->uniform( "model2" );
	int viewL       = leafProg->uniform( "view" );
	int projectionL = leafProg->uniform( "projection" );
	int vinvL = leafProg->uniform( "vinv" );

	//glm::mat4 mvp = projectionMatrixG * viewMatrixG * modelMatrixG * leafMatrices[index];

	glUniformMatrix4fv( model2L, 1, GL_FALSE, &leafMatrices[index][0][0] );
	glUniformMatrix4fv( model1L, 1, GL_FALSE, &modelMatrixG[0][0] );
	glUniformMatrix4fv( viewL, 1, GL_FALSE, &viewMatrixG[0][0] );
	glUniformMatrix4fv( projectionL, 1, GL_FALSE, &projectionMatrixG[0][0] );
	glUniformMatrix4fv( vinvL, 1, GL_FALSE, &glm::inverse(viewMatrixG)[0][0] );

	glBindBuffer( GL_ARRAY_BUFFER, leafVertexBuffers[0] -> getInstance() );
	glEnableVertexAttribArray( 0 );
	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL
	);
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, leafNormalBuffers[0]->getInstance() );
	glEnableVertexAttribArray( 1 );
	glVertexAttribPointer(
		1,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL
	);
	glBindBuffer( GL_ARRAY_BUFFER, 0 );*/

	//glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, leafIndexBuffers[0] -> getInstance() );
	glDrawElements( GL_TRIANGLES, leafIndicesCount[0], GL_UNSIGNED_INT, NULL );
	//glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

	//glDisableVertexAttribArray( 0 );
	//glDisableVertexAttribArray( 1 );

	//glUseProgram( 0 );
}

void renderLeafs(){
	leafProg -> use();

	int model1L     = leafProg->uniform( "model1" );
	int model2L     = leafProg->uniform( "model2" );
	int viewL       = leafProg->uniform( "view" );
	int projectionL = leafProg->uniform( "projection" );
	int vinvL = leafProg->uniform( "vinv" );

	//glm::mat4 mvp = projectionMatrixG * viewMatrixG * modelMatrixG * leafMatrices[index];

	glUniformMatrix4fv( model1L, 1, GL_FALSE, &modelMatrixG[0][0] );
	glUniformMatrix4fv( viewL, 1, GL_FALSE, &viewMatrixG[0][0] );
	glUniformMatrix4fv( projectionL, 1, GL_FALSE, &projectionMatrixG[0][0] );
	glUniformMatrix4fv( vinvL, 1, GL_FALSE, &glm::inverse(viewMatrixG)[0][0] );

	glBindBuffer( GL_ARRAY_BUFFER, leafVertexBuffers[0] -> getInstance() );
	glEnableVertexAttribArray( 0 );
	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL
	);
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	glBindBuffer( GL_ARRAY_BUFFER, leafNormalBuffers[0]->getInstance() );
	glEnableVertexAttribArray( 1 );
	glVertexAttribPointer(
		1,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL
	);
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, leafIndexBuffers[0] -> getInstance() );

	for( int i = 0; i < leafMatrices.size(); ++i )
		renderLeaf( i );

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

	glUseProgram( 0 );
	glDisableVertexAttribArray( 0 );
	glDisableVertexAttribArray( 1 );
}

void render( bool shadowMap ){

	glm::mat4 vmg = viewMatrixG;
	viewMatrixG = glm::lookAt( glm::vec3( 0, 15, 7 ), glm::vec3( 0, 0, 0 ), glm::vec3( 0, -1, 0 ) );

	depthProg -> use();
	//mainProg -> use();
	glViewport( 0, 0, 1024, 1024 );
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer );
	clear();
	//glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
		//glDepthFunc(GL_LESS); 
		renderBorders();
		glDisable(GL_CULL_FACE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glm::mat4 biasMatrix(
		0.5, 0.0, 0.0, 0.0, 
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0
	);

	shadowMatrix = biasMatrix * projectionMatrixG * viewMatrixG * groundModel;
	
	viewMatrixG = vmg;

	//mainProg -> use();
	glViewport( 0, 0, viewportWidth, viewportHeight );

	clear();
	renderBorders();

	if(isNormalMode)
		renderNormals();

	//renderGround();
	//renderLeafs();

}

void keyPress( unsigned char key, int x, int y  );
float pl = 0;

void update(){
	
	int time = glfwGetTime();
	timePerFrames = time - lastFrameTime;
	lastFrameTime = time;

	//int time = glutGet(GLUT_ELAPSED_TIME);	
	
	//pl = 0.0001f * time;
	
	//keyPress( 'o', 0, 0 );

	//glutPostRedisplay();
}

void refresh(){
		update();
		render(true);
}

void finish();

void keyPress( unsigned char key, int act, int mod ){

	const float speed = 7.f / 100;

	if( key == 'w' )
		rotX += speed;		

	if( key == 's' )
		rotX -= speed;

	if( key == 'a' )
		rotY -= speed;

	if( key == 'd' )
		rotY += speed;

	if( key == 'z' )
		scale *= 1.1;

	if( key == 'x' )
		scale /= 1.1;

	if( key == 'q' )
		rotZ -= speed;

	if( key == 'e' )
		rotZ += speed;

	if( key == 'r' )
		top -= speed;

	if( key == 'f' )
		top += speed;

	if( key == '(' )
		cameraPosition.posX += speed * 3.f;

	if( key == ')' )
		cameraPosition.posX -= speed * 3.f;

	if( key == '\'' )
		cameraPosition.posZ += speed * 3.f;

	if( key == '&' )
		cameraPosition.posZ -= speed * 3.f;

	if( key == 'l' && act == 0 )
		isLineMode = !isLineMode;

	if( key == 'n' && act == 0 )
		isNormalMode = !isNormalMode;

	if( key == 27 ){
		finish();
		exit(0);
	}	

	/*glm::mat4 r1 = glm::rotate( glm::mat4(1.0), rotX, glm::vec3(1,0,0) );
	glm::mat4 r2 = glm::rotate( glm::mat4(1.0), rotY, glm::vec3(0,0,1) );
	glm::mat4 r3 = glm::rotate( glm::mat4(1.0), rotZ, glm::vec3(0,1,0) );
	glm::mat4 s  = glm::scale( glm::mat4(1.0), glm::vec3( scale, scale, scale ) );
	glm::mat4 t = glm::translate( glm::mat4(1.0), glm::vec3( 0, top, 0 ) );*/
	//modelMatrixG = r1 * r2 * r3 * s * t;
	
	float radius = 15.f * scale;

	float eRadius = radius * cosf( rotX );

	glm::vec3 movement = glm::vec3(
		cameraPosition.posX,
		top,
		cameraPosition.posZ
	);
	
	glm::vec3 newPos(
		eRadius * cosf( rotZ ),
		0,
		eRadius * sinf( rotZ )		
	);

	newPos.y = radius * sinf( rotX );

	viewMatrixG = glm::lookAt(newPos + movement, glm::vec3(0.0, 0.0, 0) + movement, glm::vec3(0.0, -1.0, 0.0));
}

void glewKeyPress( GLFWwindow* window, int key, int scancode, int action, int mods ){
	//if( action == GLFW_RELEASE ){
	keyPress( static_cast<unsigned char>(key + 32), action, mods );
	//}
	refresh();
	glfwSwapBuffers(window);
}

void finish(){
	delete mainProg;	
	delete mainProg2;	
	delete vbuffer;
	delete nbuffer;
	delete ibuffer;
	delete tbuffer;
}

int main(int argcp, char **argv){

	GLFWwindow* window;
	if (!glfwInit()){
		cerr << "Error initing GLFW" << endl;
	}
	glfwWindowHint(GLFW_SAMPLES, 8);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	window = glfwCreateWindow(viewportWidth, viewportHeight, "Plant generator", NULL, NULL);
	if (!window){
		cerr << "Error creating window" << endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glfwGetFramebufferSize(window, &viewportWidth, &viewportHeight );
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	cout << "Version: " << glGetString( GL_VERSION ) << endl;
	cout << "Rend: " << glGetString( GL_RENDERER ) << endl;
	
	initMeshMaker();
	initBuffers();
	initGround();
	//initLeafs();
	initFramebuffer();

	initGL();

	glfwSetKeyCallback(window, glewKeyPress);

	refresh();
	glfwSwapBuffers(window);
	while ( !glfwWindowShouldClose(window) && glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS ){
		glfwPollEvents();
	}

	glfwTerminate();
	
	return 1;
}
