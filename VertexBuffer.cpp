#include <iostream>
#include <vector>

#include <OpenGL/gl.h>
#include <GLUT/glut.h>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

using namespace std;

class VertexBuffer{

	GLuint handler;
	GLuint target;
	bool isTwice;

	void genBuffer(){
		glGenBuffers( 1, &handler );
	}
	
public:

	template< typename T >
	float *getLinear( vector< T > data, int stepSize ){
		int i,j,size;
		size = data.size();
		float *linearData = new float[ stepSize * size ];
		for( i = 0; i < size; i += stepSize )
			for( j = 0; j < stepSize; ++j )
				linearData[ i + j ] = data[ i ][ j ];

		return linearData;
	}
	
	void pushData( int type, vector< glm::vec3 > data ){

		target = (type == 1) ? GL_ARRAY_BUFFER : GL_ELEMENT_ARRAY_BUFFER;

		glBindBuffer( target, handler );
		glBufferData( target, sizeof( glm::vec3 ) * data.size(), &data[0][0], GL_STATIC_DRAW );
		glBindBuffer( target, 0 );

	}

	void pushData( int type, vector< int > data ){

		target = (type == 1) ? GL_ARRAY_BUFFER : GL_ELEMENT_ARRAY_BUFFER;

		glBindBuffer( target, handler );
		glBufferData( target, sizeof( int ) * data.size(), &data[0], GL_STATIC_DRAW );
		glBindBuffer( target, 0 );
	}

	void pushData( int type, vector< size_t > data ){

		target = (type == 1) ? GL_ARRAY_BUFFER : GL_ELEMENT_ARRAY_BUFFER;

		glBindBuffer( target, handler );
		glBufferData( target, sizeof( size_t ) * data.size(), &data[0], GL_STATIC_DRAW );
		glBindBuffer( target, 0 );
	}

	void pushData( int type, vector< glm::vec2 > data ){
		target = (type == 1) ? GL_ARRAY_BUFFER : GL_ELEMENT_ARRAY_BUFFER;
		isTwice = true;

		glBindBuffer( target, handler );
		glBufferData( target, sizeof( glm::vec2 ) * data.size(), &data[0][0], GL_STATIC_DRAW );
		glBindBuffer( target, 0 );
	}

	void render( int attr  ){
		glBindBuffer(target, handler);
		if( target == GL_ARRAY_BUFFER ){
			glEnableVertexAttribArray(attr);
			glVertexAttribPointer(
				attr,
				isTwice ? 2 : 3,
				GL_FLOAT,
				GL_FALSE,
				0,
				NULL
			);
			glBindBuffer( target, 0 );
		}
		//glBindBuffer(target, 0);
	}


	VertexBuffer( int type, vector< glm::vec3 > data ){
		genBuffer();
		pushData( type, data );
	}

	VertexBuffer( int type, vector< glm::vec2 > data ){
		genBuffer();
		pushData( type, data );
	}

	VertexBuffer( int type, vector< int > data ){
		genBuffer();
		pushData( type, data );
	}

	VertexBuffer( int type, vector< size_t > data ){
		std::cout << "sz" << std::endl;
		genBuffer();
		pushData( type, data );
	}

	int getInstance(){
		return handler;
	}

	VertexBuffer(){}

	~VertexBuffer(){
		glDeleteBuffers( 1, &handler );
	}

};
