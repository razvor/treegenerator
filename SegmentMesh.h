#ifndef SEGMENT_MESH_H
#define SEGMENT_MESH_H

#include <vector>
#include <array>
#include "glm/glm.hpp"
#include "SegmentData.h"
#include "SegmentBinderMesh.h"

class SegmentMesh{
private:

	SegmentData *data;
	SegmentBinderMesh *binder;

	size_t offsetIndex;
	
	glm::vec3 *vertices;
	glm::vec3 *normals;
	glm::vec2 *texCords;
	std::vector<size_t> indices;
	size_t **rowIndices;

	glm::vec3 getBezierPoint( float t, glm::vec3 p1, glm::vec3 p2, glm::vec3 cp );
	void makeVertices();
	void makeNormals();
	void makeTexCords();
	void makeIndices();

	void makeEndVertices();
	void makeEndNormals();
	void makeEndTexCords();
	void makeEndIndices();

	void bindDigitVertices( size_t *arr1, size_t *arr2, int dw1, int dw2 );

	template<int S>
	void addInds(std::array<size_t, S> arr);

	void bindWithBinder();

public:
	SegmentMesh( SegmentData *data );
	~SegmentMesh();
	void setBinder(SegmentBinderMesh *bm);
	void runProcessing();
	size_t moveIndices( size_t offset );
	void bindWithParent( SegmentMesh *parentSegment );
	
	SegmentData *getData();

	size_t *getLastRing( float marg );
	size_t *getFirstRing();
	std::vector< glm::vec3 > getVertices();
	std::vector< glm::vec3 > getNormals();
	std::vector< glm::vec2 > getTexCords();
	std::vector< size_t > getIndices();

};

#endif
