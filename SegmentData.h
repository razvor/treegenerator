#ifndef SEGMENT_DATA
#define SEGMENT_DATA

#include <vector>
#include "glm/glm.hpp"
#include "glm/gtx/string_cast.hpp"

class SegmentData{
private:
//
	int childIndex;
	int outgrouthIndex;

	glm::vec3 startPosition;
	glm::vec2 outgrouthPosition; //If current segment outgrouth
	glm::vec3 endPosition;
	std::vector<glm::vec3> curvePoints;
	glm::vec3 angle;
	int widthDensity, 
			heightDensity;
	float startRadius,
				endRadius;
	float length;
	float childRotation;
	SegmentData *parent;
	std::vector<SegmentData *> children;
	std::vector<SegmentData *> outgrouths;

	std::vector<glm::vec3> childrenPositions;
	std::vector<float> sidesHeights;

	//For children coutgrouths
	std::vector<glm::vec3> outgrouthsPositions;

	glm::vec3 startAngle;
	glm::vec3 endAngle;
	glm::mat4 postureMatrix;
	glm::mat4 binderMatrix;

public:

	bool isChildrenRotation;
	
	SegmentData();

	~SegmentData();

	int getInheritType();
	int getInheritValue();
	//0 - trunk //1 - child //2 - outgraouth
	void setInheritIndices( int child, int outgrouth );

	glm::vec3 getPosition();
	void setPosition(glm::vec3 p);

	void setPostureMatrix(glm::mat4 p);

	glm::vec3 getOutgrouthPosition(int ind);
	void setOutgrouthPosition(glm::vec2 pos);

	std::vector<glm::vec3> getCurvePoints();
	void addCurvePoint(glm::vec3 p);

	glm::vec3 getAngle();
	void setAngle(glm::vec3 a);

	float getLength();
	void setLength(float l);

	std::pair<float, float> getRadiuses();
	void setRadiuses(float sr, float er);

	std::pair<int, int> getDensity();
	void setDensity(int w, int h);

	float getChildrenRotation();
	void setChildrenRotation(float cr);

	SegmentData *getParent();
	void setParent(SegmentData *p);

	SegmentData *getChild(int ind);
	int getChildrenCount();
	void addChild(SegmentData *p);

	void addOutgrouth(SegmentData *out);
	int getOutgrouthsCount();
	SegmentData *getOutgrouth(int ind);

	glm::mat4 getPostureMatrix();
	glm::mat4 getParentPostureMatrix();
	glm::mat4 getBinderMatrix();
	glm::vec3 getEndPosition();
	glm::vec3 getChildPosition(int ind);
	float getSideHeight(int ind);
	glm::vec2 getCurrentOutgrouthPosition();
	glm::mat4 getOutgrouthMatrix(int ind);	


	void computeStartAngle();
	void comuteEndAngle();
	void computeStartPosition();
	void computePostureMatrix();
	void computeBinerMatrix();
	void computeEndPosition();
	void computeChildrenPositions();
	void computeSidesHeights();
	void computeOutgrouthsPositions();

	void computeAll();

};

#endif
