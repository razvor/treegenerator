#include "SegmentBinderMesh.h"
#include <iostream>
#include <algorithm>
#include "glm/gtc/matrix_transform.hpp"

SegmentBinderMesh::SegmentBinderMesh(SegmentData *s){
	currentSegment = s;

	childrenCount = s->getChildrenCount();
	dw = s->getDensity().first;
	dh = s->getDensity().second;

	initDefaults();

	rowIndicesSides.resize( childrenCount );
	for(int j = 0; j < childrenCount; ++j){
		rowIndicesSides[j] = new size_t*[ dh / 2 ];
		for (int i = 0; i < dh/2; i++) {
			rowIndicesSides[j][i] = new size_t[ dw ];
		}
	}
	int ringCount = 1 + s->getChildrenCount();
	for(int j=0;j<ringCount;++j){
		ringIndices.push_back( new size_t[dw] );
	}
	rowIndicesTop.resize(1);
	//for(int j = 0; j < 1; ++j){
	rowIndicesTop[0] = new size_t*[ dw ];
	for (int i = 0; i < dw; i++) {
		rowIndicesTop[0][i] = new size_t[ dw ];
	}

	//}

	rowIndicesCenter.resize( childrenCount );
	for(int j = 0; j < childrenCount; ++j){
		rowIndicesCenter[j] = new size_t*[ dh ];
		for (int i = 0; i < dh; i++) {
			rowIndicesCenter[j][i] = new size_t[ dw ];
		}
	}

	sidesCenters = new glm::vec3*[ childrenCount ];
	for(int j = 0; j < childrenCount; ++j){
		sidesCenters[j] = new glm::vec3[ dhSides ];
	}

	centersCenters = new glm::vec3*[ childrenCount ];
	for(int j = 0; j < childrenCount; ++j){
		centersCenters[j] = new glm::vec3[ dhSides ];
	}

}

SegmentBinderMesh::~SegmentBinderMesh(){
	for(auto ri : rowIndicesSides){
		delete[] ri;
	}
	for(auto ri : rowIndicesCenter){
		delete[] ri;
	}
	for(auto ri : rowIndicesTop){
		delete[] ri;
	}
	for(auto ri : ringIndices){
		delete ri;
	}
}

void SegmentBinderMesh::initDefaults(){
	dwSides = dw/2+1;
	dhSides = dh/2;
}

void SegmentBinderMesh::bindVertices( size_t *a1, size_t *a2, int s ){
	size_t ci, ni, ti, nti;
	for(int i=0;i<s-1;++i){
		ci = a1[i];
		ti = a2[i];

		ni = a1[i+1];
		nti = a2[i+1];

		indices.push_back(ci);
		indices.push_back(ni);
		indices.push_back(ti);
		indices.push_back(ti);
		indices.push_back(ni);
		indices.push_back(nti);
	}
}
void SegmentBinderMesh::bindDigitVertices( size_t *arr1, size_t *arr2, int s ){
	const int sz = 9;
	size_t ci, ni, bci, bni, bmi;
	for(int i=0;i<s-1;++i){
		ci = arr2[i];
		bci = arr1[i*2];
		
		ni = arr2[i+1];
		bni = arr1[(i+1)*2];
		
		bmi = arr1[i*2+1];
		std::array<size_t, sz> indArr = { 
			ci, bci, bmi, 
			bmi, ci, ni, 
			bni, bmi, ni 
		};

		addInds<sz>(indArr);
	}
}

void SegmentBinderMesh::runProcessing(){
	makeVertices();
	makeIndices();
	makeNormals();
	makeTexCords();
}

int SegmentBinderMesh::addVert( glm::vec3 p ){
	vertices.push_back( p );
	return vertices.size() - 1;
}

template<int S>
void SegmentBinderMesh::addInds( std::array<size_t, S> arr ){
	for(int i=0; i<S; ++i){
		indices.push_back(arr[i]);
	}
}

glm::vec3 SegmentBinderMesh::getRoundPoint( glm::mat3 mat, float radius, float rot ){
	return mat * glm::vec3( radius * cosf( rot ), 0, radius * sinf( rot ) );
}

glm::vec3 SegmentBinderMesh::getBezierDirection( float t, glm::vec3 p1, glm::vec3 p2, glm::vec3 cp ){
	glm::vec3 d1 = p1 + (cp-p1)*glm::vec3(t);
	glm::vec3 d2 = cp + (p2-cp)*glm::vec3(t);
	glm::vec3 pp = d1 - d2;
	return glm::normalize(pp);
}

glm::vec3 SegmentBinderMesh::getBezierPoint( float t, glm::vec3 p1, glm::vec3 p2, glm::vec3 cp1, glm::vec3 cp2 ){
	const float dt = 1.f - t;
	glm::vec3 p;
	p = dt * dt * p1 + 2 * dt * t * cp1 + t * t * p2;
	return p;

}

glm::vec3 SegmentBinderMesh::getBezierPoint( float t, glm::vec3 p1, glm::vec3 p2, glm::vec3 cp ){
	const float dt = 1.f - t;
	glm::vec3 p;
	p = dt * dt * p1 + 2 * dt * t * cp + t * t * p2;
	return p;
}

glm::vec3 SegmentBinderMesh::getBezierPointR( float t, float w, glm::vec3 p1, glm::vec3 p2, glm::vec3 cp ){
	const float dt = 1.f - t;
	glm::vec3 p;
	p = dt * dt * p1 + w * dt * t * cp + t * t * p2;
	return p;
}

void SegmentBinderMesh::makeRingVertices(){
	int i,j,k;
	float cRadius, hStep;
	glm::vec3 sPoint, cPoint;
	glm::mat3 trmat;

	if(currentSegment->getChildrenCount()==2){
		//bottom
		sPoint  = currentSegment->getEndPosition();
		hStep   = 2.f * M_PI / dw;
		trmat   = glm::mat3(currentSegment->getBinderMatrix());
			//glm::mat3(currentSegment->getPostureMatrix());
		cRadius = currentSegment->getRadiuses().second;
		
		for(i=0; i<dw; i++){
			cPoint = glm::vec3(
				cRadius * cosf( hStep * i ),
				0,
				cRadius * sinf( hStep * i )
			);
			cPoint = sPoint + trmat * cPoint;

			ringIndices[0][i] = addVert(cPoint);
		}

		for(k=0; k<2; ++k){
			auto cChild = currentSegment->getChild(k);
			sPoint = currentSegment->getChildPosition(k); //cChild->getPosition();

			cRadius = cChild->getRadiuses().first;
			trmat = glm::mat3(cChild->getPostureMatrix());
			for(i=0; i<dw; ++i){
				cPoint = glm::vec3(
					cRadius * cosf( hStep * i ),
					0,
					cRadius * sinf( hStep * i )
				);
				cPoint = sPoint + trmat * cPoint;
				ringIndices[k+1][i] = addVert(cPoint);
			}
		}

	}
}

void SegmentBinderMesh::makeSideVertices(){
	int i,j,k,n,ind;
	glm::vec3 sCenter, eCenter, cPoint, curveBasis;
	glm::mat3 trmat, pmat;
	float pRadius, cRadius, radius, curveRadius, angle, curveAngle, side, hMargin, hStep, rStep, vStep;
	auto d = currentSegment;

	ind = 0;
	pRadius = d->getRadiuses().second;
	hStep = 2.f * M_PI / dw;

	if( d->getChildrenCount() == 2 ){
		for (k = 0; k < 2; k++) {
			
			side    = k ? -1.f : 1.f;
			hMargin = k ? M_PI : 0;
			cRadius = d->getChild(k)->getRadiuses().first;
			vStep   = d->getSideHeight(k) / dhSides;
			rStep   = (cRadius - pRadius) / dhSides;
			
			trmat   = glm::mat3( d->getChild(k)->getPostureMatrix() );
			pmat    = glm::mat3( d->getBinderMatrix() );
			
			sCenter = d->getEndPosition();
			sCenter += pmat * glm::vec3(0, 0, pRadius * side);
			sCenter += glm::mat3(trmat) * glm::vec3(0, 0, -pRadius * side);
			
			//TODO
			//angle = M_PI/2 - d->getChild(k)->getAngle().x;
			//curveRadius = d->getSideHeight(k) / cosf(angle);
			//curveAngle = M_PI/2 - angle;
			//curveBasis = d->getEndPosition() + pmat * glm::vec3( 0, 0, curveRadius );

			for (j = 0; j < dhSides; j++) {
				for (i = 0; i < dwSides; i++) {

					//TODO
					//trmat = glm::mat3(glm::rotate(d->getBinderMatrix(), curveAngle * vStep * j + (float)M_PI, glm::vec3(1, 0, 0) ));
					//sCenter = curveBasis;
					//sCenter += trmat * glm::vec3( 0, 0, curveRadius );

					radius = pRadius + rStep * j;
					cPoint = 	glm::vec3(
						radius * cosf( hStep * (i) + hMargin ),
						0,
						radius * sinf( hStep * (i) + hMargin )
					);
					cPoint = trmat * cPoint;
					cPoint += trmat * glm::vec3( 0, vStep * j, 0 );
					cPoint += sCenter;

					//for normals, not for current vertices
					sidesCenters[k][j] = sCenter + trmat * glm::vec3( 0, vStep * j, 0 );

					ind = addVert( cPoint );
					rowIndicesSides[k][j][i] = ind;
				}
			}
		}
	}

}
void SegmentBinderMesh::makeCenterVertices(){
	glm::vec3 interPoint, startPoint, endPoint, sBPoint, eBPoint, cBPoint, currentPoint;
	glm::mat3 trmat;
	float radius, bStep, hStep, vStep, currentV;
	int i,j,k,ind, si1, si2, fi1, fi2, lastInd;

	radius   = currentSegment->getRadiuses().second;
	
	trmat    = glm::mat3(currentSegment->getBinderMatrix());

	float r1, r2, r, a, b, c, cos1, cos2, ang1, ang2, ang3, diffX, diffZ, dist, side, rMarg;
	int ci, stepsCount;
	glm::vec3 c1, c2, c0;
	glm::vec3 pc;

	hStep = 1.f / (dwSides+1);
	vStep = 1.f / (dhSides-1);
	
	for(k=0; k<2; ++k){
		for(j=0; j<dhSides; ++j){

			c1 = sidesCenters[0][j];
			c2 = sidesCenters[1][j];
			c0 = (c1+c2)/2.f;
			dist = glm::distance(c1,c2);

			r = dist / 1.2f;
			r1 = Common::Instance().getTVal(j * vStep, 
					radius,
					currentSegment->getChild(0)->getRadiuses().first 
			);
			r2 = Common::Instance().getTVal(j * vStep, 
					radius,
					currentSegment->getChild(1)->getRadiuses().first 
			);
			
			a = r1 + r;
			b = r2 + r;
			c = dist;
			
			cos1 = (a * a + c * c - b * b) / (2.f * a * c);
			ang1 = acosf(cos1);

			diffZ = a * cos1;
			diffX = a * sinf(ang1);
			
			cos2 = diffZ / b;
			ang2 = acosf(cos2);

			ang3 = M_PI - ang1 - ang2;

			side = k ? 1 : -1;
			pc = c1 - trmat * glm::vec3( diffX * side, 0, diffZ );
			centersCenters[k][j] = pc;

			//Continue side circle #1
			ci = 1;
			stepsCount = dwSides/4;
			hStep = (M_PI/2 - ang2)/(stepsCount+1);
			
			for(i=0; i<stepsCount; ++i){
				rMarg = k ? -M_PI : 0;
				currentPoint = c2 + 
					trmat * glm::vec3(
						r2 * cosf( -(i+0)*hStep * side + rMarg ),
						0,
						r2 * sinf( -(i+0)*hStep * side + rMarg )
					);

				ind = addVert(currentPoint);
				rowIndicesCenter[k][j][dwSides-ci] = ind;
				++ci;
			}

			//Curve circle
			stepsCount = (dwSides - dwSides/2);
			if(currentSegment->getDensity().first == 4) stepsCount = 3;
			hStep = (ang3)/(stepsCount-1);

			for(i=0; i<stepsCount; ++i){
				
				rMarg = -M_PI/2 - ang1;
				if(k) rMarg = M_PI/2 + ang2 + M_PI;

				currentPoint = pc + 
					trmat * glm::vec3(
						r * cosf( (i)*hStep * side + rMarg ),
						0,
						r * sinf( (i)*hStep * side + rMarg )
					);

				ind = addVert(currentPoint);

				rowIndicesCenter[k][j][dwSides-ci] = ind;
				++ci;	
			}

			//Continue side circle #2
			stepsCount = dwSides/4;
			hStep = -(M_PI/2 - ang1)/(stepsCount);

			for(i=0; i<stepsCount; ++i){
				rMarg = (M_PI/2 - ang1)/(dwSides/4) * (dwSides/4-1);
				rMarg = k ? (-M_PI + rMarg): (0-rMarg);
				
				currentPoint = c1 + 
					trmat * glm::vec3(
						r1 * cosf( (i)*hStep * side + rMarg ),
						0,
						r1 * sinf( (i)*hStep * side + rMarg )
					);

				ind = addVert(currentPoint);
				rowIndicesCenter[k][j][dwSides-ci] = ind;
				++ci;
			}

		}
	}

}
void SegmentBinderMesh::makeTopVertices(){
	glm::vec3 d[2], s[2];
	glm::vec3 interPoint, rmarg, margin, endPoint, startPoint, currentPoint;
	glm::mat3 trmat;
	float r, sd, cr, rStep;
	int i, j, k;
	
	for(j=0;j<dwSides;++j){
		for(k=0;k<2;++k){
			auto c = currentSegment->getChild(k);
			
			r = c->getRadiuses().first;
			sd = k ? 1.f : -1.f;
			rStep = M_PI / (dwSides - 1);
			cr = M_PI - rStep * j*sd;
			
			trmat = glm::mat3(c->getPostureMatrix());
			rmarg = glm::vec3(r*cosf(cr), 0, r*sinf(cr));
			margin = trmat * rmarg;
			endPoint = c->getEndPosition() + margin;
			startPoint = c->getPosition() + margin;

			d[k] = (startPoint - endPoint);
			d[k] = glm::normalize(d[k]);
			s[k] = startPoint;
		}

		//float u = (starts[0].z*dirs[1].z+dirs[1].y*starts[0].z-starts[1].y*dirs[1].z-dirs[1].y*starts[1].z)/(dirs[0].z*dirs[1].y-dirs[0].y*dirs[1].z);


		//float u = ((s[0].z-s[1].z)*d[1].y)/(d[1].z*(s[0].y-s[0].y)*(d[0].y-d[1].z-d[0].z*d[1].y));

		//float u = 1;
		
		glm::vec3 mrg = glm::mat3(currentSegment->getBinderMatrix()) * glm::vec3(0,currentSegment->getRadiuses().second*2.f ,0);
		//interPoint = currentSegment->getEndPosition() + mrg; 

		glm::vec3 p1 = currentSegment->getChildPosition(0);
		glm::vec3 p2 = currentSegment->getChildPosition(1);
		interPoint = p1 + (p2-p1)*glm::vec3(0.5f);

		//s[0]+d[0]*glm::vec3(u);

		glm::vec3 inp1 = interPoint + glm::mat3(currentSegment->getBinderMatrix()) * glm::vec3( 0, 0, currentSegment->getRadiuses().second/2 );
		glm::vec3 inp2 = interPoint + glm::mat3(currentSegment->getBinderMatrix()) * glm::vec3( 0, 0, -currentSegment->getRadiuses().second/2 );

		float st = 1.f / (dwSides+1);

		for(i=0;i<dwSides;++i){
			currentPoint = getBezierPoint( st * (i+1), s[0], s[1], interPoint );
			/*if(i<=dwSides/2){
				currentPoint = getBezierPoint( st * (i), s[0], interPoint, inp1 );
			}else{
				currentPoint = getBezierPoint( st * (i+1-dwSides/2), interPoint, s[1], inp2 );
			}*/
			int ind = addVert(currentPoint);
			rowIndicesTop[0][j][i] = ind;
		}
	}

}
void SegmentBinderMesh::makeVertices(){
	makeRingVertices();
	makeSideVertices();
	makeTopVertices();
	makeCenterVertices();
}

void SegmentBinderMesh::makeNormals(){
	glm::vec3 currentVec, center, point, basis;
	glm::mat3 cmat;
	float vStep, hStep;
	int i,j,k, ind;
	
	normals.resize( vertices.size(), glm::vec3(1) );

	//Sides

	for(k=0; k<2; ++k){

		auto c = currentSegment->getChild(k);
		basis = currentSegment->getEndPosition() +
			glm::vec3( 0, 0, currentSegment->getRadiuses().second * (k?-1.f:1.f) );

		for(j=0; j<dhSides; ++j){
			for(i=0; i<dwSides; ++i){

				ind = rowIndicesSides[k][j][i];
				point = vertices[ind];
				center = sidesCenters[k][j];
				currentVec = glm::normalize(point - center);
				normals[ind] = currentVec;
				++ind;

			}
		}
	}


	//Rings
	
	hStep = 2.f * M_PI / dw;
	for(k=0;k<3;++k){
		
		auto c  = k ? currentSegment->getChild(k-1) : currentSegment;
		cmat    = glm::mat3( k ? c->getPostureMatrix() : c->getBinderMatrix() );

		for (i = 0; i < dw; i++) {
			ind = ringIndices[k][i];
			currentVec = getRoundPoint( cmat, 1.f, hStep * i );						
			normals[ind] = currentVec;
		}
	}

	//Top
	
	glm::vec3 
		point1, 
		point2, 
		point3, 
		point4, 
		interPoint1, 
		interPoint2, 
		parVec1, 
		parVec2;

	cmat   = glm::mat3(currentSegment->getBinderMatrix());
	vStep = 1.f / ((dwSides-1));
	hStep = 1.f / (dwSides-1);
	
	for(j=0; j<dwSides; ++j){
		for(i=0; i<dwSides; ++i){

			point1 = glm::vec3( 0, 1, 1 );
			point2 = glm::vec3( 0, 1, -1 );
			interPoint1 = glm::vec3( 0, 0, 0 );
			point3 = glm::vec3(-1, 0, 0);
			point4 = glm::vec3( 1, 0, 0 ); 
			interPoint2 = glm::vec3( 0, 1, 0 );
			
			parVec1 = getBezierDirection( hStep * (i), point1, point2, interPoint1 ); 
			parVec2 = getBezierDirection( vStep * (j), point3, point4, interPoint2 ); 

			currentVec = glm::normalize(-glm::cross( parVec1, parVec2 ));
			currentVec = cmat * currentVec;


			ind = rowIndicesTop[0][j][i];
			normals[ind] = currentVec;
		}
	}

	glm::vec3 endPoint;
	int si1, si2, lastInd;
	float radius, vStepD, currentT, currentTD;

	radius = currentSegment->getRadiuses().second;
	endPoint = currentSegment->getEndPosition();
	interPoint1  = endPoint + cmat * glm::vec3(0, radius * 4.f, 0);
	point1  = endPoint + cmat * glm::vec3( radius, 0, 0 );
	point2  = endPoint + cmat * glm::vec3( -radius, 0, 0 );

	lastInd = dwSides - 1;

	hStep = 1.f / (dwSides+1);
	vStepD = 1.f / (4.f * (dhSides));
	vStep = 1.f / (4.f * (dhSides-1));

	for(k=0; k<2; ++k){
		for(j=0; j<dhSides; ++j){

			interPoint1 = glm::vec3( 0, j * 1.f / dhSides * 0.4, 0 );

			for(i=0; i<dwSides/4; ++i){
				lastInd = dwSides-1-i;
				ind = rowIndicesCenter[k][j][lastInd];
				point1 = vertices[ind];
				point2 = sidesCenters[1][j];
				currentVec = glm::normalize( point1 - point2 );
				normals[ind] = currentVec;
			}
			for(i=0; i<(dwSides-dwSides/2); ++i){
				lastInd = dwSides/4 + i;
				ind = rowIndicesCenter[k][j][lastInd];
				point1 = vertices[ind];
				point2 = centersCenters[k][j];
				currentVec = glm::normalize(point2-point1) + interPoint1;
				normals[ind]=currentVec;
			}
			for(i=0; i<(dwSides/4); ++i){
				lastInd = i;
				ind = rowIndicesCenter[k][j][lastInd];
				point1 = vertices[ind];
				point2 = sidesCenters[0][j];
				currentVec = glm::normalize(point1 - point2);
				normals[ind]=currentVec;
			}
		}
	}
	
	// Center #2
	/*for(k=0; k<2; ++k){
		for(j=0; j<dhSides; ++j){
			
			si1 = k ? lastInd : 0;
			si2 = k ? 0 : lastInd;
			currentT = k ? (1.f - vStep * j) : (vStep * j);
			currentTD = k ? (1.f - vStepD * j) : (vStepD * j);

			parVec1 = getBezierDirection( 
					currentTD, 
					point1, 
					point2, 
					interPoint1 
			);
			interPoint2 = getBezierPoint( 
					currentT, 
					point1, 
					point2, 
					interPoint1 
			);

			point3 = vertices[ rowIndicesSides[ 0 ][ j ][ si1 ] ];
			point4   = vertices[ rowIndicesSides[ 1 ][ j ][ si2 ] ];

			for(i=0; i<dwSides; ++i){
				parVec2 = getBezierDirection( 
						hStep * (i+1), 
						point3, 
						point4, 
						interPoint2
				);
				currentVec = -glm::cross( parVec1, parVec2 );
				//if(!k) currentVec = -currentVec;

				normals[ rowIndicesCenter[k][j][i] ] = currentVec;
			}

		}
	}*/

	//Center #1
	/*vStep = 1.f / (4.f * (dhSides));	

	for(k=0; k<2; ++k){

		for(j=0; j<dhSides; ++j){
			point1 = vertices[
				rowIndicesCenter[k][j][0]
			];
			point2 = vertices[
				rowIndicesCenter[k][j][dwSides-1]
			];
			point3 = vertices[
				//ringIndices[0][0]
				rowIndicesCenter[k][j][0]
			];
			point4 = vertices[
				//ringIndices[0][dwSides-1]
				rowIndicesCenter[k][j][dwSides-1]
			];

			glm::vec3 pp1 = getBezierDirection( j * 1.f / 4.f / (dhSides), sBPoint, eBPoint, cBPoint );
			glm::vec3 pp1p = getBezierPoint( j * 1.f / 4.f / (dhSides), sBPoint, eBPoint, cBPoint );
			
			interPoint2 = currentSegment->getEndPosition() 
				+ cmat * glm::vec3(0, currentSegment->getRadiuses().second * 4.f, 0);
			
			interPoint1 = getBezierPoint( !k ? (1.f - vStep * (j+1)) : (vStep * (j+1)), point3, point4, interPoint2 ); 

			for(i=0; i<dwSides; ++i){
				parVec1 = pp1; //getBezierDirection( hStep * i, point1, point2, interPoint1 );
				if(!k) 
					parVec2 =	getBezierDirection( vStep * (j+1), point3, point4, interPoint2 );
				else 
					parVec2 =	getBezierDirection( vStep * (j+1), point4, point3, interPoint2 );				

				parVec2 = getBezierPoint( 1.f / dwSides * i, point3, point4, pp1p );

				currentVec = glm::normalize( glm::cross( parVec1, parVec2 ) );
				//if(k) currentVec = -currentVec;

				//parVec1 = getBezierDirection( hStep * i, glm::vec3( 0, 0, -1 ), glm::vec3( 0, 0, 1 ), glm::vec3( 0.4, 0, 0 ) );
				//parVec2 = glm::vec3( 0, -0.2, -1 );
					//getBezierDirection( hStep * j, glm::vec3(0,0,0), glm::vec3(0, 1, -1), glm::vec3( 0, 0.5, 0 ) );
				//currentVec = glm::normalize( glm::cross( parVec1, parVec2 ) );

				//currentVec = glm::cross( glm::vec3( 1, 1, 0 ), glm::vec3( 0, 0, 1 ) );

				currentVec = cmat * currentVec;
				
				//currentVec = cmat * glm::vec3( k ? -1.f : 1.f, 0, 0 );

				ind = rowIndicesCenter[k][j][i];
				normals[ind] = currentVec;
			}
		}
	}*/
	
	/*std::cout << "nnns " << normals.size() << std::endl;
	std::vector<glm::vec3> ns;
	for(i=0; i<normals.size(); ++i){
		for(j=1; j<indices.size(); ++j){
			if(indices[j] == i){
				ns.push_back( vertices[indices[j]] - vertices[indices[j-1]] );
				if(j+1<indices.size()) ns.push_back( vertices[indices[j]] - vertices[indices[j+1]] );
			}
		}
		std::cout << "nns " << ns.size() << std::endl;
		if(ns.size() > 1){
			glm::vec3 r = glm::vec3(0);
			for(k=1; k < ns.size();++k){
				std::cout << "ppp " << ns[k].x << " " << ns[k].y << " " << ns[k].z << std::endl;
				r = r + ns[k];
				//r += ni;
			}
			normals[i] = glm::normalize(glm::cross(r, ns[0]));
			std::cout << "nnn " << r.x << std::endl;
		}
		ns.clear();
	}*/

}

void SegmentBinderMesh::makeTexCords(){
	int i,j,k, vStepsCount, hStepsCount;
	float vStep, hStep, hStartMargin, vStartMargin, sideLength, centerLength;
	glm::vec2 t1, t2;

	vStepsCount = dw;
	hStepsCount = dhSides + dwSides/2 + 1;
	
	vStep = 1.f / (vStepsCount-1);
	hStep = 1.f / (hStepsCount-1);
	hStartMargin = 0.f;
	vStartMargin = 0.f;

	texCords.resize( vertices.size(), glm::vec2(0) );
	//Rings
	//Bottom ring
	for(i=0; i<vStepsCount; ++i){
		texCords[ ringIndices[0][i] ] = glm::vec2( vStep * i, 0 );
	}	
	vStepsCount = dwSides * 4;
	vStep = 1.f / ( vStepsCount );
	sideLength = dwSides / vStepsCount;
	centerLength = dwSides / vStepsCount;
	hStartMargin = hStep;

	//Ring 1 and 2
	
	for(i=0; i<dwSides; ++i){
		t1 = glm::vec2( vStep * i + sideLength * 1.5 + centerLength, 1 );
		t2 = glm::vec2( vStep * i + centerLength / 2.f, 1 );
		
		if(i==dw-1)
			texCords[ ringIndices[1][0] ] = t1;
		else{
			texCords[ ringIndices[1][i + dwSides - 2 ] ] = t1;
		
		}

		texCords[ ringIndices[2][i] ] = t2;
	}	

	for(j=0; j<dhSides; ++j){
		for(i=0; i<dwSides; ++i){

			t1 = glm::vec2( vStep * i - centerLength / 2.f, hStep * j );
			t2 = glm::vec2( vStep * i + sideLength * 1.5f + centerLength, hStep * j );
		
			texCords[ rowIndicesSides[1][j][i] ] = t2;
			texCords[ rowIndicesSides[0][j][i] ] = t1;
		}
	}

	for(j=0; j<dhSides; ++j){
		for(i=0; i<dwSides; ++i){
			t1 = glm::vec2( vStep * i, hStep * j );
			t2 = glm::vec2( vStep * i + centerLength + sideLength, hStep * j );
			texCords[ rowIndicesCenter[0][j][i] ] = t1;
			texCords[ rowIndicesCenter[1][j][i] ] = t2;
		}
	}

	for(j=0; j<dwSides/2; ++j){
		for(i=0; i<dwSides; ++i){
			t1 = glm::vec2( vStep * i - centerLength / 2.f, hStep * j );
			t2 = glm::vec2( vStep * i + sideLength * 1.5f + centerLength, hStep * j );
			texCords[ rowIndicesTop[0][j][i] ] = t1;
			texCords[ rowIndicesTop[0][dwSides - j - 1][i] ] = t2;
		}
	}
		
}

void SegmentBinderMesh::makeSingleChildIndices(){
	int i, j, k, ci, ni, ti, nti;
}
void SegmentBinderMesh::makeSidesIndices(){

	int i, j, k, ci, ni, ti, nti;

	for(k=0;k<2;++k){
		size_t **rowIndices = rowIndicesSides[k];
		for(j=0;j<dhSides-1;++j){
			for(i=0;i<dwSides-1;++i){
				ci = rowIndices[j][i];
				ti = rowIndices[j+1][i];

				if( i == dw - 1 ){ 
					ni = rowIndices[j][0]; 
					nti = rowIndices[j+1][0];
				}else{
					ni = rowIndices[j][i+1];
					nti = rowIndices[j+1][i+1];
				}
				indices.push_back(ci);
				indices.push_back(ni);
				indices.push_back(ti);
				indices.push_back(ti);
				indices.push_back(ni);
				indices.push_back(nti);
			}
		}
	}
	
	size_t *arr1 = new size_t[dwSides];
	size_t *arr2 = new size_t[dwSides];

	//Side 1 with bottom ring
	std::copy( &ringIndices[0][0], &ringIndices[0][dwSides], arr1 );
	std::copy( &rowIndicesSides[0][0][0], &rowIndicesSides[0][0][dwSides], arr2 );
	bindVertices(arr1, arr2, dwSides);

	//Side 2 with bottom ring 
	std::copy( &ringIndices[0][dw/2], &ringIndices[0][dw], arr1 );
	std::copy( &rowIndicesSides[1][0][0], &rowIndicesSides[1][0][dwSides], arr2 );
	arr1[dwSides-1] = ringIndices[0][0];
	bindVertices(arr1, arr2, dwSides);

	//Side 1 with top ring
	std::copy( &ringIndices[2][dw/2], &ringIndices[2][dw], arr1 );
	std::copy( &rowIndicesSides[1][dhSides-1][0], &rowIndicesSides[1][dhSides-1][dwSides], arr2 );
	arr1[dwSides-1] = ringIndices[2][0];
	bindVertices(arr1, arr2, dwSides);
	
	//Side 2 with top ring
	std::copy( &ringIndices[1][0], &ringIndices[1][dwSides], arr1 );
	std::copy( &rowIndicesSides[0][dhSides-1][0], &rowIndicesSides[0][dhSides-1][dwSides], arr2 );
	bindVertices(arr1, arr2, dwSides);

	delete[] arr1;
	delete[] arr2;

}
void SegmentBinderMesh::makeTopIndices(){
	int i;
	size_t *arr1 = new size_t[dwSides];
	size_t *arr2 = new size_t[dwSides];

	//Bind top
	for(i=1; i<dwSides; ++i){
		std::copy( &rowIndicesTop[0][i-1][0], &rowIndicesTop[0][i-1][dwSides], arr1 );
		std::copy( &rowIndicesTop[0][i][0], &rowIndicesTop[0][i][dwSides], arr2 );
		bindVertices(arr1, arr2, dwSides);
	}
	
	//Top with ring 1
	std::copy( &ringIndices[1][dwSides-1], &ringIndices[1][dw], arr1 );
	arr1[dwSides-1] = ringIndices[1][0];
	for(int i=0; i<dwSides; ++i) arr2[i] = rowIndicesTop[0][i][0];
	bindVertices(arr1, arr2, dwSides);

	//Top with ring 2
	std::copy( &ringIndices[2][0], &ringIndices[2][dwSides], arr1 );
	for(int i=0; i<dwSides; ++i) arr2[i] = rowIndicesTop[0][dwSides-i-1][dwSides-1];
	bindVertices(arr1, arr2, dwSides);

	delete[] arr1;
	delete[] arr2;
}
void SegmentBinderMesh::makeCenterIndices(){

	int i,k;
	size_t *arr1 = new size_t[dwSides];
	size_t *arr2 = new size_t[dwSides];

	//Bind center 1
	
	for(k=0;k<2;++k){
		for(i=1; i<dhSides; ++i){
			std::copy( &rowIndicesCenter[k][i-1][1], &rowIndicesCenter[k][i-1][dwSides], arr1 );
			std::copy( &rowIndicesCenter[k][i][1], &rowIndicesCenter[k][i][dwSides], arr2 );
			bindVertices(arr1, arr2, dwSides-1);
		}
	}

	//Center with top
	for(k=0; k<2; ++k){
		for(i=0; i<dwSides; ++i){
			arr1[i] = rowIndicesTop[0][k?0:(dwSides-1)][i];
			arr2[i] = rowIndicesCenter[k][dwSides-2][i];
		}
		bindVertices(arr1, arr2, dwSides);
	}

	//Center with side 1
	
	for(k=0; k<2; ++k){
		for(i=0;i<dhSides;++i){
			arr1[i] = rowIndicesSides[0][i][ k?(dwSides-1):0 ];
			arr2[i] = rowIndicesCenter[k][i][ 1 ];
		}
		bindVertices(arr1, arr2, dwSides-1);
	}

	//Center with side 2
	for(k=0; k<2; ++k){
		for(i=0;i<dhSides;++i){
			arr1[i] = rowIndicesSides[1][i][k?0:(dwSides-1)];
			arr2[i] = rowIndicesCenter[k][i][dwSides-1];
		}
		bindVertices(arr1, arr2, dwSides-1);
	}

	//Center with bottom ring
	
	for(k=0; k<2; ++k){
		for(i=0;i<dwSides;++i){
			arr1[i] =	ringIndices[0][k?(dwSides-1):0];
			arr2[i] = rowIndicesCenter[k][0][i];
		}
		bindVertices(arr1, arr2, dwSides-1);
	}

	//Additional
	
	for(k=0; k<2; ++k){
		std::fill( &arr1[0], &arr1[2], ringIndices[0][k?(dwSides-1):0] );	
		arr2[0] = rowIndicesSides[0][0][k?(dwSides-1):0];
		arr2[1] = rowIndicesCenter[k][0][0];
		bindVertices(arr1, arr2, 2);

		arr2[0] = rowIndicesSides[1][0][k?0:(dwSides-1)];
		arr2[1] = rowIndicesCenter[k][0][dhSides-1];
		bindVertices(arr1, arr2, 2);
	}

	//Sides, tops, center, rings
	
	//First center side
	arr1[0] = ringIndices[1][0];
	arr1[1] = rowIndicesSides[0][dhSides-1][0];
	arr2[0] = rowIndicesTop[0][dwSides-1][0];
	arr2[1] = rowIndicesCenter[0][dhSides-1][0];
	bindVertices(arr1, arr2, 2);

	arr1[0] = ringIndices[2][0];
	arr1[1] = rowIndicesSides[1][dhSides-1][dhSides];
	arr2[0] = rowIndicesTop[0][dwSides-1][dwSides-1];
	arr2[1] = rowIndicesCenter[0][dhSides-1][dwSides-1];
	bindVertices(arr1, arr2, 2);

	//Second center side
	arr1[0] = ringIndices[1][dwSides-1];
	arr1[1] = rowIndicesSides[0][dhSides-1][dwSides-1];
	arr2[0] = rowIndicesTop[0][0][0];
	arr2[1] = rowIndicesCenter[1][dhSides-1][0];
	bindVertices(arr1, arr2, 2);

	arr1[0] = ringIndices[2][dwSides-1];
	arr1[1] = rowIndicesSides[1][dhSides-1][0];
	arr2[0] = rowIndicesTop[0][0][dwSides-1];
	arr2[1] = rowIndicesCenter[1][dhSides-1][dwSides-1];
	bindVertices(arr1, arr2, 2);
	
	delete[] arr1;
	delete[] arr2;
}
void SegmentBinderMesh::makeIndices(){

	if(currentSegment->getChildrenCount() > 1){
		makeSidesIndices();
		makeTopIndices();
		makeCenterIndices();
	}else{
		makeSingleChildIndices();
	}

}

size_t SegmentBinderMesh::moveIndices( size_t mv ){
	for(int i=0;i<indices.size();++i){
		indices[i] += mv;
		if(i < dw){ 
			for(int k=0; k<currentSegment->getChildrenCount()+1; ++k){
				ringIndices[k][i] += mv;
			}
		}
	}
	return mv + vertices.size();
}

size_t *SegmentBinderMesh::getFirstRing(){
	size_t *res = new size_t[dw+1];
	for(int i=0; i<dw; ++i){
		res[i] = ringIndices[0][i];
	}
	res[dw] = res[0];
	return res;
}

size_t *SegmentBinderMesh::getLastRing(int ind){
	assert(ind < currentSegment->getChildrenCount());
	size_t *res = new size_t[dw+1];
	for(int i=0; i<dw; ++i){
		res[i] = ringIndices[ind + 1][i];
	}
	res[dw] = res[0];
	return res;
}

std::vector<glm::vec3> SegmentBinderMesh::getVertices(){
	return vertices;
}
std::vector<glm::vec3> SegmentBinderMesh::getNormals(){
	return normals;
}
std::vector<glm::vec2> SegmentBinderMesh::getTexCords(){
	return texCords;
}
std::vector<size_t> SegmentBinderMesh::getIndices(){
	return indices;
}
