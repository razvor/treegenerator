#include "TreeMeshMaker.h"
#include "SegmentBinderMesh.h"

TreeMeshMaker::TreeMeshMaker( SegmentData *r ){
	rootSegment = r;
	model = new TreeModel();
}
TreeMeshMaker::~TreeMeshMaker(){
	if(model) delete model;
	for( auto si : inheritSegmentsList ) delete si;
}

void TreeMeshMaker::runProcessing(){
	processingQueue.resize(1);
	currentProcessingQueue = 0;
	pushSegment(rootSegment);
	makerRunner(currentProcessingQueue);
	processSegments();
}

void TreeMeshMaker::makeTestSegment(){
	SegmentData *d = new SegmentData();
	d->setPosition( glm::vec3(0) );
	d->setAngle( glm::vec3(1)*glm::vec3( 0.7, 0.4, 0.2 ) );
	d->setRadiuses( 1.2, 1 );
	d->setLength( 12 );
	d->setDensity( 16, 16 );
	d->addCurvePoint( glm::vec3( 1, 2, 0 ) );
	d->addCurvePoint( glm::vec3( 0.3, 5, 1 ) );
	d->addCurvePoint( glm::vec3( -0.14, 8, -1 ) );
	d->setChildrenRotation( M_PI / 180.f * 0 );
	d->setInheritIndices( -1, -1 );
	d->computePostureMatrix();
	d->computeBinerMatrix();
	d->computeEndPosition();
	
	SegmentData *dc1 = new SegmentData();
	dc1->setRadiuses( 1.2 / sqrt(2.0), 1.2 / sqrt(2.0) * 0.9 );
	dc1->setDensity( 16, 16 );
	dc1->setLength(4);
	dc1->setAngle(glm::vec3(M_PI/6, 0, 0));
	dc1->addCurvePoint( glm::vec3( 0, 1, -0.2 ) );
	dc1->addCurvePoint( glm::vec3( 0, 2, -1 ) );
	dc1->setInheritIndices( 1, -1 );
	dc1->setParent(d);
	dc1->computePostureMatrix();
	
	SegmentData *dc2 = new SegmentData();
	dc2->setRadiuses( 0.9, 0.85 );
	dc2->setDensity( 16, 16 );
	dc2->setLength(4);
	dc2->setAngle(glm::vec3(-M_PI/3, 0, 0));
	dc2->addCurvePoint( glm::vec3( 1, 1, 1 ) );
	dc2->addCurvePoint( glm::vec3( 1, 2, 0 ) );
	dc2->setInheritIndices( 2, -1 );
	dc2->setParent(d);
	dc2->computePostureMatrix();

	SegmentData *dc3 = new SegmentData();
	dc3->setOutgrouthPosition(glm::vec2(0.75, 0.1f));
	dc3->setRadiuses( 0.6, 0.5 );
	dc3->setDensity( 16, 16 );
	dc3->setLength(4);
	dc3->setAngle(glm::vec3(-M_PI/9, 0, 0));
	dc3->addCurvePoint( glm::vec3( 0, 2, 0 ) );
	dc3->addCurvePoint( glm::vec3( 1, 2, 0 ) );
	dc3->setInheritIndices( -1, 0 );
	dc3->setOutgrouthPosition( glm::vec2(0.5, 0.5) );
	dc3->setParent(d);
	//dc3->computePostureMatrix();

	SegmentData *dc4 = new SegmentData();
	dc4->setOutgrouthPosition(glm::vec2(0.75, 0.1f));
	dc4->setRadiuses( 0.5, 0.4 );
	dc4->setDensity( 16, 16 );
	dc4->setLength(4);
	dc4->setAngle(glm::vec3(0, 0, 0));
	dc4->addCurvePoint( glm::vec3( 0, 2, 0 ) );
	dc4->addCurvePoint( glm::vec3( 1, 2, 0 ) );
	dc4->setInheritIndices( -1, 0 );
	dc4->setOutgrouthPosition( glm::vec2(0.5, 0.5) );
	dc4->setParent(dc3);

	SegmentData *dc5 = new SegmentData();
	dc5->setRadiuses( 0.4, 0.35 );
	dc5->setDensity( 16, 16 );
	dc5->setLength(4);
	dc5->setAngle(glm::vec3(M_PI/6, 0, 0));
	dc5->addCurvePoint( glm::vec3( 0, 1, -1 ) );
	dc5->addCurvePoint( glm::vec3( 0, 2, -1 ) );
	dc5->setInheritIndices( 1, -1 );
	dc5->setParent(dc4);

	SegmentData *dc6 = new SegmentData();
	dc6->setRadiuses( 0.35, 0.3 );
	dc6->setDensity( 16, 16 );
	dc6->setLength(4);
	dc6->setAngle(glm::vec3(-M_PI/6, 0, 0));
	dc6->addCurvePoint( glm::vec3( 0, 1, -1 ) );
	dc6->addCurvePoint( glm::vec3( 0, 2, -1 ) );
	dc6->setInheritIndices( 2, -1 );
	dc6->setParent(dc4);
	
	d->addChild(dc1);
	d->addChild(dc2);
	d->addOutgrouth(dc3);
	d->computeSidesHeights();
	d->computeChildrenPositions();
	d->computeOutgrouthsPositions();

	dc3->addChild(dc4);

	dc1->setPosition(d->getChildPosition(0));
	dc1->computeEndPosition();
	
	dc2->setPosition(d->getChildPosition(1));
	dc2->computeEndPosition();
	
	dc3->setPosition(d->getOutgrouthPosition(0));
	dc3->setPostureMatrix(d->getOutgrouthMatrix(0));
	dc3->computeEndPosition();
	//dc3->computeChildrenPositions();
	dc3->addOutgrouth(dc4);
	dc3->computeOutgrouthsPositions();

	dc4->setPosition(dc3->getOutgrouthPosition(0));
	//dc4->(dc3);
	dc4->setPostureMatrix(dc3->getOutgrouthMatrix(0));
	dc4->computeEndPosition();

	dc4->addChild(dc5);
	dc4->addChild(dc6);

	dc4->computeBinerMatrix();
	dc4->computeSidesHeights();
	dc5->computePostureMatrix();
	dc6->computePostureMatrix();
	dc4->computeChildrenPositions();
	
	dc5->setPosition(dc4->getChildPosition(0));
	dc5->computeEndPosition();

	dc6->setPosition(dc4->getChildPosition(1));
	dc6->computeEndPosition();

	//dc4->setPosition();

	makeSegment(d);
	makeSegment(dc1);
	makeSegment(dc2);
	makeSegment(dc3);
	makeSegment(dc4);
	makeSegment(dc5);
	makeSegment(dc6);
	processSegments();
}

void TreeMeshMaker::makeSegment( SegmentData *d ){
	SegmentMesh *m = new SegmentMesh(d);
	m->runProcessing();
	if( d->getChildrenCount() > 1 ){
		SegmentBinderMesh *bm = new SegmentBinderMesh(d);
		bm->runProcessing();
		m->setBinder(bm);
	}
	//std::cout << "pp " << m->getIndices().size() << std::endl;
	segmentsQueue.push(m);
	inheritSegmentsList.push_back(m);
}

void TreeMeshMaker::pushSegment( SegmentData *d ){
	processingQueue[currentProcessingQueue].push(d);
	for(int i=0; i<d->getChildrenCount(); ++i){
		pushSegment(d->getChild(i));
	}
	for(int i=0; i<d->getOutgrouthsCount(); ++i){
		pushSegment(d->getOutgrouth(i));
	}
}

void TreeMeshMaker::makerRunner(int ct){
	auto q = processingQueue[ct];
	while(!q.empty()){
		makeSegment(q.front());
		q.pop();
	}
}

TreeModel* TreeMeshMaker::getModel(){
	return model;
}

size_t TreeMeshMaker::processSegment(size_t offset){
	SegmentMesh *s = segmentsQueue.front();
	size_t r = s->moveIndices(offset);
	segmentsQueue.pop();

	SegmentMesh *par = findParent(s);
	if(par != NULL){
		s->bindWithParent(par);
	}

	auto verts = s->getVertices();
	auto norms = s->getNormals();
	auto texs = s->getTexCords();
	auto inds = s->getIndices();

	//std::cout << "process " << inds.size() << std::endl;
	
	model->vertices.insert( model->vertices.end(), verts.begin(), verts.end() );
	model->normals.insert( model->normals.end(), norms.begin(), norms.end() );
	model->texCords.insert( model->texCords.end(), texs.begin(), texs.end() );
	model->indices.insert( model->indices.end(), inds.begin(), inds.end() );

	return r;
}

SegmentMesh* TreeMeshMaker::findParent(SegmentMesh *s){
	SegmentMesh *p = NULL;
	if(s->getData()->getInheritType() == 1){
		for(auto it=inheritSegmentsList.begin(); it!=inheritSegmentsList.end(); ++it){
			if((*it)->getData() == s->getData()->getParent()){
				p = *it;
				break;
			}
		}
	}
	return p;
}

void TreeMeshMaker::processSegments(){
	size_t lastOffset = 0;
	while(!segmentsQueue.empty()){
		lastOffset = processSegment(lastOffset);	
	}
}
