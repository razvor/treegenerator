#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <fstream>
#include <streambuf>
#include <cmath>
#include <iomanip>

#include <GL/glew.h>

class Vec{
public:
	float x, y, z;

	Vec cross( Vec &b ){
		Vec c;

		/*cout << "c" << a.x << endl;
		cout << "c" << a.y << endl;
		cout << "c" << a.z << endl;

		cout << "b" << b.x<< endl;
		cout << "b" << b.y << endl;
		cout << "b" << b.z << endl;*/

		c.x = y * b.z - z * b.y;
		c.y = z * b.x - x * b.z;
		c.z = x * b.y - y * b.x;

		return c;
	}

	Vec normal(  ){
		Vec r;

		float l = sqrt( pow( x, 2.f ) + pow( y, 2.f ) + pow( z, 2.f ) );

		if( l == 0 )
			return Vec( x, y, z );

		r.x = x / l;
		r.y = y / l;
		r.y = z / l;

		return r;
	}

	float dot(Vec b){
		float r;

		r += x * b.x;
		r += y * b.y;
		r += z * b.z;

		cout << "Dot" << r << endl;

		return r;
	}

	friend Vec operator-( Vec a, const Vec& b ){
		Vec r;

		//r.x = a.x - b.x;
		//r.y = a.y - b.y;
		//r.z = a.z	- b.z;
	
		a.x -= b.x;
		a.y -= b.y;
		a.z -= b.z;

		return a;
	}

	Vec( float x, float y, float z ){
		this -> x = x;
		this -> y = y;
		this -> z = z;
	}

	Vec(){
		x = 0;
		y = 0;
		z = 0;
	}
};


//Matrices

float *getOrthoMatrix( float left, float right, float bottom, float top){
	const int rc = 4;
	float *mat = new float[rc * rc];
	
	mat[0] = 2.0 / (right-left);
	mat[1] = 0;
	mat[2] = 0;
	mat[3] = 0;

	mat[4] = 0;
	mat[5] = 2.0 / (top - bottom);
	mat[6] = 0;
	mat[7] = 0;

	mat[8] = 0;
	mat[9] = 0;
	mat[10] = 1;
	mat[11] = 0;
	
	mat[12] = -(right+left)/(right-left);
	mat[13] = -(top+bottom)/(top-bottom);
	mat[14] = 0;
	mat[15] = 1;

	return mat;
}

float *getTransformMatrix( float x, float y, float scaleX, float scaleY ){
	const int rc = 4;
	float *mat = new float[ rc * rc ];

	mat[0] = scaleX;
	mat[1] = 0;
	mat[2] = 0;
	mat[3] = 0;
	
	mat[4] = 0;
	mat[5] = scaleY;
	mat[6] = 0;
	mat[7] = 0;
	
	mat[8] = 0;
	mat[9] = 0;
	mat[10] = 1;
	mat[11] = 0;

	mat[12] = x;
	mat[13] = y;
	mat[14] = 0;
	mat[15] = 1;

	return mat;
}

float *getProjectionMatrix( float fov, float aspect, float far, float near ){
	float *mat = new float[16];

	float halfTan = tan( fov / 2.0f );
	float range = near - far;
	
	mat[0] = 1.0f / ( halfTan * aspect );
	mat[1] = 0;
	mat[2] = 0;
	mat[3] = 0;
	
	mat[4] = 0;
	mat[5] = 1.0f / ( halfTan );
	mat[6] = 0;
	mat[7] = 0;
	
	mat[8] = 0;
	mat[9] = 0;
	mat[10] = ( -near - far ) / range;
	mat[11] = 2.0f * far * near / range;

	mat[12] = 0;
	mat[13] = 0;
	mat[14] = 1;
	mat[15] = 0;

	return mat;
}

float *getViewMatrix( Vec eye, Vec target, Vec up ){
	float *mat = new float[16];

	cout << "----View----"<< endl;

	Vec az = (eye - target).normal();    
	Vec ax = up.cross( az ).normal(); //normal(cross(up, az));
	Vec ay = az.cross( ax );

	Vec f = Vec(
		target.x - eye.x,
		target.z - eye.y,
		target.z - eye.z		
	);

	f = f.normal();

	Vec s = f.cross( up );
	Vec u = s.normal().cross( f );

	//cout << az << endl;
	//cout << ax << endl;
	//cout << 
	
	mat[0] = ax.x;
	mat[1] = ay.x;
	mat[2] = az.x;
	mat[3] = 0;

	mat[4] = ax.y;
	mat[5] = ay.y;
	mat[6] = az.y;
	mat[7] = 0;

	mat[8] = ax.z;
	mat[9] = ax.y;
	mat[10] = ax.z;
	mat[11] = 0;

	mat[12] = -ax.dot( eye );
	mat[13] = -ay.dot( eye );
	mat[14] = -az.dot( eye );
	mat[15] = 1;

	//if( mat[0] == 0 ) mat[0] = 1;
	//if( mat[5] == 0 ) mat[5] = 1;
	//if( mat[10] == 0 ) mat[10] = 1;

	cout << "-----End--View---" << endl;

	return mat;
}

float *getEyeMatrix( Vec eye ){
	float *mat = new float[16];
	mat[0] = 1;
	mat[5] = 1;
	mat[10] = 1;
	mat[15] = 1;
	mat[12] = eye.x;
	mat[13] = eye.y;
	mat[14] = eye.z;
	return mat;
}

void showMatrix( float *mat ){
	const int r = 4;
	for( int i = 0; i < r; i++ ){
		for( int j = 0; j < r; j++ )
			cout << fixed << setw( 4 ) << setprecision( 6 ) << mat[ i * 4 + j ] << " ";
		cout << endl;
	}
}
