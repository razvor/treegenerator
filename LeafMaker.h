#ifndef LEAF_MAKER_H
#define LEAF_MAKER_H

#include <iostream>
#include <vector>
#include <algorithm>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/noise.hpp"

#include <GL/glew.h>

#include "TreeMeshMaker.h"
//#include "TreeGenerator.h"

using namespace std;

struct Leaf{
	vector< glm::vec3 > vertices;
	vector< glm::vec3 > normals;
	vector< int > indices;

	glm::vec3 position;
	glm::vec3 rotation;
};

struct LeafParams{
	bool isLeaf;	
	
	int leafCount;

	int branchCount;
	float startAngle;
	float size;

	int outCount;
	int curveFactor;

	int color;
};

class LeafMaker{
	
	vector< Leaf > leafs;
	
	LeafParams params;

	float getOneRandom();

	void addNewLeaf();
	void makeVertices();
	void makeIndices();
	void makeNormals();
	void makeTexCords();

	void makeLeaf( glm::vec3 position, glm::vec3 rotation );

	public:

	void setParams( LeafParams p );

	void makeLeafs( SegmentData *seg );

	vector<	Leaf > getLeafs();

	LeafMaker();
	~LeafMaker();

};

#endif
