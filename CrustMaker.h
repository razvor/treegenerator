#ifndef CRUST_MAKER_H
#define CRUST_MAKER_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <random>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/noise.hpp"

#include <GL/glew.h>

using namespace std;

struct Pixel{
	GLubyte r;	
	GLubyte g;	
	GLubyte b;	
};

class CrustMaker{

	GLubyte *texture;
	Pixel **pTexture;
	Pixel **pNormalMap;
	GLubyte *normalMap;
	int Width, Height;

public:

	void initTexture();

	void genStartNoise();

	const double intensity(const Pixel pixel);

	const int clamp(int pX, int pMax);
	const GLubyte mapComponent( float pX );

	void normalsFromHeight( float pTexture );
	void prepareNormalMap();

	void makeCrust();

	GLubyte *getTexture();
	GLubyte *getNormalMap();

	CrustMaker( int w, int h );

	~CrustMaker();

};

#endif
