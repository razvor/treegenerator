#ifndef TREE_GENERATOR_H
#define TREE_GENERATOR_H

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "SegmentData.h"
#include "TreeMeshMaker.h"
#include "LeafMaker.h"

typedef pair<float, float> MinMax;

struct TreeParams{
	
	struct Trunk{
		MinMax height;
		MinMax thin;
		MinMax scalation;
		MinMax curve;
		MinMax outgrouthStart;
		MinMax outgrouthFrequency;
		MinMax controlPointsCount;
		int duality;
		bool toEnd;
	} trunk;

	struct Branch{
		MinMax heightScale;
		MinMax radiusScale;
		MinMax curve;
		MinMax angle;
		MinMax outgrouthFrequency;
		MinMax outgrouthStart;
		MinMax controlPointsCount;
		float minRadius;
		int duality;
	} branch;

	struct Outgrouth{
		MinMax radius;
		MinMax radiusScale;
		MinMax controlPointsCount;
		MinMax height;
		MinMax angle;
		int duality;
	} outgrouth;
};

class TreeGenerator{

public:
	
	//Tree types
	enum ForkType{
		Straight,
		Forked
	};

	enum TrunkType{
		Long,
		Wide
	};

	enum Age{
		Yound,
		Middle,
		Old
	};

	enum BranchDirect{
		Top,
		Bottom
	};

	//Types params

	//Locals

	TrunkType currentTrunkType;
	ForkType forkType;
	Age currentAge;

	LeafMaker *leafMaker;
	//vector< TreeMeshSegment* > segmentsList; 
	
	SegmentData *rootSegment;
	TreeMeshMaker *maker;

	long long callCount = 0;

	/*float startMinHeight, 
				startMaxHeight,
				startMinRadius,
				startMaxRadius,
				minRadius,
				skinnyFactorMin,
				skinnyFactorMax,
				scaleCoff,
				childRotationFacor,
				curveFactor,
				outgrouthStart,
				outgrouthFrequencyMin,
				outgrouthFrequencyMax,
				outgrouthSizeFactor;*/

	TreeParams params;

	void setStandartParams();
	void setFirParams();
	void setParams( TreeParams p );

	static float getOneRand();

	float getRandom( float start, float end );

	int getRandomI( int start, int end );

	bool randomIs( int a );

	const float clamp( const float a, const float max );

	void cc();

	void generateFragment();

	SegmentData* generateFragment( SegmentData *par, int childType );
	
	void processPositions( SegmentData *d );

	void generateOutgrouths( SegmentData *par );

	void generateFragmentO( SegmentData *par, int ind, float positionX, float positionY );

	std::vector<glm::vec3> genControlPoints( int type, float height );

	void runProcessing();
	TreeModel *getModel();

	void generateNode();
	///void addSeg( TreeMeshSegment *seg );
	void clearSegmentsData();

	void generateLeafs( SegmentData *s );
	void initLeafMaker();
	std::vector< Leaf > getLeafs();

	TreeGenerator();
	~TreeGenerator();
};
#endif
