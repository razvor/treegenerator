#ifndef TREE_GENERATOR
#define TREE_GENERATOR

#include "TreeGenerator.h"

void TreeGenerator::setStandartParams(){

	params.trunk.height             = make_pair( 3, 6 );
	params.trunk.thin               = make_pair( 0.02, 0.1 );
	params.trunk.scalation          = make_pair( 0.8, 0.9 );
	params.trunk.curve              = make_pair( 0.0, 0.2f );
	params.trunk.outgrouthStart     = make_pair( 0.6f, 0.9f );
	params.trunk.outgrouthFrequency = make_pair( 0, 1 );
	params.trunk.controlPointsCount = make_pair( 1, 2 );
	params.trunk.duality            = 3;
	params.trunk.toEnd              = false;

	params.branch.heightScale = make_pair( 0.7f, 0.8f );
	params.branch.radiusScale = make_pair( 0.8f, 0.7f );
	params.branch.curve       = make_pair( 0.07f, 0.2f );
	params.branch.angle       = make_pair( 0.1f, 0.16f );
	params.branch.controlPointsCount = make_pair( 1, 2 );
	params.branch.duality     = 2;
	params.branch.minRadius   = 0.01;
	params.branch.outgrouthFrequency = make_pair( 1, 6 );
	params.branch.outgrouthStart     = make_pair( 0.1f, 0.1f );

	params.outgrouth.height = make_pair( 0.5, 0.8 );
	params.outgrouth.radius = make_pair( 0.5, 0.6 );
	params.outgrouth.controlPointsCount = make_pair( 1, 2 );
	params.outgrouth.duality = 4;
	params.outgrouth.radiusScale = make_pair( 0.7, 0.6 );
	params.outgrouth.angle = make_pair( 0.1, 0.25 );

}

void TreeGenerator::setFirParams(){

	params.trunk.height             = make_pair( 20, 30 );
	params.trunk.thin               = make_pair( 0.02, 0.02 );
	params.trunk.scalation          = make_pair( 0.1, 0.1 );
	params.trunk.curve              = make_pair( 0.0, 0.1f );
	params.trunk.outgrouthStart     = make_pair( 0.1f, 0.9f );
	params.trunk.outgrouthFrequency = make_pair( 15, 30 );
	params.trunk.controlPointsCount = make_pair( 2, 5 );
	params.trunk.duality            = 6;
	params.trunk.toEnd              = true;

	params.branch.heightScale = make_pair( 0.6f, 0.7f );
	params.branch.radiusScale = make_pair( 0.01f, 0.01f );
	params.branch.curve       = make_pair( 0.03f, 0.2f );
	params.branch.angle       = make_pair( 0.05f, 0.2f );
	params.branch.controlPointsCount = make_pair( 2, 4 );
	params.branch.duality     = 6;
	params.branch.minRadius   = 0.01;
	params.branch.outgrouthFrequency = make_pair( 10, 20 );
	params.branch.outgrouthStart     = make_pair( 0.1f, 0.5f );

	params.outgrouth.height = make_pair( 0.4, 0.6 );
	params.outgrouth.radius = make_pair( 0.5, 0.5 );
	params.outgrouth.controlPointsCount = make_pair( 1, 2 );
	params.outgrouth.duality = 6;
	params.outgrouth.radiusScale = make_pair( 0.01, 0.001 );
	params.outgrouth.angle = make_pair( 0.1, 0.25 );

}

float TreeGenerator::getOneRand(){
	return (rand() % 1000) / 1000.f;
}

float TreeGenerator::getRandom( float start, float end ){
	return start + ( getOneRand() * ( end - start ) );
}

int TreeGenerator::getRandomI( int start, int end ){
	return start + ( rand() % ( end - start ) );
}

bool TreeGenerator::randomIs( int a ){
	return ( rand() % a == a/2 );
}

void TreeGenerator::cc(){
	callCount ++;
}

const float TreeGenerator::clamp( const float a, const float max ){
	if( a <= 0 ) return 0;
	if( a >= max ) return max;
	return a;
}

//Trunk
void TreeGenerator::generateFragment(){
	
	srand( time( 0 ) );
	
	auto trunkParams = params.trunk;
	
	float skinnyFactorMin = trunkParams.thin.first,
				skinnyFactorMax = trunkParams.thin.second,
				heightMin       = trunkParams.height.first,
				heightMax       = trunkParams.height.second,
				scalationMin    = trunkParams.scalation.first,
				scalationMax    = trunkParams.scalation.second,
				curveMin        = trunkParams.curve.first,
				curveMax        = trunkParams.curve.second;


	float height      = getRandom( heightMin, heightMax ),
				startRadius = height      * getRandom(skinnyFactorMin, skinnyFactorMax),
				endRadius   = startRadius * getRandom( scalationMin, scalationMax ),
				pointX      = height      * getRandom( curveMin, curveMax ),
				pointZ      = height      * getRandom( curveMin, curveMax ),
				pointY      = height / 2;

	int density = 16;
	std::vector<glm::vec3> controlPoints;

	controlPoints = genControlPoints(1, height);

	SegmentData *s = new SegmentData();
	SegmentData *child1, *child2;

	rootSegment = s;

	s->setPosition(glm::vec3(0));
	s->setAngle(glm::vec3(0));
	s->setRadiuses( startRadius, endRadius );
	s->setLength( height );
	s->setDensity( density, density );

	for(auto cp : controlPoints)
		s->addCurvePoint(cp);
	
	s->setChildrenRotation( M_PI / 180.f * 0 );
	
	if( !trunkParams.toEnd ){
		if( randomIs( trunkParams.duality ) ){
			child1 = generateFragment( s, 1 );
			child2 = generateFragment( s, 2 );

		}else{
			child1 = generateFragment( s, 0 );
		}

	}
	generateOutgrouths( s	);

	processPositions(s);

}

SegmentData* TreeGenerator::generateFragment( SegmentData *par, int childType ){		

	auto branchParams = params.branch;

	const float 
		pHeight = par->getLength(),
		pRadius = par->getRadiuses().second;

	const float startRadiusCoffs[] = {
		0.8,
		1
	};

	const float
			heightCoffMin   = branchParams.heightScale.first,
			heightCoffMax   = branchParams.heightScale.second,	
			radiusCoffMin   = branchParams.radiusScale.first,
			radiusCoffMax   = branchParams.radiusScale.second,
			startRadiusCoff = childType == 0 ? 1 : startRadiusCoffs[ childType - 1 ],
			rotationCoffMin = branchParams.angle.first,
			rotationCoffMax = branchParams.angle.second,
			curveCoffMin    = branchParams.curve.first,
			curveCoffMax    = branchParams.curve.second;

	float 
				height        = pHeight     * getRandom( heightCoffMin, heightCoffMax ),
				startRadius   = pRadius     * startRadiusCoff,
				endRadius     = startRadius * getRandom( radiusCoffMin, radiusCoffMax ),
				pointX        = height      * getRandom( curveCoffMin, curveCoffMax ),
				pointZ        = height      * getRandom( curveCoffMin, curveCoffMax ),
				pointY        = height      * getRandom( 0.3, 0.7 ),
				sideRotation  = M_PI        * getRandom( rotationCoffMin, rotationCoffMax ),
				blockRotation = getRandom( 0, M_PI );

	int density = par->getDensity().first;

	int coef = 1;

	do{
		if(rootSegment->getRadiuses().first > coef * par->getRadiuses().first ){
			coef *= 2;
		}else{
			break;
		}
	}while(1);

	if(coef >= 2 && density > 4) density /= 2;

	glm::mat4 parentMat = par->getPostureMatrix();

	if( childType == 1 ){ 
		//pointZ *= -1;
		sideRotation *= -1;
	}

	if( childType == 2 ){
		//sideRotation = 0;
		pointZ *= -1;
	}

	if( childType == 0 ){
		parentMat = par->getPostureMatrix();
		sideRotation = -par->getAngle().x * getRandom( 0.1f, 1.0f );
		pointX = -par->getCurvePoints()[0].x * getRandom( 0.1f, 1.0f );
		pointZ = -par->getCurvePoints()[1].z * getRandom( 0.1f, 1.0f );
	}else{
		if( randomIs( 5 ) ){
			pointX *= -1;
		}

		if( randomIs( 5 ) ){
			pointZ *= -1;
		}
	}

	SegmentData *s = new SegmentData();
	s->setRadiuses( startRadius, endRadius );
	s->setDensity( density, density );
	s->setLength( height );
	s->setAngle( glm::vec3(-sideRotation, 0, 0) );
	s->addCurvePoint( glm::vec3(pointX, pointY, pointZ) );
	s->addCurvePoint( glm::vec3(pointX, pointY, pointZ) );
	s->setChildrenRotation( blockRotation );
	s->setParent(par);
	s->setInheritIndices(childType, -1);
	s->computePostureMatrix();
	s->computeBinerMatrix();

	par->addChild(s);

	if( endRadius >= params.branch.minRadius ){
		SegmentData *child1, *child2, *outgrouth;
		child1 = NULL;
		child2 = NULL;

		if( randomIs( branchParams.duality ) ){
			child1 = generateFragment( s, 1 );
			child2 = generateFragment( s, 2 );
		}else{
			child1 = generateFragment( s, 0 );
		}

		for(int i=0; i < 10; ++i){
			float a = 0.5 / 10.f * i;
			//generateFragmentO( s, i, 0.3 + a , 0.4 );
		}
		generateOutgrouths( s	);
	}
	

	if( endRadius <= branchParams.minRadius * 9 ){
		//std::cout << "LEAf" << std::endl;
		//generateLeafs( s );
	}

	return s;

}

void TreeGenerator::generateOutgrouths( SegmentData *par ){

	const int quality = par -> getDensity().first;
	
	const float
		pRadius = par -> getRadiuses().first,
		pHeight = par -> getLength();

	const float maxOutgrouthRadius = params.outgrouth.radius.second * pRadius;

	const MinMax countMM = !par -> getParent() ? params.trunk.outgrouthFrequency : params.branch.outgrouthFrequency;
	const MinMax startH = !par -> getParent() ? params.trunk.outgrouthStart : params.branch.outgrouthStart;

	const int 
		heightSectorsCount = pHeight / maxOutgrouthRadius,
		widthSectorsCount  = 2.f * M_PI * pRadius / maxOutgrouthRadius;

	const float
		width       = 4, //2.f * M_PI * pRadius,
		height      = 4, //pHeight - pHeight / static_cast<float>(quality),
		startWidth  = 0.f,
		startHeight = getRandom( startH.first, startH.second ) * height;

	const int 
		count      = getRandomI( countMM.first, countMM.second ),
		relation   = floor( height / width );

	if( !count || !heightSectorsCount ) return;

	int widthOutgrouthCount, 
			heightOutgrouthCount, 
			widthMotionStep, 
			heightMotionStep,
			ind;

	float 
		widthSpace,
		heightSpace,
		currentWidthPosition, 
		currentHeightPosition,
		currentWidthMargin,
		currentHeightMargin;

	widthOutgrouthCount = count / heightSectorsCount;
	if( widthOutgrouthCount == 0 ) widthOutgrouthCount = 1;
	heightOutgrouthCount = count / widthOutgrouthCount;

	widthMotionStep = widthSectorsCount / ( widthOutgrouthCount + 1 );
	heightMotionStep = heightSectorsCount / ( heightOutgrouthCount + 1 );

	if( widthMotionStep == 0 ) widthMotionStep = 1;
	if( heightMotionStep == 0 ) heightMotionStep = 1;

	widthSpace = 1.f / (widthSectorsCount + 1);
	heightSpace = 1.f / (heightSectorsCount + 1);

	/*
	FOR DEBUGGING 
	cout << "GEN OUT" << endl;
	cout << "wsc" << widthSectorsCount << endl;
	cout << "hsc" << heightSectorsCount << endl;
	cout << widthOutgrouthCount << endl;
	cout << heightOutgrouthCount << endl;
	cout << "wms" <<  widthMotionStep << endl;
	cout << "hms" << heightMotionStep << endl;
	cout << "OUT" << endl;*/

	currentWidthPosition = 0;

	for( int j = 0; j < heightOutgrouthCount; ++j ){
		for( int i = 0; i < widthOutgrouthCount; ++i ){
			
			currentWidthMargin  = getRandom( -widthMotionStep / 2, widthMotionStep / 2 );
			currentHeightMargin = getRandom( -heightMotionStep / 2, heightMotionStep / 2 );

			currentWidthPosition += widthSpace * ( i + 1 ) * (widthMotionStep + currentWidthMargin);
			currentHeightPosition = heightSpace * ( j + 1 ) * (heightMotionStep + currentHeightMargin);

			if( currentWidthPosition > 1 ) currentWidthPosition -= 1;

			/*
			FOR DEBUGGING
			cout << "CCp" << endl;
			cout << currentWidthPosition << endl;
			cout << currentHeightPosition << endl;
			cout << "END CC" << endl;
			*/

			ind = 0;
			if( currentWidthPosition < 0.9 && currentHeightPosition < 0.9 ){
				std::cout << "GEN OUTS " << currentWidthPosition << " " << currentHeightPosition << std::endl;
				generateFragmentO( par, ind, currentWidthPosition, currentHeightPosition );
				++ind;
			}

		}		
	}
	
}

void TreeGenerator::generateFragmentO( SegmentData *par, int ind, float positionX, float positionY ){

	//std::cout << "making outgrouth " << std::endl;

	auto outgrouthParams = params.outgrouth;
	
	const float 
		pHeight = par->getLength(),
		pStartRadius = par->getRadiuses().first,
		pEndRadius = par->getRadiuses().second,
		pCurrentRadius = pStartRadius + (pEndRadius - pStartRadius) * positionY,
		pQ = par->getDensity().first;

	std::vector<glm::vec3> controlPoints;
	int controlPointsCount = getRandom( outgrouthParams.controlPointsCount.first, outgrouthParams.controlPointsCount.second );

	float 
		radiusCoffMin  = outgrouthParams.radius.first,
		radiusCoffMax  = outgrouthParams.radius.second,
		heightCoffMin  = outgrouthParams.height.first,
		heightCoffMax  = outgrouthParams.height.second,
		scaleCoff      = getRandom( heightCoffMin, heightCoffMax ),
		scaleCoff2      = getRandom( radiusCoffMin, radiusCoffMax ),
		radiusScaleCoffMin = outgrouthParams.radiusScale.first,
		radiusScaleCoffMax = outgrouthParams.radiusScale.second,
		angleCoffMin   = outgrouthParams.angle.first,
		angleCoffMax   = outgrouthParams.angle.second;

	if( rand() % 3 == 1 )
		scaleCoff *= 0.7;

	float
		oHeight = pHeight * scaleCoff * powf(1.f-positionY * 0.5f, 1.f),
		oStartRadius = pCurrentRadius * scaleCoff2,
		oEndRadius = oStartRadius * getRandom( radiusScaleCoffMin, radiusScaleCoffMax ),
		oSideRotation = -M_PI / 2.f + getRandom( angleCoffMin, angleCoffMax ) * M_PI;

	int density = par->getDensity().first;

	int coef = 1;

	do{
		if(rootSegment->getRadiuses().first > coef * par->getRadiuses().first ){
			coef *= 2;
		}else{
			break;
		}
	}while(1);

	if(coef >= 2 && density > 4) density /= 2;

	controlPoints = genControlPoints( 3, oHeight );

	if( oStartRadius <= params.branch.minRadius )
		return;

	SegmentData *s = new SegmentData();
	s -> setRadiuses( oStartRadius, oEndRadius );
	s -> setLength( oHeight );
	s -> setAngle( glm::vec3( oSideRotation, 0, 0 ) );
	s -> setDensity( density, density );
	for(auto cp : controlPoints)
		s -> addCurvePoint(cp);
	//s -> addCurvePoint( glm::vec3( pointX, pointY, pointZ ) );
	s -> setOutgrouthPosition(glm::vec2( positionX, positionY));
	s -> setInheritIndices( -1, 0 );
	s -> setParent(par);

	par->addOutgrouth(s);

	SegmentData *child1, *child2;

	if( oEndRadius >= params.branch.minRadius ){
		if( randomIs( outgrouthParams.duality ) ){
		
			child1 = generateFragment( s, 1 );
			child2 = generateFragment( s, 2 );

		}else{

			child1 = generateFragment( s, 0 );

		}
	}

	int cnt = 16;
	for(int i=0; i < cnt; ++i){
		float a = 0.8 / cnt * i;
		//generateFragmentO( s, i, getRandom(0, 1.0), 0.0 +a );
	}
	
	generateOutgrouths( s );

}

std::vector<glm::vec3> TreeGenerator::genControlPoints( int type, float height ){
	
	assert( type >= 1 && type <= 3  );
	
	std::vector<glm::vec3> res;
	MinMax mmCount, mmCurve;
	float pointX, pointY, pointZ, curveCoef;
	int count, i;
	glm::vec3 point;

	if(type == 1){ //Trunk
		mmCount = params.trunk.controlPointsCount;
		mmCurve = params.trunk.curve;
	}else if(type == 2){ //Brancheight
		mmCount = params.branch.controlPointsCount;
		mmCurve = params.branch.curve;
	}else{ //Outgrouth
		mmCount = params.outgrouth.controlPointsCount;
		mmCurve = params.branch.curve;
	}

	count = getRandom(mmCount.first, mmCount.second);
	curveCoef = height * getRandom(mmCurve.first, mmCurve.second);

	pointY = height * 0.1;
	for(i=0; i<count; ++i){
		pointX = getRandom( -curveCoef, curveCoef ),
		pointZ = getRandom( -curveCoef, curveCoef ),
		pointY = getRandom( pointY, height * 0.9 );
		point = glm::vec3(pointX, pointY, pointZ);

		res.push_back(point);
	}

	return res;
}

void TreeGenerator::processPositions( SegmentData *s ){
	s->computeEndPosition();

	if(s->getChildrenCount() > 0){
		
		s->computeBinerMatrix();
		for(int i=0; i<s->getChildrenCount(); ++i)
			s->getChild(i)->computePostureMatrix();
		
		s->computeSidesHeights();
		s->computeChildrenPositions();
		
		for(int i=0; i<s->getChildrenCount(); ++i){
			s->getChild(i)->setPosition(s->getChildPosition(i));
			processPositions(s->getChild(i));
		}
	}

	if(s->getOutgrouthsCount() > 0){
		//std::cout << "processing outgrouths" << std::endl;
		s->computeOutgrouthsPositions();
		for(int i=0; i<s->getOutgrouthsCount(); ++i){
			s->getOutgrouth(i)->setPosition(s->getOutgrouthPosition(i));
			s->getOutgrouth(i)->setPostureMatrix(s->getOutgrouthMatrix(i));
			s->getOutgrouth(i)->computeBinerMatrix();
			processPositions(s->getOutgrouth(i));
		}
	}
}

void TreeGenerator::generateLeafs( SegmentData *s ){
	leafMaker -> makeLeafs( s );
}

void TreeGenerator::generateNode(){

}

/*void TreeGenerator::addSeg( TreeMeshSegment *seg ){
	segmentsList.push_back( seg );
}*/

void TreeGenerator::initLeafMaker(){
	leafMaker = new LeafMaker();
	LeafParams p;
	p.branchCount = 1;
	p.outCount = 3;
	p.size = 0.5f;
	p.leafCount = 7;
	leafMaker -> setParams(p);
	//leafMaker -> makeLeafs( nullptr );
}

vector< Leaf > TreeGenerator::getLeafs(){
	return leafMaker->getLeafs();
}

void TreeGenerator::runProcessing(){
	std::cout << "Begin mesh " << std::endl;
	maker = new TreeMeshMaker(rootSegment);
	maker->runProcessing();
}

TreeModel *TreeGenerator::getModel(){
	return maker->getModel();
}

TreeGenerator::TreeGenerator(){

	initLeafMaker();

	setStandartParams();
	//setFirParams();
	generateFragment();
}

TreeGenerator::~TreeGenerator(){
	//delete leafMaker;
	delete maker;
	delete rootSegment; 
}

#endif
